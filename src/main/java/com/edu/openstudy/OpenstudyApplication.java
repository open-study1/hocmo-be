package com.edu.openstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion;

@SpringBootApplication
@EnableScheduling
public class OpenstudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenstudyApplication.class, args);
	}


}
