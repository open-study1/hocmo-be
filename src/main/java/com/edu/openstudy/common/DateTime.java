package com.edu.openstudy.common;

import java.time.LocalDateTime;

public class DateTime {
	private static LocalDateTime CREATE_DATE;

	public synchronized static LocalDateTime getInstances() {
		if (CREATE_DATE == null)
			CREATE_DATE = LocalDateTime.now();
		return CREATE_DATE;
	}
	public synchronized static void setTimeNow()
	{
		CREATE_DATE = LocalDateTime.now();
	}
}
