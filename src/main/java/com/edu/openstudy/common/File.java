package com.edu.openstudy.common;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class File {
	public static final String TYPE_AUDIO = "audio";
	public static final String TYPE_IMAGE = "image";
	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_SHADOWING = "shadowing";
	private MultipartFile file;
	private String fileName;
}
