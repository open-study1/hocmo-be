package com.edu.openstudy.common;

import org.slf4j.LoggerFactory;

public class Logger {
	public static final org.slf4j.Logger logInfo = LoggerFactory.getLogger("info");
	public static final org.slf4j.Logger logWarn = LoggerFactory.getLogger("warn");
	public static final org.slf4j.Logger logError = LoggerFactory.getLogger("error");
}
