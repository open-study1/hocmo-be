package com.edu.openstudy.common;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

public class ObjectMapper {
	private static com.fasterxml.jackson.databind.ObjectMapper OBJECT_MAPPER;

	public static com.fasterxml.jackson.databind.ObjectMapper getMapper() {
		if (OBJECT_MAPPER == null) {
			OBJECT_MAPPER = new com.fasterxml.jackson.databind.ObjectMapper();
			OBJECT_MAPPER.registerModule(new ParameterNamesModule());
			OBJECT_MAPPER.registerModule(new Jdk8Module());
			OBJECT_MAPPER.registerModule(new JavaTimeModule());
			OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		}
		return OBJECT_MAPPER;
	}
}
