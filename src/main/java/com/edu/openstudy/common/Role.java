package com.edu.openstudy.common;

public class Role {
    public static String SUPER_ADMIN = "ROLE_Super_Admin";
    public static String ADMIN = "ROLE_Admin";
    public static String EDITOR = "ROLE_Editor";
    public static String TEACHER = "ROLE_Teacher";
    public static String PARTNER = "ROLE_Partner";
    public static String END_USER = "ROLE_End_User";
    public static String MODERATOR = "ROLE_Moderator";
    public static String IT_STAFF = "ROLE_IT_Staff";
}

