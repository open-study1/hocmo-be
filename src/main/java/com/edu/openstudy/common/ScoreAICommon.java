package com.edu.openstudy.common;

public class ScoreAICommon {
	public static final Integer MAX_SCORE_CONTENT_SPEECH = 5;
	public static final Integer MAX_SCORE_CONTENT_SWT_AND_SST = 2;
	public static final Integer MAX_SCORE_CONTENT_ESSAY = 3;
	public static final Integer MAX_SCORE_PRONUNCIATION = 5;
	public static final Integer MAX_SCORE_ORAL_FLUENCY = 5;
	public static final Integer MAX_SCORE_VOCABULARY_RANGE = 2;
	public static final Integer MAX_SCORE_FORM_ESSAY = 2;
	public static final Integer MAX_SCORE_FORM_SST = 2;
	public static final Integer MAX_SCORE_FORM_SWT = 1;
	public static final Integer MAX_SCORE_DEVELOPMENT_STRUCTURE_COHERENCE = 2;
	public static final Integer MAX_SCORE_GENERAL_LINGUISTIC_RANGE = 2;
	public static final Integer MAX_SCORE_SPELLING = 2;
	public static final Integer MAX_SCORE_GRAMMAR = 2;
}
