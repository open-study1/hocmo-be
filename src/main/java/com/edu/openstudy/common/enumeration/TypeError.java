package com.edu.openstudy.common.enumeration;

public enum TypeError {
	NotFound,
	InternalServerError,
	BadRequest,
	NoContent,
	AlreadyExists,
	Forbidden
}
