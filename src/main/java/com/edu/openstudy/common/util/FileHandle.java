
package com.edu.openstudy.common.util;

import com.edu.openstudy.common.File;

public interface FileHandle {
    void update(File file);

    void delete(String url);

    String getAttributeFile(String nameFile);

    String setURLServer(String url);
}
