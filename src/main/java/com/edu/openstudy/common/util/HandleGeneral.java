
package com.edu.openstudy.common.util;

import java.time.LocalDateTime;
import java.util.List;

import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.TokenDTO;
import com.edu.openstudy.data.entity.QuestionTypeEntity;
import com.edu.openstudy.data.request.RequestUserInfoDTO;

public interface HandleGeneral {

	QuestionTypeEntity setQuestionTypeMatchCategrory(List<ChapterDTO> chapter);
	int countWord(String input);
	String normalizeString(String input);
	String getURL(String content);
	String parseLocalDateTime(LocalDateTime dateTime);
	LocalDateTime parseLocalDateTime(String dateTime);
	Integer randomScore();

    Long getIdUser(String response);

    Long getUserType(String response);

    TokenDTO createToken(RequestUserInfoDTO requestUserInfoDTO);
	double convertScore5To90(double score, double maxScore);

	void validStartTime(String startTimeStr);
}
