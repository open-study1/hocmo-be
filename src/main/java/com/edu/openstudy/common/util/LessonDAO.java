package com.edu.openstudy.common.util;

import java.util.List;

import com.edu.openstudy.data.entity.LessonEntity;

public interface LessonDAO {
	public List<LessonEntity> findByQuery(String query, int page, int limit);
	public long countTotalRecords(String query);
}
