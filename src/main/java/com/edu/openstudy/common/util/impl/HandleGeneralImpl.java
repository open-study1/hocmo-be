package com.edu.openstudy.common.util.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Component;

import com.edu.openstudy.common.util.HandleGeneral;
import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.TokenDTO;
import com.edu.openstudy.data.entity.QuestionTypeEntity;
import com.edu.openstudy.data.request.RequestUserInfoDTO;

@Component
public class HandleGeneralImpl implements HandleGeneral {

	@Override
	public QuestionTypeEntity setQuestionTypeMatchCategrory(List<ChapterDTO> chapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countWord(String input) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String normalizeString(String input) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getURL(String content) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String parseLocalDateTime(LocalDateTime dateTime) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LocalDateTime parseLocalDateTime(String dateTime) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer randomScore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getIdUser(String response) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getUserType(String response) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TokenDTO createToken(RequestUserInfoDTO requestUserInfoDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double convertScore5To90(double score, double maxScore) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void validStartTime(String startTimeStr) {
		// TODO Auto-generated method stub
		
	}
   

}
