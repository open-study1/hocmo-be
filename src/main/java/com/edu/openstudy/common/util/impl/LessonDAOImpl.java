
package com.edu.openstudy.common.util.impl;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.common.util.LessonDAO;
import com.edu.openstudy.data.entity.LessonEntity;

@Repository
public class LessonDAOImpl implements LessonDAO {

	private EntityManager entityManager;

	@Autowired
	public LessonDAOImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional
	public List<LessonEntity> findByQuery(String query, int page, int limit) {
		System.out.print(query);
		List<LessonEntity> lessons = new ArrayList<LessonEntity>();
		try {
			Session currentSession = entityManager.unwrap(Session.class);

			Query<LessonEntity> theQuery = currentSession.createQuery(query, LessonEntity.class);

			theQuery.setFirstResult(page * limit );
			theQuery.setMaxResults(limit);
			

			lessons = theQuery.getResultList();
			
		} catch (Exception e) {
			
		}
		return lessons;
	}

	@SuppressWarnings("unused")
	@Override
	@Transactional
	public long countTotalRecords(String query) {
		Session currentSession = entityManager.unwrap(Session.class);
		String countQ = "Select count (*) ";
		// Delete Group by from query
		int indexOfGroupBy = query.indexOf("GROUP BY");
		if(indexOfGroupBy > 0) {
			query = query.substring(0,indexOfGroupBy);	
		}
		countQ = countQ.concat(query);
		Query<Long> countQuery = currentSession.createQuery(countQ);
		return (Long) countQuery.uniqueResult();
	}
}
