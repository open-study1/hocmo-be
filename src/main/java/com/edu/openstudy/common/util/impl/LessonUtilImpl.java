package com.edu.openstudy.common.util.impl;

import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.edu.openstudy.common.util.LessonUtil;
import com.edu.openstudy.constant.Constant;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.mapper.LessonMapper;
import com.edu.openstudy.data.repository.ExamRepository;
import com.edu.openstudy.data.repository.LessonRepository;
import com.edu.openstudy.service.ChapterService;
import com.edu.openstudy.service.LessonService;

@Component("LessonUtil")
public class LessonUtilImpl implements LessonUtil {
    @Autowired
    private LessonRepository lessonRepository;
    @Autowired
    private LessonMapper lessonMapper;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LessonService lessonService;
    @Autowired
    private ChapterService chapterService;
    @Autowired
    private ChapterMapper chapterMapper;
    @Autowired
    private ExamRepository questionGroupRepository;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.DATE_TIME_FORMAT);

//    @Override
//    public LessonEntity setOrderAndSave(LessonCreateDTO dto, ChapterDTO firstChapter) {
//        LessonEntity lesson = lessonMapper.mapToEntity(dto);
//        // Set order
//        if (firstChapter == null)
//            lesson.setZOrder(setOrder(firstChapter.getId(), null));
//        else
//            lesson.setZOrder(setOrder(firstChapter.getId(), firstChapter.getId()));
//        // Save Lesson
//        LessonEntity lessonSaved = save(lesson);
//        return lessonSaved;
//    }
//
//    @Override
//    public void checkExistId(Long id) {
//        boolean isExists = lessonService.isExists(id);
//        if (!isExists)
//            throw new ErrorMessageException(String.format(this.messageSource.getMessage("NotFound", null, null),
//                    "Lesson", "id", String.valueOf(id)), TypeError.NotFound);
//    }
//
//    @Override
//    public LessonEntity setModifiedAndProperty(Long id, LessonCreateDTO dto) {
//        LessonEntity lesson = this.lessonService.findById(id);
//        // map dto
//        lesson.setModifiedDate(DateTime.getInstances());
//        lesson.setModifiedBy(UserContext.getId());
//        this.lessonMapper.mapToDTOUpdate(lesson, dto);
//        mapToUpdateQuestinGroup(dto.getQuestionGroup(), lesson);
//        if (lesson.getQuestionGroup().getId() == 0) {
//            QuestionGroupEntity questionGroup = lesson.getQuestionGroup();
//            questionGroup.setLesson(lesson);
//            QuestionGroupEntity questionGroupSaved = this.questionGroupRepository.save(questionGroup);
//            lesson.setQuestionGroup(questionGroupSaved);
//        }
//        // set zorder
//        Long zOrder = dto.getZorder();
//        if (zOrder != null)
//            lesson.setZOrder(zOrder);
//        return lesson;
//    }
//
//    private LessonEntity save(LessonEntity lesson) {
//        lesson.setCreatedDate(DateTime.getInstances());
//        lesson.setModifiedDate(DateTime.getInstances());
//        lesson.setCreatedBy(UserContext.getId());
//        lesson.setModifiedBy(UserContext.getId());
//        // Set time Question Group
//        lesson.getQuestionGroup().setCreatedDate(DateTime.getInstances());
//        lesson.getQuestionGroup().setModifiedDate(DateTime.getInstances());
//        lesson.getQuestionGroup().setCreatedBy(lesson.getCreatedBy());
//        lesson.getQuestionGroup().setModifiedBy(lesson.getModifiedBy());
//        lesson.getQuestionGroup().setLesson(lesson);
//        LessonEntity lessonSaved = this.lessonRepository.save(lesson);
//        //save Source if have
//        return this.lessonRepository.save(lesson);
//    }
//
//    private Long setOrder(Long id, Long chapterId) {
////        if (chapterId == null) // for no selected chapter
////            return null;
////        Long max = this.lessonRepository.getMaxZOrderByChapter(chapterId);
////        if(max == null)
////            return Long.valueOf(1);
////        return max + 1;
//    	return null;
//    }
//
//    @Override
//    public String makeFindLessonQueryWithRequestDAO(RequestLessonDTO requestDTO) {
//        String query = "from LessonEntity L ";
//        // Get lessons by chapter
//        if (requestDTO.getChapterId() != null) {
//            query = generateQueryByChapter(requestDTO, query);
//        }
//        // Get lessons by status
//        if (requestDTO.getStatus() != null) {
//            query = generateQueryByStatus(requestDTO, query);
//        }
//        // Get lessons by priority
//        if (requestDTO.getPriority() != null) {
//            query = generateQueryByPriority(requestDTO, query);
//        }
//        // Get lessons by explanation
//        if (requestDTO.getExplanation() != null) {
//            query = generateQueryByExplanation(requestDTO, query);
//        }
//        // Get lessons by practiceStatus
//        if (requestDTO.getPracticeStatus() != null) {
//            query = generateQueryByPracticeStatus(requestDTO, query);
//        }
//        // Get lessons by searchKeyWord
//        if (requestDTO.getSearchKeyWord() != null) {
//            query = generateQueryBySearchKeyWord(requestDTO, query);
//        }
//        // Get lessons by month
//        if (requestDTO.getMonth() != null) {
//            query = generateQueryByMonth(requestDTO, query);
//        }
//        // Get lessons by week
//        if (requestDTO.getWeekly() != null) {
//            query = generateQueryByWeekly(requestDTO, query);
//        }
//        // Get lessons with Order
//        if (requestDTO.getSort() != null) {
//            query = generateQueryByOder(requestDTO, query);
//        }
//        System.out.print(query);
//        return query;
//    }
//
//    @Override
//    public String generateQueryByChapter(RequestLessonDTO requestDTO, String query) {
//        List<String> chapterCodes = new ArrayList<>();
//        // Create string query
//        String chapterQuery = "";
//        if (requestDTO.getChapterId() != null) {
//            if (requestDTO.getChapterId() == 0) {
//                chapterQuery = "L.id not in (select lc.lesson.id from LessonChapterEntity lc  )";
//            } else {
//                String chapterCode = chapterService.findById(requestDTO.getChapterId()).getCode();
//                List<String> codes = findCodeChild(chapterCode, chapterCodes);
//                // get by requesrDTO Code
//                chapterQuery = "L.id in (select lc.lesson.id from LessonChapterEntity lc where lc.chapter.id = "
//                        + requestDTO.getChapterId();
//                // from 1 to skip requestDTO Code
//                for (int i = 1; i < codes.size(); i++) {
//
//                    chapterQuery = chapterQuery.concat(" or lc.chapter.code = '" + codes.get(i) + "'");
//                }
//                chapterQuery = chapterQuery.concat(")");
//
//            }
//
//        }
//        // Handle generate Query
//        if (query.contains("where")) {
//            query = query.concat(" and ").concat(chapterQuery);
//        } else {
//            query = query.concat("  where ").concat(chapterQuery);
//        }
//        return query;
//    }
//
//    // find CodeChilds of chapterCode in Request
//    @Override
//    public List<String> findCodeChild(String code, List<String> chapterCodes) {
//        chapterCodes.add(code);
//        List<String> codes = chapterService.getCodeByParentCode(code);
//        if (codes.size() > 0) {
//            for (int i = 0; i < codes.size(); i++) {
//                findCodeChild(codes.get(i), chapterCodes);
//            }
//            return chapterCodes;
//        } else {
//            return chapterCodes;
//        }
//    }
//
//    @Override
//    public String generateQueryByPriority(RequestLessonDTO requestDTO, String query) {
//        String priorityQuery = null;
//        final int ALL_MAKR = 0;
//        final int ALL_NOT_MARK = -1;
//        // Create string query
//        // Get by all mark
//        if (requestDTO.getPriority() == ALL_MAKR) {
//            priorityQuery = "L.id in (select LP.lesson.id from LessonPriorityEntity LP where LP.modifiedBy = " + UserContext.getId() + ")";
//        }
//        // get by all not mark
//        else if (requestDTO.getPriority() == ALL_NOT_MARK) {
//            priorityQuery = "L.id not in (select LP.lesson.id from LessonPriorityEntity LP where LP.modifiedBy = " + UserContext.getId() + ")";
//        }
//        // get by mark
//        else {
//            priorityQuery = " L.id in (select LP.lesson.id from LessonPriorityEntity LP where LP.priority = " + requestDTO.getPriority() + " and LP.modifiedBy = " + UserContext.getId() + " )";
//        }
//        // Handle generate Query
//        if (query.contains("where")) {
//            query = query.concat(" and ").concat(priorityQuery);
//        } else {
//            query = query.concat(" where ").concat(priorityQuery);
//        }
//        return query;
//    }
//
//    @Override
//    public String generateQueryByStatus(RequestLessonDTO requestDTO, String query) {
//        // Create string query
//        String statusQuery = " L.status = " + requestDTO.getStatus();
//        // Handle generate Query
//        if (query.contains("where")) {
//            query = query.concat(" and ").concat(statusQuery);
//        } else {
//            query = query.concat(" where ").concat(statusQuery);
//        }
//        return query;
//    }
//
//
//    @Override
//    public String generateQueryByShadowing(RequestLessonDTO requestDTO, String query) {
//        // Create string query
//        String shadowingQuery = " L.id in (select LM.lesson.id from LessonMediaEntity LM where LM.type = 'shadowing' ) ";
//        // Handle generate Query
//        if (query.contains("where")) {
//            query = query.concat(" and ").concat(shadowingQuery);
//        } else {
//            query = query.concat(" where ").concat(shadowingQuery);
//        }
//        return query;
//    }
//
//    @Override
//    public String generateQueryByExplanation(RequestLessonDTO requestDTO, String query) {
//        // Create string query
//        String explanationQuestionSolutionQuery = " L.id in (select QG.lesson.id from QuestionGroupEntity QG where QG.id in (select Q.questionGroup.id from QuestionEntity Q where Q.id in (select QS.question.id from QuestionSolutionEntity QS where QS.explanation is  ";
//        String explanationQuestionQuery = " L.id in (select QG.lesson.id from QuestionGroupEntity QG where QG.id in (select Q.questionGroup.id from QuestionEntity Q where Q.explanation is  ";
//        String explanationLessonQuery = " L.explanation is ";
//
//        // Handle generate Query
//        if (query.contains("where")) {
//            if (requestDTO.getExplanation()) {
//                query = query.concat(" and ").concat(explanationQuestionSolutionQuery)
//                        + " not null and  QS.explanation != '' )))";
//                query = query.concat(" and ").concat(explanationQuestionQuery)
//                        + " not null and  Q.explanation != '' ))";
//                query = query.concat(" and ").concat(explanationLessonQuery) + " not null ";
//            } else {
//                query = query.concat(" and ").concat(explanationQuestionSolutionQuery)
//                        + " null or  QS.explanation = '' )))";
//                query = query.concat(" or ").concat(explanationQuestionQuery) + " null or  Q.explanation = '' ))";
//                query = query.concat(" or ").concat(explanationLessonQuery) + " null ";
//            }
//        } else {
//            if (requestDTO.getExplanation()) {
//                query = query.concat(" where ").concat(explanationQuestionSolutionQuery)
//                        + " not null and  QS.explanation != '' )))";
//                query = query.concat(" or ").concat(explanationQuestionQuery) + " not null and Q.explanation != ''))";
//                query = query.concat(" or ").concat(explanationLessonQuery) + " not null ";
//            } else {
//                query = query.concat(" where ").concat(explanationQuestionSolutionQuery)
//                        + " null or  QS.explanation = '' )))";
//                query = query.concat(" or ").concat(explanationQuestionQuery) + " null or  Q.explanation = '' ))";
//                query = query.concat(" or ").concat(explanationLessonQuery) + " null ";
//            }
//        }
//        return query;
//    }
//
//    @Override
//    public String generateQueryByPracticeStatus(RequestLessonDTO requestDTO, String query) {
//        // Create string query
//        String practiceStatusQuery = " (select QG.lesson.id from QuestionGroupEntity QG where QG.id in (select  Q.questionGroup.id from QuestionEntity Q where Q.id in (select US.question.id from UserScoreEntity US where US.user =  ";
//        // Handle generate Query
//        if (requestDTO.getPracticeStatus() == true) {
//            if (query.contains("where")) {
//                query = query.concat(" and ").concat("L.id in ").concat(practiceStatusQuery) + UserContext.getId()
//                        + " )))";
//            } else {
//                query = query.concat(" where ").concat("L.id in ").concat(practiceStatusQuery) + UserContext.getId()
//                        + " )))";
//            }
//        } else {
//            if (query.contains("where")) {
//                query = query.concat(" and ").concat("L.id not in ").concat(practiceStatusQuery) + UserContext.getId()
//                        + " )))";
//            } else {
//                query = query.concat(" where ").concat("L.id not in ").concat(practiceStatusQuery) + UserContext.getId()
//                        + " )))";
//            }
//        }
//        return query;
//    }
//
//    @Override
//    public String generateQueryBySearchKeyWord(RequestLessonDTO requestDTO, String query) {
//        // Create string query
//        String zOrderQuery = " L.zOrder = " + requestDTO.getSearchKeyWord();
//        String titleQuery = "";
//        if (requestDTO.getSearchKeyWord() != null) {
//            titleQuery = " ( L.title like '%" + requestDTO.getSearchKeyWord().trim() + "%'" + " or L.content like '%" + requestDTO.getSearchKeyWord().trim() + "%' )";
//        }
//
//        // Handle generate Query
//        if (query.contains("where")) {
//            if (isInteger(requestDTO.getSearchKeyWord())) {
//                query = query.concat(" and ").concat(zOrderQuery);
//            } else {
//                query = query.concat(" and ").concat(titleQuery);
//            }
//        } else {
//            if (isInteger(requestDTO.getSearchKeyWord())) {
//                query = query.concat("  where ").concat(zOrderQuery);
//            } else {
//                query = query.concat("  where ").concat(titleQuery);
//            }
//        }
//        return query;
//    }
//
//
//    @Override
//    public String generateQueryByOder(RequestLessonDTO requestDTO, String query) {
//        final int sortDate = 1;
//        final int sortZOder = 2;
//        final int sortTitle = 3;
//        // Create string query
//        String sortByZOrderQuery = " GROUP BY L.zOrder, L.id, L.content, L.createdBy, L.createdDate, L.modifiedDate, L.modifiedBy, L.description, "
//                + "L.explanation, L.internalNotes, L.preparationTime, L.duration, L.shared, L.status, L.languageId, L.lessonSourceMediaLinkShadow, "
//                + "L.lessonSourceMediaLinkVideo, L.lessonSourceMediaLinkImage, L.userIdAsReviewer, L.userIdAsAppover, L.title  Order by L.zOrder DESC ";
//        String sortByCreateDateQuery = " GROUP BY L.zOrder, L.id, L.content, L.createdBy, L.createdDate, L.modifiedDate, L.modifiedBy, L.description,"
//                + " L.explanation, L.internalNotes, L.preparationTime, L.duration, L.shared, L.status, L.languageId, L.lessonSourceMediaLinkShadow, "
//                + "L.lessonSourceMediaLinkVideo, L.lessonSourceMediaLinkImage, L.userIdAsReviewer, L.userIdAsAppover, L.title  Order by L.modifiedDate DESC ";
//        String sortByTitleQuery = " GROUP BY L.zOrder, L.id, L.content, L.createdBy, L.createdDate, L.modifiedDate, L.modifiedBy, L.description,"
//                + " L.explanation, L.internalNotes, L.preparationTime, L.duration, L.shared, L.status, L.languageId, L.lessonSourceMediaLinkShadow, "
//                + "L.lessonSourceMediaLinkVideo, L.lessonSourceMediaLinkImage, L.userIdAsReviewer, L.userIdAsAppover, L.title  Order by L.title ASC ";
//
//        // Handle generate Query
//        if (requestDTO.getSort() == sortDate) {
//            query = query.concat(sortByCreateDateQuery);
//        }
//        if (requestDTO.getSort() == sortZOder) {
//            query = query.concat(sortByZOrderQuery);
//        }
//        if (requestDTO.getSort() == sortTitle) {
//            query = query.concat(sortByTitleQuery);
//        }
//        return query;
//    }
//
//    @Override
//    public String generateQueryByWeekly(RequestLessonDTO requestDTO, String query) {
//        // Create firstDayOfWeek
//        LocalDate firstDayOfWeek = LocalDate.now();
//        int thisWeek = LocalDate.now().get(WeekFields.of(Locale.getDefault()).weekOfMonth());
//        while (firstDayOfWeek.get(WeekFields.of(Locale.getDefault()).weekOfMonth()) == thisWeek) {
//            firstDayOfWeek = firstDayOfWeek.minusDays(1);
//        }
//        // Create lastDayOfWeek
//        LocalDate lastDayOfWeek = LocalDate.now();
//        while (lastDayOfWeek.get(WeekFields.of(Locale.getDefault()).weekOfMonth()) == thisWeek) {
//            lastDayOfWeek = lastDayOfWeek.plusDays(1);
//        }
//        lastDayOfWeek = lastDayOfWeek.minusDays(1);
//     
//        return query;
//    }
//
//    //Check string is Integer
//    @Override
//    public boolean isInteger(String s) {
//        if (s == null || s.equals("")) {
//            return false;
//        }
//        try {
//            Integer.parseInt(s);
//            return true;
//        } catch (NumberFormatException e) {
//
//        }
//        return false;
//    }
//
//    @Override
//    public void mapToUpdateQuestinGroup(QuestionGroupEntity questionGroupDTO, LessonEntity lesson) {
//        List<QuestionGroupEntity> questionGroupEntities = (List<QuestionGroupEntity>) lesson.getQuestionGroups();
////        if (questionGroupEntity == null) {
////            questionGroupEntity = new QuestionGroupEntity();
////            questionGroupEntity.setCreatedBy(UserContext.getId());
////            questionGroupEntity.setModifiedBy(UserContext.getId());
////            questionGroupEntity.setCreatedDate(DateTime.getInstances());
////            questionGroupEntity.setModifiedDate(DateTime.getInstances());
////        }
////        questionGroupEntity.setTitle(questionGroupDTO.getTitle());
////        questionGroupEntity.setDescription(questionGroupDTO.getDescription());
////        lesson.setQuestionGroup(questionGroupEntity);
//    }

}
