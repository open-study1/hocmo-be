package com.edu.openstudy.config.auth;

import java.util.ArrayList;
import java.util.List;

public class CorsConstant {
    private final static List<String> origins = new ArrayList<>();
    private final static List<String> originPatterns = new ArrayList<>();
    static {
        origins.add("http://localhost:3000");
       
    }
    public static List<String> getOrigins()
    {
        return origins;
    }
    public static List<String> getOriginPatterns()
    {
        return originPatterns;
    }
}
