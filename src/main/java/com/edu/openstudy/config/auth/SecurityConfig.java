package com.edu.openstudy.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.edu.openstudy.common.JwtAuthFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig  {
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
    private JwtAuthFilter authFilter;
    
	@Bean
	public BCryptPasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder(BCryptVersion.$2A,10);
	}
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//	    return http
//	            .csrf(AbstractHttpConfigurer::disable)
//	            .authorizeHttpRequests( auth -> auth
//	                    .requestMatchers("*").permitAll()
//	                    .anyRequest().authenticated()
//	            )
//	            .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//	            .httpBasic(withDefaults())
////	            .addFilterBefore(authenticationJwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
//	            //.addFilterAfter(authenticationJwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
//	            .build();		
		
//	    http.authorizeRequests(authorizeRequests -> authorizeRequests.anyRequest()
//	    	      .anonymous())
//	    	      .csrf(AbstractHttpConfigurer::disable);
		
//    return http.build();
		return http.csrf(csrf -> csrf.disable())
		        .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
		        .authorizeHttpRequests(auth -> 
		          auth.requestMatchers("/api/login").permitAll()
		              .requestMatchers("/api/*").permitAll()
		              .requestMatchers("/api/user/*").permitAll()
		              .requestMatchers("/api/user/send-otp/*").permitAll()
		              .requestMatchers("/api/user/verifymail/*").permitAll()
		              .requestMatchers("/api/user/register").permitAll()
		              .requestMatchers("/api/exam/*").permitAll()
		              .requestMatchers("/api/exercise/*").permitAll()
		              .requestMatchers("/api/exam/*/*").permitAll()
		              .requestMatchers("/api/question/*").permitAll()
		              .requestMatchers("/api/exam/question/*").permitAll()
		          	.anyRequest().authenticated()
		              
		        )
		       
		        .authenticationProvider(authenticationProvider()) 
		        .addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class)
		        .build();
	}
	
	   @Bean
	   public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
	       return authenticationConfiguration.getAuthenticationManager();
	   }
	   
	    @Bean
	    public AuthenticationProvider authenticationProvider() { 
	        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider(); 
	        authenticationProvider.setUserDetailsService(userDetailsService); 
	        authenticationProvider.setPasswordEncoder(passwordEncoder()); 
	        return authenticationProvider; 
	    } 

	   @Bean
	   public WebMvcConfigurer corsConfigurer() {
	       return new WebMvcConfigurer() {
	           @Override
	           public void addCorsMappings(CorsRegistry registry) {
	               registry.addMapping("/**")
	                       .allowedMethods("*");
	           }
	       };
	   }
     
}
