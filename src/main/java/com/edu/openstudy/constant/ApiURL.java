package com.edu.openstudy.constant;

public class ApiURL {
    /*
     * API URL GENERAL
     */
    public static final String URI_API = "api/";
    /*
     * API URL Chapter
     */
    public static final String CHAPTER = URI_API + Constant.CHAPTER; /* api/chapter */
    public static final String QUESTION = URI_API + Constant.QUESTION;
    public static final String EXERCISE = URI_API + Constant.EXERCISE;
    public static final String USER = URI_API + Constant.USER;
    public static final String EXAM = URI_API + Constant.EXAM;
    public static final String TOKEN = URI_API + Constant.TOKEN;
    public static final String DISCUSSION = URI_API + Constant.DISCUSSION;
    public static final String DISCUSSION_REACTIONS = URI_API + Constant.DISCUSSION_REACTIONS;
    public static final String LESSON = URI_API + Constant.LESSON;
    public static final String LESSON_PRIORITY = URI_API + Constant.LESSON_PRIORITY;
    public static final String USER_SCORE = URI_API + Constant.USER_SCORE;
    public static final String CLASS = URI_API + Constant.CLASS;

}
