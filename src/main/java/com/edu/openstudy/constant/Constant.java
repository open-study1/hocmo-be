package com.edu.openstudy.constant;


import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Constant {
	public static final String SUCCESS = "success";
	public static final String CREATE_SUCCESS = "create new item success";
	public static final String DELETE_SUCCESS = "delete item success";
	public static final String CHAPTER = "chapter";
	public static final String QUESTION = "question";
	public static final String EXERCISE = "exercise";
	public static final String EXAM = "exam";
	public static final String TOKEN ="token";
	public static final String DISCUSSION = "discussion";
	public static final String DISCUSSION_REACTIONS = "discussion-reactions";
	public static final String LESSON = "lesson";
	public static final String LESSON_PRIORITY = "lesson-priority";
	public static final String USER_SCORE = "user-score";
	public static final String USER = "user";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
	public static final String CLASS = "class";
	public static final Integer PAGE_SIZE_DEFAULT = 10;
	public static final Integer PAGE_NUMBER_DEFAULT = 0;
	public static final String CODE = "code";
	public static final int ALL_MAKR = 0;
	public static final int ALL_NOT_MARK = -1;
	public static final int SORT_BY_DATE = 1;
	public static final int SORT_BY_ZODER = 2;
	public static final int SORT_BY_TITLE = 3;
}

