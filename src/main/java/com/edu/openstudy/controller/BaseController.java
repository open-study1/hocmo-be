package com.edu.openstudy.controller;

import com.edu.openstudy.constant.Constant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.edu.openstudy.data.result.ResultJson;

public class BaseController {

	protected ResponseEntity<ResultJson> setResponseEntity(Object data) {
		return ResponseEntity.ok(new ResultJson(data));
	}
	
	protected ResponseEntity<ResultJson> setResponseEntityForbidden(Object data) {
		ResultJson rs = new ResultJson();
		rs.setCode(403);
		rs.setMessage(data.toString());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(rs);
	}
	protected ResponseEntity<ResultJson> setResponseEntityError(Object data) {
		ResultJson rs = new ResultJson();
		rs.setCode(500);
		rs.setMessage(data.toString());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(rs);
	}

	protected ResponseEntity<ResultJson> setResponseEntityCreated(Object data) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(new ResultJson(HttpStatus.CREATED.value(), Constant.CREATE_SUCCESS, data));
	}

}
