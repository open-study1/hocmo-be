package com.edu.openstudy.controller;

import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiURL.CHAPTER)
public class ChapterController extends BaseController {
    @Autowired
    private ChapterService chapterService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return setResponseEntity(chapterService.getAll());
    }

    @GetMapping(value ="parentcode")
    public ResponseEntity<?> getAllByParentCode(@RequestParam("code") String parentCode) {
        return setResponseEntity(chapterService.getByParentCode(parentCode));
    }
    
    @GetMapping("{id}")
    public ResponseEntity<?> findId(@PathVariable Long id) {
        return setResponseEntity(chapterService.findById(id));
    }
    
    
    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody ChapterDTO chapterDTO) {
        return setResponseEntityCreated(chapterService.save(chapterDTO));
    }

    @PutMapping("{id}")
    public ResponseEntity<?> update(@RequestBody ChapterDTO chapterDTO, @PathVariable Long id) {
        return setResponseEntity(chapterService.update(id, chapterDTO));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable(required = false) Long id) {
		return setResponseEntity("");
    }
}
