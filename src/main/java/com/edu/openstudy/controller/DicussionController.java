package com.edu.openstudy.controller;

import com.edu.openstudy.constant.ApiURL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.data.dto.DiscussionCreateDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.service.DiscussionService;

@RestController
@RequestMapping(ApiURL.DISCUSSION)
public class DicussionController extends BaseController {
	@Autowired
	private DiscussionService discussionService;

	@Autowired
	private MessageSource messageSource;
	private String messageResponse = "";

	@GetMapping("lesson/{lessonId}")
	public ResponseEntity<?> findAllDiscussionByLesson(@PathVariable long lessonId, @RequestParam("page") int page,
			@RequestParam("limit") int limit) {
		return setResponseEntity(this.discussionService.findAllDiscussionByLessonId(lessonId, page, limit));
	}

	@GetMapping("board/lesson/{lessonId}")
	public ResponseEntity<?> findAllBoardByLesson(@PathVariable long lessonId, @RequestParam("page") int page,
			@RequestParam("limit") int limit) {
		return setResponseEntity(this.discussionService.findAllDiscussionIncludeAnswerByLessonId(lessonId, null, page,limit));
	}

	@GetMapping("me/lesson/{lessonId}")
	public ResponseEntity<?> findAllMedByLesson(@PathVariable long lessonId, @RequestParam("page") int page,
			@RequestParam("limit") int limit) {
		return setResponseEntity(this.discussionService.findAllDiscussionIncludeAnswerByLessonId(lessonId,
				UserContext.getId(), page, limit));
	}
	@GetMapping("me/lesson/{lessonId}/{userId}")
	public ResponseEntity<?> findAllMedByLesson(@PathVariable long lessonId, @PathVariable long userId) {
		return setResponseEntity(this.discussionService.findAllDiscussionIncludeAnswerByLessonId(lessonId,
				userId, 0, 10));
	}

	@PostMapping("")
	public ResponseEntity<?> saveDiscussion(@RequestBody DiscussionCreateDTO discussionDTO) {
		DateTime.setTimeNow();
		this.discussionService.save(discussionDTO);
		messageResponse = String.format(this.messageSource.getMessage("createSuccessfully", null, null), "Discussion");
		return setResponseEntityCreated(messageResponse);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateDiscussion(@RequestBody DiscussionCreateDTO discussionDTO, @PathVariable long id) {
		DateTime.setTimeNow();
		this.discussionService.update(discussionDTO, id);
		messageResponse = String.format(this.messageSource.getMessage("updateSuccessfully", null, null), "Discussion");
		return setResponseEntity(messageResponse);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteDiscussion(@PathVariable long id) {
		this.discussionService.delete(id);
		messageResponse = String.format(this.messageSource.getMessage("deleteSuccessfully", null, null), "Discussion");
		return setResponseEntity(messageResponse);
	}
}
