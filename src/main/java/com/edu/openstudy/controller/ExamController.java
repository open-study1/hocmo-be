package com.edu.openstudy.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.DoExamDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import com.edu.openstudy.service.ExamService;
import com.edu.openstudy.service.QuestionResponseUserService;
import com.edu.openstudy.service.QuestionService;
import com.edu.openstudy.service.UserScoreService;

@RestController
@RequestMapping(value= ApiURL.EXAM)
public class ExamController extends BaseController {
	@Autowired
	private ExamService examService;

	@Autowired
	private QuestionService questionService;
	@Autowired
	private UserScoreService userScoreService;
	@Autowired
	private QuestionResponseUserService questionResponseUserService;
    @GetMapping("{id}")
    public ResponseEntity<?> findId(@PathVariable Long id) {
        return setResponseEntity(examService.getById(id));
    }
    
    @GetMapping("/question")
    public ResponseEntity<?> findQuestionById(@RequestParam("exam") long examId,@RequestParam("order") int order) {
        return setResponseEntity(questionService.getByExamAndOrder(examId,order));
    }
    
    @GetMapping("")
    public ResponseEntity<?> findAll() {
        return setResponseEntity(examService.getAllExam());
    }
    
    @GetMapping("/admin")
    public ResponseEntity<?> findAllPagging(@RequestParam("page") int page) {
        return setResponseEntity(examService.findAllPaging(page,10));
    }
    
    
    @PostMapping("/doExam/{examId}")
    public ResponseEntity<?> doExam(@PathVariable Long examId, @RequestBody DoExamDTO doExamDTO) {
		LocalDateTime createDate = LocalDateTime.now();
		List<QuestionResponseUserEntity> responseUserEntities = questionResponseUserService.saveListV2(examId, doExamDTO.getQuestionResponseUserDoExamDTOs(), createDate);
        return setResponseEntityCreated(userScoreService.doScore(examId, createDate, responseUserEntities, doExamDTO.getDuration()));
    }
    
    @GetMapping("/history/{id}")
    public ResponseEntity<?> getDetail(@PathVariable Long id) {
        return setResponseEntityCreated(userScoreService.findById(id));
    }
    
    @GetMapping("/history")
    public ResponseEntity<?> getHistory() {
        return setResponseEntity(examService.getAllExamPracticed(UserContext.getId()));
    }

    @GetMapping("/statistics")
    public ResponseEntity<?> getStatistics() {
        return setResponseEntity(userScoreService.statisticsChapter(UserContext.getId()));
    }
    

//    @PutMapping("{id}")
//    public ResponseEntity<?> update(@RequestBody  QuestionDTO questionDTO, @PathVariable Long id) {
//        return setResponseEntity(questionService.update(id, chapterDTO));
//    }
//
//    @DeleteMapping("{id}")
//    public ResponseEntity<?> deleteById(@PathVariable(required = false) Long id) {
//		return setResponseEntity("");
//    }
}
