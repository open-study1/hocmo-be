package com.edu.openstudy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDTO;
import com.edu.openstudy.service.ExamService;
import com.edu.openstudy.service.ExerciseService;
import com.edu.openstudy.service.QuestionResponseUserService;
import com.edu.openstudy.service.QuestionService;



@RestController
@RequestMapping(value= ApiURL.EXERCISE)
public class ExerciseController extends BaseController{
	@Autowired
	private ExerciseService exerciseService;

	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private QuestionResponseUserService questionResponseUserService;
	
    @GetMapping("{id}")
    public ResponseEntity<?> findId(@PathVariable Long id) {
        return setResponseEntity(exerciseService.getExerciseById(id));
    }
    
    @GetMapping("/question/{id}")
    public ResponseEntity<?> findQuestionById(@PathVariable Long id) {
        return setResponseEntity(questionService.findById(id));
    }
    
    @GetMapping("/admin")
    public ResponseEntity<?> findAllPagging(@RequestParam("page") int page) {
        return setResponseEntity(exerciseService.findAllPaging(page,10));
    }
    
    @GetMapping("")
    public ResponseEntity<?> findByLesson(@RequestParam(name = "lessonId", required = true) String lessonId) {
        return setResponseEntity(exerciseService.getExerciseByLessonId(Long.parseLong(lessonId)));
    }
    
    @PostMapping("/doExercise")
    public ResponseEntity<?> doExercise(@RequestBody QuestionResponseUserDTO questionResponseUserDTO) {
        return setResponseEntityCreated(questionResponseUserService.save(questionResponseUserDTO));
    }
//
//    @PutMapping("{id}")
//    public ResponseEntity<?> update(@RequestBody  QuestionDTO questionDTO, @PathVariable Long id) {
//        return setResponseEntity(questionService.update(id, chapterDTO));
//    }
//
    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable(required = false) Long id) {
		return setResponseEntity("");
    }

}
