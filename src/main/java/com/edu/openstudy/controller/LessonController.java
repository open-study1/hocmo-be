package com.edu.openstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.request.RequestLessonDTO;
import com.edu.openstudy.service.LessonService;

@RestController
@RequestMapping(value= ApiURL.LESSON)
@CrossOrigin(origins = "*", maxAge = 3600)
public class LessonController extends BaseController {
	@Autowired
	private LessonService lessonService;
	@Autowired
	private MessageSource messageSource;
	private final String entity = "Lesson";
	private String messageResponse = "";

	@GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDetail(@PathVariable Long id) {
		return setResponseEntity(lessonService.getDetailById(id));
	}

	@PostMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAll(@RequestBody RequestLessonDTO requestDTO) {
		PaginationDTO lessonViewDTO = new PaginationDTO();
		lessonViewDTO = this.lessonService.findByRequestLessonDTO(requestDTO);
		return setResponseEntity(lessonViewDTO);
	}

	@PostMapping(value = "", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> create(@RequestPart(name = "lesson", required = true) String lesson,
			@RequestPart(name = "image", required = false) MultipartFile image) {
		LessonCreateDTO lessonCreate = this.lessonService.readJson(lesson, image);
		DateTime.setTimeNow();
		LessonCreateDTO lessonDto = this.lessonService.save(lessonCreate);
		messageResponse = String.format(this.messageSource.getMessage("createSuccessfully", null, null), entity);
		return setResponseEntityCreated(lessonDto);
	}

	@PutMapping(value = "{id}", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> update(@RequestPart(name = "lesson", required = true) String lesson,
			@RequestPart(name = "image", required = false) MultipartFile image,@PathVariable Long id) {
		LessonCreateDTO lessonCreate = this.lessonService.readJson(lesson, image);
		DateTime.setTimeNow();
		this.lessonService.update(lessonCreate, id);
		messageResponse = String.format(this.messageSource.getMessage("updateSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}

	@PutMapping(value = "{id}/active", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> active(@PathVariable Long id) {
		DateTime.setTimeNow();
		this.lessonService.active(id);
		messageResponse = String.format(this.messageSource.getMessage("activeSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}

	@PutMapping(value = "{id}/disable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> disable(@PathVariable Long id) {
		DateTime.setTimeNow();
		this.lessonService.disableById(id);
		messageResponse = String.format(this.messageSource.getMessage("disableSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}


	@DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteById(@PathVariable Long id) {
		this.lessonService.deleteById(id);
		messageResponse = String.format(this.messageSource.getMessage("deleteSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}

	@DeleteMapping(value = "force", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteByListId(@RequestParam(value = "ids", required = true) List<Long> ids) {
		this.lessonService.deleteByListId(ids);
		messageResponse = String.format(this.messageSource.getMessage("deleteSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}

	@PutMapping(value = "active", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> activeListId(@RequestParam(value = "id", required = false) List<Long> listIdLesson) {
		boolean statusActive = true;
		this.lessonService.setStatusListIdLesson(listIdLesson, statusActive);
		messageResponse = String.format(this.messageSource.getMessage("activeSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}

	@PutMapping(value = "disable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> disableListId(@RequestParam(value = "id", required = false) List<Long> listIdLesson) {
		boolean statusdisable = false;
		this.lessonService.setStatusListIdLesson(listIdLesson, statusdisable);
		messageResponse = String.format(this.messageSource.getMessage("disableSuccessfully", null, null), entity);
		return setResponseEntity(messageResponse);
	}
	@PutMapping(value = "reset-zorder")
	public ResponseEntity<?> resetAllZorder()
	{
		lessonService.resetAllZorder();
		return setResponseEntity(null);
	}
}
