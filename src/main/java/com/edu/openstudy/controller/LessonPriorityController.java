package com.edu.openstudy.controller;

import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.constant.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.data.dto.LessonPriorityDTO;
import com.edu.openstudy.service.LessonPriorityService;

@RestController
@RequestMapping(ApiURL.LESSON_PRIORITY)
public class LessonPriorityController extends BaseController{
	@Autowired
	private LessonPriorityService lessonPriorityService;

	@PostMapping("")
	public ResponseEntity<?> create(@RequestBody LessonPriorityDTO lessonPriorityDTO) {
		return setResponseEntityCreated(lessonPriorityService.creation(lessonPriorityDTO));
	}

	@DeleteMapping("/{lessonId}")
	public ResponseEntity<?> delete(@PathVariable Long lessonId) {
		this.lessonPriorityService.delete(lessonId);
		return setResponseEntity(Constant.DELETE_SUCCESS);
	}
}
