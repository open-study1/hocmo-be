package com.edu.openstudy.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.common.JwtService;
import com.edu.openstudy.config.auth.UserDetailsImpl;
import com.edu.openstudy.data.dto.JwtRespone;
import com.edu.openstudy.data.dto.LoginDTO;
import com.edu.openstudy.data.dto.UserDTO;
import com.edu.openstudy.data.entity.UserEntity;
import com.edu.openstudy.service.UserService;

@RestController
public class LoginController extends BaseController{
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtService jwtService; 
    @Autowired
    private UserService userService;

    public final Logger logger = LoggerFactory.getLogger("info");

    @PostMapping("api/login")
    public ResponseEntity<?> authenticateUser(@Validated @RequestBody LoginDTO loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();

        String jwt = jwtService.generateToken(user.getUsername());
        UserDTO userDto = userService.findByEmail(user.getEmail());
        if(!userDto.isVerifiedEmail()) {
        	 return setResponseEntityForbidden("Email has not been verified");
        }
        long id = userDto.getId();
        List<String> roles = user.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());
        logger.info("%s has successfully logged in.", user.getUsername());
        return ResponseEntity.ok(new JwtRespone(jwt, user.getUser().getName(), user.getEmail(), roles.get(0), userDto.getAvatar(), id));
    }

}
