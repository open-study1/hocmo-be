package com.edu.openstudy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.service.QuestionService;
@RestController
@RequestMapping(ApiURL.QUESTION)
public class QuestionController extends BaseController{
	@Autowired
	private QuestionService questionService;

	@GetMapping("")
	public ResponseEntity<?> findAll(@RequestParam("page") int page) {
		return setResponseEntity(questionService.findAllPaging(page,10));
	}
	
    @GetMapping("{id}")
    public ResponseEntity<?> findId(@PathVariable Long id) {
        return setResponseEntity(questionService.findById(id));
    }
    
    
    
    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody QuestionDTO questionDTO) {
        return setResponseEntityCreated(questionService.create(questionDTO));
    }
//
//    @PutMapping("{id}")
//    public ResponseEntity<?> update(@RequestBody  QuestionDTO questionDTO, @PathVariable Long id) {
//        return setResponseEntity(questionService.update(id, chapterDTO));
//    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable(required = false) Long id) {
    	questionService.deleteById(id);
		return setResponseEntity("Delete Success");
    }
}
