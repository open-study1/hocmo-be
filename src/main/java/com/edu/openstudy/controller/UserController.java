package com.edu.openstudy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.UserDTO;
import com.edu.openstudy.service.MailService;
import com.edu.openstudy.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping(value= ApiURL.USER)
public class UserController extends BaseController{

    @Autowired
    private UserService userService;
    @Autowired
    private MailService mailService;
    @Autowired
    private MessageSource messageSource;

    @PostMapping("")
    public UserDTO checkUser(@Valid @RequestBody UserDTO dto) {
        return this.userService.handlerValid(dto);
    }

    @PutMapping("")
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO dto) {
        return ResponseEntity.ok(this.userService.update(dto));
    }

    @PostMapping("/register")
    public ResponseEntity<?> create(@RequestBody UserDTO dto) {
        return setResponseEntity(this.userService.create(dto));
    }
    
    @PostMapping("/forgot-password")
    public ResponseEntity<?> forGotPassword(@RequestBody UserDTO dto) {
        return setResponseEntity(this.userService.create(dto));
    }
    @GetMapping("")
	public ResponseEntity<?> getAllUser(@RequestParam("page") int page) {
		return setResponseEntity(userService.findAllPaging(page, 10));
	}
    
	@GetMapping("{id}")
	public ResponseEntity<?> getUser(@PathVariable Long id) {
		return setResponseEntity(userService.findById(id));
	}
	
	@GetMapping("/send-otp/{id}")
	public ResponseEntity<?> sendOTP(@PathVariable Long id) {
		userService.sendOTPEmail(id);
		return setResponseEntity("Ok");
	}
	
	@GetMapping("/verifymail")
	public ResponseEntity<?> verifiedEmail( @RequestParam("user") long userId,  @RequestParam("otp") String otp) {
		if(userService.verifiedEmail(userId, otp)) {
			return setResponseEntity("Authentication successful");
		}
		 return setResponseEntityError("Authentication Fail");
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		userService.delete(id);
		return setResponseEntity("Ok");
	}

}
