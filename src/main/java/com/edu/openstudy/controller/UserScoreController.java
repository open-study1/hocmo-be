package com.edu.openstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.constant.ApiURL;
import com.edu.openstudy.data.dto.ParamUserScoreDTO;
import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.service.UserScoreService;

@RestController
@RequestMapping(ApiURL.USER_SCORE)
public class UserScoreController extends BaseController{
	@Autowired
	private UserScoreService userScoreService;
	@Autowired
	private MessageSource messageSource;
	private final String ENTITY = "User Score";

	@PostMapping("")
	public ResponseEntity<?> create(@RequestBody List<UserScoreDTO> userScoreDTOs) {
		DateTime.setTimeNow();
		return null;
	}

	@GetMapping("{lessonId}/{userId}")
	public ResponseEntity<?> get(@PathVariable Long lessonId, @PathVariable Long userId) {
		return null;
	}

	@PostMapping("smw/{lessonId}")
	public ResponseEntity<?> createSMW(@RequestBody List<UserScoreDTO> userScoreDTOs, @PathVariable Long lessonId) {
		DateTime.setTimeNow();
		return null;
	}

	@PostMapping("hiw/{lessonId}")
	public ResponseEntity<?> createHIW(@RequestBody List<UserScoreDTO> userScoreDTOs, @PathVariable Long lessonId) {
		DateTime.setTimeNow();
		return null;
	}

	@DeleteMapping("")
	public ResponseEntity<?> deleteByDateTime(@RequestParam String dateTime) {
		this.userScoreService.deleteByCreate(dateTime);
		String messageResponse = String.format(this.messageSource.getMessage("deleteSuccessfully", null, null), ENTITY);
		return setResponseEntity(messageResponse);
	}

	@PostMapping("param")
	public ResponseEntity<?> getByLesson(@RequestBody ParamUserScoreDTO paramUserScoreDTO) {
		DateTime.setTimeNow();
		return null;
	}
}
