package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BaseLessonDTO {
    private long id;
    private String title;
    private String content;
    private String description;
    private String internalNotes;
    private int preparationTime;
    private int duration;
    private Long zorder;
}
