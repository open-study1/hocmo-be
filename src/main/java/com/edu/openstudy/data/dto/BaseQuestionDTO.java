package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BaseQuestionDTO {
	private long id;
    private String content;
    private String explanation;
    private List<QuestionOptionDTO> questionOptions = new ArrayList<QuestionOptionDTO>();
    private QuestionSolutionDTO questionSolution = new QuestionSolutionDTO();
    private ChapterDTO chapter;
    public BaseQuestionDTO(long id) {
		super();
		this.id = id;
	}
    
    
}
