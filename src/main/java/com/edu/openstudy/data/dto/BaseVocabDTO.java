package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BaseVocabDTO {
    private long id;
    private String vocab;
    private Long countryId;
    private String ipa;
    private Boolean isPhrasalWord;
    private String definition;
    private String example;
}
