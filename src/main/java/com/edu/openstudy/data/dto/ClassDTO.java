package com.edu.openstudy.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClassDTO {
    private long id;
    private String code;
    private String name;
    private String startTime;
    private String endTime;
    private Long teacherId;
    private String invitationLink;
    private String description;

    public ClassDTO(String code, String name, Long teacherId, String startTime, String endTime) {
        this.code = code;
        this.name = name;
        this.teacherId = teacherId;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
