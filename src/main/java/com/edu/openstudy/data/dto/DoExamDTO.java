package com.edu.openstudy.data.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DoExamDTO {
	private long duration;
	private List<QuestionResponseUserDoExamDTO> questionResponseUserDoExamDTOs;

}
