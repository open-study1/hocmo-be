package com.edu.openstudy.data.dto;


import java.util.List;

import lombok.Data;

@Data
public class ExamHistoryDTO {
	private long id;
	private String title;
	private String description;
	private Boolean isShuffle;
	private int duration;
	private int quantity;
	private boolean isPraticed;
	private long  totalPracticed;
	private double  maxScore;
	private double  minScore;
	private double  mediumScore;
	private ChapterDTO chapterDTO;
	private List<UserScoreViewDTO> userScoreViewDTOs;
}
