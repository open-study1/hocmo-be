package com.edu.openstudy.data.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ExamViewDTO {
	private long id;
	private String title;
	private String description;
	private Boolean isShuffle;
	private int duration;
	private int quantity;
	private boolean isPraticed;
	private long  timePraticed;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime modifiedDate;
	private Long modifiedBy;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdDate;
	private Long createdBy;
	private ChapterDTO chapterDTO;
}
