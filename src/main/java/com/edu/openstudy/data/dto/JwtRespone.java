package com.edu.openstudy.data.dto;


import lombok.Data;

@Data
public class JwtRespone {
	private String token;
	private String type="Bearer";

	private String name;
	private String email;
	private String role;
	private String avatar;
	private long id;
	public JwtRespone(String accessToken,  String name, String email, String roles, String avatar, long id)
	{	this.id = id;
		this.token = accessToken;
		this.avatar = avatar;
		this.name = name;
		this.email = email;
		this.role = roles;
	}
}