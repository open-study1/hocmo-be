package com.edu.openstudy.data.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.edu.openstudy.data.entity.ExamEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LessonCreateDTO extends BaseLessonDTO {
    private ChapterDTO chapterDTO;
    private ExamEntity questionGroup;
    private String explanation;
    private String sourceMediaLinkVideo;
    private String sourceMediaLinkImage;
    private List<QuestionDTO> questionDTO;
    private MultipartFile image;
}
