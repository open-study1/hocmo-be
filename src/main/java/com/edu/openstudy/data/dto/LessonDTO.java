package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LessonDTO {
	private long id;
	private int languageId;
	private String title;
	private String content;
	private String description;
	private String internalNotes;
	private int preparationTime;
	private int duration;
	private boolean shared;
	private Long zorder;
}
