package com.edu.openstudy.data.dto;

import java.util.List;

import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.QuestionOptionEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LessonDetailDTO {
    private LessonCreateDTO lesson;
    private ExamEntity questionGroup;
    private List<QuestionEntity> question;
    private List<QuestionOptionEntity> questionOptions;
}
