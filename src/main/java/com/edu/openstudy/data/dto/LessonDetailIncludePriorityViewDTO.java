package com.edu.openstudy.data.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LessonDetailIncludePriorityViewDTO extends LessonViewDTO {
    private List<LessonPriorityViewDTO> priorities;
    private ExamDTO questionGroup;
    private List<LessonMediaDTO> lessonMediaDTOs;
}
