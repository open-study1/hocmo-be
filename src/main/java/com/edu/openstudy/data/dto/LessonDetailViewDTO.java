package com.edu.openstudy.data.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LessonDetailViewDTO extends LessonViewDTO {
    private ExamDTO questionGroup;
    private List<LessonMediaDTO> lessonMediaDTOs;
}
