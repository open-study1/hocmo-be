package com.edu.openstudy.data.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LessonViewDTO {
    private long id;
    private String title;
    private String content;
    private String description;
    private int duration;
    private boolean status;
    private Long idQuestionType;
    private long zOrder;
    private String explanation;
    private String lessonSourceMediaLinkVideo;
    private String lessonSourceMediaLinkImage;
    private ChapterDTO chapter;
    private List<LessonPriorityViewDTO> priorities;
    private long practiced;
    private Long userIdAsAppover;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifiedDate;
    private long modifiedBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;
    private long createdBy;
}

