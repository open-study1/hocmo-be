package com.edu.openstudy.data.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParamUserScoreDTO {
    private List<Long> questionIds;
    private Long userId;
    private String codeChapter;
}
