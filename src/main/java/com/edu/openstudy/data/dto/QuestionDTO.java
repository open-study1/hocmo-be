package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuestionDTO extends BaseQuestionDTO {

	public QuestionDTO(long id) {
		super(id);
	}

	private QuestionTypeDTO questionType;
    
    
}
