package com.edu.openstudy.data.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuestionExerciseDTO{
    private long id;
    private Integer order;
    private String content;
    private String explanation;
    private List<QuestionOptionViewDTO> questionOptions = new ArrayList<QuestionOptionViewDTO>();
    private QuestionSolutionDTO questionSolutions = new QuestionSolutionDTO();
	private QuestionTypeDTO questionType;
	private List<String> questionResponseUser;
	private boolean result ;

}
