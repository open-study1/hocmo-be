package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QuestionOptionViewDTO {
    private long id;
    private String code;
    private String content;
    private int order;
	private boolean checked ;
}
