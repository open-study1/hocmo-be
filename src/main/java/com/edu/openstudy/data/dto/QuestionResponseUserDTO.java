package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QuestionResponseUserDTO {
    private Long id;
    private String valueText;
    private QuestionDTO questionDTO;
    private ExamDTO examDTO;
    private ExerciseDTO exerciseDTO;
    private Long userId;

}
