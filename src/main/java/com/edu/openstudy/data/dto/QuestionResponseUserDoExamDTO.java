package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QuestionResponseUserDoExamDTO {
    private Long id;
    private String valueText;
    private long questionId;
    private long examId;
}
