package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QuestionResponseUserViewDTO {

    private Long id;
    private String valueText;
    private QuestionExerciseDTO questionDTO;
}
