package com.edu.openstudy.data.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QuestionSolutionDTO {
    private long id;
    private List<QuestionOptionDTO> solution = new ArrayList<>() ;
    private String solutionStr;

    private String explanation ;
}
