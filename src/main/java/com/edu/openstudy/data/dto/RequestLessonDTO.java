package com.edu.openstudy.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RequestLessonDTO {
	private Long userId;
	private Integer chapterCode;
	private Integer sort;
	private Integer priority;
	private Boolean practiceStatus;
	private Boolean status;
	private String searchKeyWord;
	private Integer pageNumber;
	private Integer limit ;
}
