package com.edu.openstudy.data.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@lombok.Data
public class ResponseDataDTO<Data> {
	private Integer status;
	private String message;
	private List<Data> data;
	private String version;
}
