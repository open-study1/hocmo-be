package com.edu.openstudy.data.dto;

import java.util.ArrayList;
import java.util.List;

import com.edu.openstudy.data.entity.LessonEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResultLessonQueryDTO {
	private List<LessonEntity>  lessons = new ArrayList<>();
	private Integer totalItems;

}
