package com.edu.openstudy.data.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatisticsDTO {
	private long id;
	private String name;
    private String code;
    private String parentCode;
    private long totalAnswers;
    private long rightAnswers;
    private long wrongAnswers;
    private long missingAnswers;
}
