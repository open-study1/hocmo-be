package com.edu.openstudy.data.dto;

import lombok.Getter;
import lombok.Setter;
//import org.springframework.security.core.context.SecurityContextHolder;

import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.config.auth.UserDetailsImpl;
import com.edu.openstudy.exception.ErrorMessageException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.context.SecurityContextHolder;

@Getter
@Setter
public class UserContext {
    private Long userId;
    private String userName;
    private List<String> roleNames = new ArrayList<>();

    public static String getUserName() {
    	UserDetailsImpl userDetail = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	return userDetail.getUsername() ;
    }
    public static List<String> getRoleNames()
    {
        List<String> roleNames = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().map(item -> item.getAuthority()).collect(Collectors.toList());
        roleNames.remove(0);
        return roleNames;
    }
    public static Long getId() {
        try {
        	UserDetailsImpl userDetail = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        	if (userDetail.getUser().getId() == 0) {
        		throw new ErrorMessageException("Forbidden", TypeError.Forbidden);
        	}
        	return userDetail.getUser().getId() ;
      	  
        } catch (Exception ex) {
            throw new ErrorMessageException("Forbidden", TypeError.Forbidden);
        }
    }

}
