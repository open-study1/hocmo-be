package com.edu.openstudy.data.dto;

import java.time.LocalDateTime;


import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDTO {

    private long id;
    
    private String name;
    
    private boolean isVerifiedEmail;
    
    private String email;
    
    private String phone;
        
    private String avatar;
    
    private boolean isStudent;

    private RoleDTO roleDTO ;
    
	@Size(min = 6,message = "{error.passwordRequire}")
	@NotEmpty(message = "{error.passwordNotNull}")
	private String password;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime modifiedDate;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdDate;
}
