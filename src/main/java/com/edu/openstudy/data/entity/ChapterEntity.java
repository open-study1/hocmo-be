package com.edu.openstudy.data.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "chapter")
public class ChapterEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String parentCode;
    @Column(length = 1000)
    private String name;
    @Column(length = 1000)
    private String code;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chapter", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private Collection<LessonEntity> lessonEntities = new HashSet<LessonEntity>();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chapter", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private Collection<ExamEntity> examEntities = new HashSet<ExamEntity>();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chapter", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private Collection<QuestionEntity> questionEntities = new HashSet<QuestionEntity>();

}
