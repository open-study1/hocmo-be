package com.edu.openstudy.data.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "exam")
public class ExamEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 500)
    private String title;
    @Column(length = 500)
    private String description;
    @Column(name = "duration")
    private int duration ;
    @Column(name = "quantity")
    private int quantity;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chapter_id")
    private ChapterEntity chapter;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "exam", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private Collection<QuestionExamEntity> questionExams = new HashSet<QuestionExamEntity>();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "exam", orphanRemoval = true)
    @JsonManagedReference
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private List<QuestionResponseUserEntity> questionResponseUsers = new ArrayList<QuestionResponseUserEntity>();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "exam", orphanRemoval = true)
    @JsonManagedReference
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private List<UserScoreEntity> userScores = new ArrayList<UserScoreEntity>();
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(id);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamEntity other = (ExamEntity) obj;
		return id == other.id;
	}


}
