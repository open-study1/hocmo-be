package com.edu.openstudy.data.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "lesson")
public class LessonEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 100)
    private String title;
    @Column(columnDefinition="TEXT")
    private String content;
    private boolean status;
    @Column(name = "lesson_source_media_link_video", length = 500)
    private String lessonSourceMediaLinkVideo;
    @Column(name = "lesson_source_media_link_image", length = 500)
    private String lessonSourceMediaLinkImage;
    @Column(name = "reviewed_by")
    private Long userIdAsAppover;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chapter_id")
    private ChapterEntity chapter;

    @OneToOne(mappedBy = "lesson", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private ExerciseEntity exercise;
    
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson", orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JsonManagedReference
    @JsonIgnore
    private Collection<LessonMediaEntity> lessonMedias = new HashSet<LessonMediaEntity>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson", orphanRemoval = true)
    @JsonIgnore
    private Collection<DiscussionEntity> discussions = new HashSet<DiscussionEntity>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson", orphanRemoval = true)
    @JsonIgnore
    private Collection<LessonPriorityEntity> lessonPriorities = new HashSet<LessonPriorityEntity>();
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(id);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LessonEntity other = (LessonEntity) obj;
		return id == other.id;
	}
}
