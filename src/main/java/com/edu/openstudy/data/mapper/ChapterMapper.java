package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.StatisticsDTO;
import com.edu.openstudy.data.entity.ChapterEntity;

public interface ChapterMapper {

	ChapterDTO mapToDTO(ChapterEntity chapter);
	
	ChapterEntity map(ChapterDTO chapterDTO);

	
	void mapToUpdate(ChapterDTO chapterDTO, ChapterEntity chapter);
	
	StatisticsDTO mapToStatisticsDTO(ChapterEntity chapter);

}
