package com.edu.openstudy.data.mapper;

import org.mapstruct.Mapper;

import com.edu.openstudy.data.dto.DiscussionCreateDTO;
import com.edu.openstudy.data.dto.DiscussionDTO;
import com.edu.openstudy.data.dto.DiscussionSubDTO;
import com.edu.openstudy.data.dto.DiscussionViewDTO;
import com.edu.openstudy.data.entity.DiscussionEntity;
public interface DiscussionMapper {
	DiscussionEntity map(DiscussionDTO discussionDTO);
	
	DiscussionEntity map(DiscussionCreateDTO discussionDTO);

	DiscussionDTO map(DiscussionEntity discussion);
	
	DiscussionViewDTO mapToViewDTO(DiscussionEntity discussion);
	
	DiscussionSubDTO mapToSubDTO(DiscussionEntity discussion);
	
}
