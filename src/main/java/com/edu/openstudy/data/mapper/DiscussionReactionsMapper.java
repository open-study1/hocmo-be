package com.edu.openstudy.data.mapper;

import org.mapstruct.Mapper;

import com.edu.openstudy.data.dto.DiscussionReactionsCreateDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsViewDTO;
import com.edu.openstudy.data.entity.DiscussionReactionsEntity;

public interface DiscussionReactionsMapper {
	DiscussionReactionsEntity map(DiscussionReactionsDTO discussionReactionsDTO);
	
	DiscussionReactionsEntity map(DiscussionReactionsCreateDTO discussionReactionsCreateDTO);

	DiscussionReactionsDTO map(DiscussionReactionsEntity discussionReactions);
	
	DiscussionReactionsViewDTO mapToView(DiscussionReactionsEntity discussionReactions);

}
