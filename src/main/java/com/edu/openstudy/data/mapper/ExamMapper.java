package com.edu.openstudy.data.mapper;

import java.time.LocalDateTime;

import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExamHistoryDTO;
import com.edu.openstudy.data.dto.ExamViewDTO;
import com.edu.openstudy.data.dto.ExamViewDetailDTO;
import com.edu.openstudy.data.entity.ExamEntity;

public interface ExamMapper {
	ExamEntity mapToEntity(ExamDTO examDTO);

	ExamDTO mapToDTO(ExamEntity examEntity);
	
	ExamViewDTO mapToViewDTO(ExamEntity examEntity);
	
	ExamViewDetailDTO mapToViewDetailDTO(ExamEntity examEntity, long userId, LocalDateTime createdDate);

	void mapToUpdate(ExamEntity examEntity, ExamEntity questionGroupEntity);

	ExamHistoryDTO mapToHistoryDTO(ExamEntity entity);
}
