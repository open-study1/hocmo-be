package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.ExerciseEntity;

public interface ExerciseMapper {

	ExerciseDTO mapToDTO(ExerciseEntity exercise,long userId);
	
	ExerciseDTO mapViewToDTO(ExerciseEntity exercise);

	ExerciseEntity mapToEntity(ExamDTO questionGroupDTO);

}
