package com.edu.openstudy.data.mapper;

import org.mapstruct.Mapper;

import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.LessonDTO;
import com.edu.openstudy.data.dto.LessonDetailViewDTO;
import com.edu.openstudy.data.dto.LessonViewDTO;
import com.edu.openstudy.data.entity.LessonEntity;

public interface LessonMapper {

	LessonEntity mapToEntity(LessonCreateDTO lessonCreateDTO);
	
	LessonViewDTO mapToViewDTO(LessonEntity lesson);
	
	LessonViewDTO mapToViewIncludePriorityDTO(LessonEntity lesson);
	
	LessonDetailViewDTO mapToViewDetailDTO(LessonEntity lessonDTO);
	
	LessonDetailViewDTO mapToViewDetailIncludePriorityDTO(LessonEntity lesson);
	
	LessonCreateDTO mapToDTO(LessonEntity lessonDTO);

	void mapToDTOUpdate(LessonEntity lesson, LessonCreateDTO lessonCreateDTO);

	LessonDTO map(LessonEntity lesson);

	LessonEntity mapToEntity(LessonDTO lessonDTO);

}
