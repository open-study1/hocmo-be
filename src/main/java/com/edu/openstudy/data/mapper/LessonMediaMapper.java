package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.LessonMediaDTO;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonMediaEntity;

public interface LessonMediaMapper {

	LessonMediaEntity mapToEntity(LessonMediaDTO dto);

	LessonMediaDTO mapToDTO(LessonMediaEntity entity);

    void mapToUpdate(LessonMediaEntity lessonMedia, LessonMediaDTO dto, LessonEntity lesson);
}
