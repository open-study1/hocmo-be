package com.edu.openstudy.data.mapper;


import com.edu.openstudy.data.dto.LessonPriorityDTO;
import com.edu.openstudy.data.dto.LessonPriorityViewDTO;
import com.edu.openstudy.data.entity.LessonPriorityEntity;

public interface LessonPriorityMapper {
	LessonPriorityViewDTO mapToViewDTO(LessonPriorityEntity lessonPriority);

	LessonPriorityDTO mapToDTO(LessonPriorityEntity lessonPriority);

	LessonPriorityEntity mapToEntity(LessonPriorityDTO lessonPriorityDTO);
}
