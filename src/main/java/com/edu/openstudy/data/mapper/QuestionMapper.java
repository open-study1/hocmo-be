package com.edu.openstudy.data.mapper;

import java.time.LocalDateTime;
import java.util.List;

import org.mapstruct.Mapper;

import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.QuestionExerciseDTO;
import com.edu.openstudy.data.entity.QuestionEntity;

public interface QuestionMapper {

	QuestionDTO mapToDTO(QuestionEntity question);
	
	QuestionExerciseDTO mapToExamDTO(QuestionEntity question,long quuestionGroupID, long userId,LocalDateTime createDate);
	
	QuestionExerciseDTO mapToExerciseDTO(QuestionEntity question,long quuestionGroupID, long userId);
	
	QuestionEntity mapToEntity(QuestionDTO questionDTO);

	QuestionDTO mapHaveSolution(QuestionEntity question);

	void mapToUpdate(QuestionEntity entity, QuestionDTO dto);

}
