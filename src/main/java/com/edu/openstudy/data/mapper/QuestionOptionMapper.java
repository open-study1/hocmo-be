package com.edu.openstudy.data.mapper;

import java.util.List;

import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.dto.QuestionOptionViewDTO;
import com.edu.openstudy.data.entity.QuestionOptionEntity;

public interface QuestionOptionMapper {

	QuestionOptionEntity mapToEntity(QuestionOptionDTO questionOptionDTO);

	QuestionOptionDTO mapToDTO(QuestionOptionEntity questionOption);
	
	QuestionOptionViewDTO mapToViewDTO(QuestionOptionEntity questionOption, List<String> valueTexts);

}
