package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.QuestionResponseUserDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserViewDTO;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;

public interface QuestionResponseUserMapper {
    QuestionResponseUserEntity mapToEntity(QuestionResponseUserDTO dto);

    QuestionResponseUserDTO mapToDTO(QuestionResponseUserEntity entity);
    
    QuestionResponseUserViewDTO mapToViewDTO(QuestionResponseUserEntity entity);
    
    QuestionResponseUserViewDTO mapToViewExamDTO(QuestionResponseUserEntity entity);

}
