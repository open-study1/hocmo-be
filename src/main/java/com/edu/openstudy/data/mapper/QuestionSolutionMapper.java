package com.edu.openstudy.data.mapper;

import org.mapstruct.Mapper;

import com.edu.openstudy.data.dto.QuestionSolutionDTO;
import com.edu.openstudy.data.entity.QuestionSolutionEntity;

public interface QuestionSolutionMapper {

	QuestionSolutionEntity mapToEntity(QuestionSolutionDTO questionSolutionDTO);

	QuestionSolutionDTO mapToDTO(QuestionSolutionEntity questionSolution);

}
