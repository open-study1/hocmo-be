package com.edu.openstudy.data.mapper;

import org.mapstruct.Mapper;

import com.edu.openstudy.data.dto.QuestionTypeDTO;
import com.edu.openstudy.data.entity.QuestionTypeEntity;

public interface QuestionTypeMapper {
	QuestionTypeEntity mapToEntity(QuestionTypeDTO questionTypeDTO);

	QuestionTypeDTO mapToDTO(QuestionTypeEntity questionType);
}
