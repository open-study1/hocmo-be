package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.RoleDTO;
import com.edu.openstudy.data.entity.RoleEntity;

public interface RoleMapper {
	
	  RoleEntity mapToEntity(RoleDTO dto);

	  RoleDTO mapToDTO(RoleEntity entity);

}
