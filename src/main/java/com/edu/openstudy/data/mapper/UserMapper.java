package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.UserDTO;
import com.edu.openstudy.data.entity.UserEntity;

public interface UserMapper {

	  UserEntity mapToEntity(UserDTO dto);

	  UserDTO mapToDTO(UserEntity entity);
}
