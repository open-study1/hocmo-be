package com.edu.openstudy.data.mapper;

import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.dto.UserScoreViewDTO;
import com.edu.openstudy.data.entity.UserScoreEntity;

public interface UserScoreMapper {
    UserScoreEntity mapToEntity(UserScoreDTO dto);

    UserScoreDTO mapToDTO(UserScoreEntity entity);
    
    UserScoreViewDTO mapToViewDTO(UserScoreEntity entity);
    

}
