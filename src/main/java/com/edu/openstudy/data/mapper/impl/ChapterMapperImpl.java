package com.edu.openstudy.data.mapper.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.StatisticsDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.ChapterEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.repository.ChapterRepository;
@Component
public class ChapterMapperImpl implements ChapterMapper {
	@Autowired
	private ChapterRepository chapterRepository;
	
	private List<String> subChapterCodes = new ArrayList<>(); 
	@Override
	public ChapterDTO mapToDTO(ChapterEntity chapter)
	{
		List<ChapterDTO> subChapters = new ArrayList<>(); 

		if(chapter == null)
			return null;
		ChapterDTO chapterDTO = new ChapterDTO();
		chapterDTO.setId(chapter.getId());
		chapterDTO.setName(chapter.getName());
		chapterDTO.setCode(chapter.getCode());
		chapterDTO.setParentCode(chapter.getParentCode());
		
		subChapterCodes = chapterRepository.findChapterCodeByParentCode(chapter.getCode());
		
		for (String code : subChapterCodes) {
				subChapters.add(mapToDTO(chapterRepository.findByCode(code).get()));
			}
		chapterDTO.setSubChapter(subChapters);
		return chapterDTO;
	}

	@Override
	public ChapterEntity map(ChapterDTO chapterDTO) {
		if(chapterDTO == null)
			return null;
		ChapterEntity chapter = new ChapterEntity();
		chapter.setId(chapterDTO.getId());
		chapter.setName(chapterDTO.getName());
		chapter.setCode(chapterDTO.getCode());
		chapter.setParentCode(chapterDTO.getParentCode());
		return chapter;
	}
	@Override
	public void mapToUpdate(ChapterDTO chapterDTO, ChapterEntity chapter)
	{
		chapter.setName(chapterDTO.getName());
		chapter.setCode(chapterDTO.getCode());
		chapter.setModifiedBy(UserContext.getId());
		chapter.setModifiedDate(LocalDateTime.now());
	}

	@Override
	public StatisticsDTO mapToStatisticsDTO(ChapterEntity chapter) {

		if(chapter == null)
			return null;
		StatisticsDTO chapterDTO = new StatisticsDTO();
		chapterDTO.setId(chapter.getId());
		chapterDTO.setName(chapter.getName());
		chapterDTO.setCode(chapter.getCode());
		chapterDTO.setParentCode(chapter.getParentCode());
		return chapterDTO;
	}
}
