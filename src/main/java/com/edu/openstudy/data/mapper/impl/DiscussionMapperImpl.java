package com.edu.openstudy.data.mapper.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.DiscussionCreateDTO;
import com.edu.openstudy.data.dto.DiscussionDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsViewDTO;
import com.edu.openstudy.data.dto.DiscussionSubDTO;
import com.edu.openstudy.data.dto.DiscussionViewDTO;
import com.edu.openstudy.data.entity.DiscussionEntity;
import com.edu.openstudy.data.entity.DiscussionReactionsEntity;
import com.edu.openstudy.data.mapper.DiscussionMapper;
import com.edu.openstudy.data.mapper.DiscussionReactionsMapper;
import com.edu.openstudy.data.mapper.LessonMapper;
import com.edu.openstudy.data.repository.DiscussionRepository;
@Component
public class DiscussionMapperImpl implements DiscussionMapper{
	@Autowired
	private LessonMapper lessonMapper;
	@Autowired
	private DiscussionReactionsMapper discussionReactionsMapper ;
	@Autowired
	private DiscussionMapper discussionMapper ;
	@Autowired
	private DiscussionRepository discussionRepository  ;
	@Override
	public DiscussionEntity map(DiscussionDTO discussionDTO) {
	 	if(discussionDTO == null)
			return null;
		DiscussionEntity discussion = new DiscussionEntity();
		discussion.setId(discussionDTO.getId());
		discussion.setParentId(discussionDTO.getParentId());
		discussion.setLesson(lessonMapper.mapToEntity(discussionDTO.getLesson()));
		discussion.setDiscussionContent(discussionDTO.getDiscussionContent());
		discussion.setCreatedBy(discussionDTO.getCreatedBy());
		discussion.setCreatedDate(discussionDTO.getCreatedDate());
		discussion.setModifiedBy(discussionDTO.getModifiedBy());
		discussion.setModifiedDate(discussionDTO.getModifiedDate());
		return discussion;
	}
	
	@Override
	public DiscussionEntity map(DiscussionCreateDTO discussionDTO) {
		if(discussionDTO == null)
			return null;
		DiscussionEntity discussion = new DiscussionEntity();
		discussion.setId(discussionDTO.getId());
		discussion.setParentId(discussionDTO.getParentId());
		discussion.setLesson(lessonMapper.mapToEntity(discussionDTO.getLesson()));
		discussion.setDiscussionContent(discussionDTO.getDiscussionContent());
		discussion.setCreatedDate(discussionDTO.getCreatedDate());
		discussion.setModifiedDate(discussionDTO.getModifiedDate());
		return discussion;
	}
	
	@Override
	public DiscussionDTO map(DiscussionEntity discussion) {
		if(discussion == null)
			return null;
		DiscussionDTO discussionDTO = new DiscussionDTO();
		discussionDTO.setId(discussion.getId());
		discussionDTO.setParentId(discussion.getParentId());
		discussionDTO.setLesson(lessonMapper.mapToDTO(discussion.getLesson()));	
		discussionDTO.setDiscussionContent(discussion.getDiscussionContent());
		discussionDTO.setCreatedBy(discussion.getCreatedBy());
		discussionDTO.setCreatedDate(discussion.getCreatedDate());
		discussionDTO.setModifiedBy(discussion.getModifiedBy());
		discussionDTO.setModifiedDate(discussion.getModifiedDate());
		return discussionDTO;
	}

	@Override
	public DiscussionViewDTO mapToViewDTO(DiscussionEntity discussion) {
		if(discussion == null)
			return null;
		DiscussionViewDTO discussionViewDTO = new DiscussionViewDTO();
		discussionViewDTO.setId(discussion.getId());
		Set<DiscussionReactionsViewDTO> discussionReactionsDTOs = new HashSet<>();
		for (DiscussionReactionsEntity discussionReactions : discussion.getDiscussionReactions()) {
			discussionReactionsDTOs.add(discussionReactionsMapper.mapToView(discussionReactions));
		}
		Set<DiscussionSubDTO> discussionSubs = new HashSet<>();
		for (DiscussionEntity discussionSub :  discussionRepository.findAllByParentId(discussion.getId())) {
			discussionSubs.add(discussionMapper.mapToSubDTO(discussionSub));
		}
		discussionViewDTO.setDiscussionSubs(discussionSubs);
		discussionViewDTO.setDiscussionReactions(discussionReactionsDTOs);
		discussionViewDTO.setDiscussionContent(discussion.getDiscussionContent());
		discussionViewDTO.setCreatedBy(discussion.getCreatedBy());
		discussionViewDTO.setCreatedDate(discussion.getCreatedDate());
		discussionViewDTO.setModifiedBy(discussion.getModifiedBy());
		discussionViewDTO.setModifiedDate(discussion.getModifiedDate());
		return discussionViewDTO;
	}

	@Override
	public DiscussionSubDTO mapToSubDTO(DiscussionEntity discussion) {
		if(discussion == null)
			return null;
		DiscussionSubDTO discussionSubDTO = new DiscussionSubDTO();
		discussionSubDTO.setId(discussion.getId());
		Set<DiscussionReactionsViewDTO> discussionReactionsDTOs = new HashSet<>();
		for (DiscussionReactionsEntity discussionReactions : discussion.getDiscussionReactions()) {
			discussionReactionsDTOs.add(discussionReactionsMapper.mapToView(discussionReactions));
		}
		discussionSubDTO.setDiscussionReactions(discussionReactionsDTOs);
		discussionSubDTO.setDiscussionContent(discussion.getDiscussionContent());
		discussionSubDTO.setCreatedBy(discussion.getCreatedBy());
		discussionSubDTO.setCreatedDate(discussion.getCreatedDate());
		discussionSubDTO.setModifiedBy(discussion.getModifiedBy());
		discussionSubDTO.setModifiedDate(discussion.getModifiedDate());
		return discussionSubDTO;
	}

}
