package com.edu.openstudy.data.mapper.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExamHistoryDTO;
import com.edu.openstudy.data.dto.ExamViewDTO;
import com.edu.openstudy.data.dto.ExamViewDetailDTO;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.QuestionExerciseDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.QuestionMapper;
import com.edu.openstudy.data.repository.QuestionRepository;
import com.edu.openstudy.data.repository.UserScoreRepository;
import com.edu.openstudy.service.QuestionService;


@Component
public class ExamMapperImpl implements ExamMapper{
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private ChapterMapper chapterMapper;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private UserScoreRepository userScoreRepository;
	@Override
	public ExamEntity mapToEntity(ExamDTO questionDto) {
		return null;
	}

	@Override
	public ExamDTO mapToDTO(ExamEntity entity) {
		if(entity == null)
			return null;
		ExamDTO dto = new ExamDTO();
		List<QuestionDTO> questionDTOs = new ArrayList<>() ;
		dto.setId(entity.getId());
		dto.setTitle(entity.getTitle());
		dto.setDescription(entity.getDescription());	
		dto.setDuration(entity.getDuration());
		questionDTOs = questionService.findQuestionByExamId(entity.getId());
		dto.setQuestions(questionDTOs);
		dto.setQuestions(questionDTOs);
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
		dto.setChapterDTO(chapterMapper.mapToDTO(entity.getChapter()));

		return dto;
	}
	@Override
	public void mapToUpdate(ExamEntity questionGroupDTO, ExamEntity questionGroupEntity)
	{
		if(questionGroupEntity == null)
		{
			questionGroupEntity = new ExamEntity();
			questionGroupEntity.setCreatedBy(UserContext.getId());
			questionGroupEntity.setModifiedBy(UserContext.getId());
			questionGroupEntity.setCreatedDate(DateTime.getInstances());
			questionGroupEntity.setModifiedDate(DateTime.getInstances());
		}
		questionGroupEntity.setTitle(questionGroupDTO.getTitle());
		questionGroupEntity.setDescription(questionGroupDTO.getDescription());
		
	}

	@Override
	public ExamViewDetailDTO mapToViewDetailDTO(ExamEntity entity, long userId,LocalDateTime createDate) {
		if(entity == null)
			return null;
		ExamViewDetailDTO dto = new ExamViewDetailDTO();
		List<QuestionExerciseDTO> questionDTOs = new ArrayList<>() ;
		dto.setId(entity.getId());
		dto.setTitle(entity.getTitle());
		dto.setDescription(entity.getDescription());	
		dto.setDuration(entity.getDuration());
		questionDTOs = questionRepository.findQuestionByExamId(entity.getId()).stream()
				.map(item -> this.questionMapper.mapToExamDTO(item,entity.getId(),userId, createDate)).collect(Collectors.toList());
		
		dto.setQuestions(questionDTOs);
//		questionDTOs.sort(new Comparator<QuestionExerciseDTO>() {
//			@Override
//			public int compare(QuestionExerciseDTO o1, QuestionExerciseDTO o2) {
//				return o1.getId().equals(o2.getId()) ? -1 : 1;
//			}
//        });
		dto.setQuestions(questionDTOs);
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
		dto.setChapterDTO(chapterMapper.mapToDTO(entity.getChapter()));

		return dto;
	}

	@Override
	public ExamViewDTO mapToViewDTO(ExamEntity entity) {
		if(entity == null)
			return null;
		ExamViewDTO dto = new ExamViewDTO();
		List<QuestionExerciseDTO> questionDTOs = new ArrayList<>() ;
		dto.setId(entity.getId());
		dto.setTitle(entity.getTitle());
		dto.setDescription(entity.getDescription());	
		dto.setDuration(entity.getDuration());
		if(userScoreRepository.existsByExamId(entity.getId()))
			dto.setPraticed(true);
		else	
			dto.setPraticed(false);
		dto.setTimePraticed(userScoreRepository.countByExamIdAndCreatedBy(entity.getId(), UserContext.getId()));
		questionDTOs.sort(new Comparator<QuestionExerciseDTO>() {
			@Override
			public int compare(QuestionExerciseDTO o1, QuestionExerciseDTO o2) {
				return o1.getOrder().equals(o2.getOrder()) ? -1 : 1;
			}
        });
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
		dto.setChapterDTO(chapterMapper.mapToDTO(entity.getChapter()));

		return dto;
	}
	@Override
	public ExamHistoryDTO mapToHistoryDTO(ExamEntity entity) {
		if(entity == null)
			return null;
		ExamHistoryDTO dto = new ExamHistoryDTO();
		List<QuestionExerciseDTO> questionDTOs = new ArrayList<>() ;
		dto.setId(entity.getId());
		dto.setTitle(entity.getTitle());
		dto.setDescription(entity.getDescription());	
		dto.setDuration(entity.getDuration());
		if(userScoreRepository.existsByExamId(entity.getId()))
			dto.setPraticed(true);
		else	
			dto.setPraticed(false);
		dto.setTotalPracticed(userScoreRepository.countByExamIdAndCreatedBy(entity.getId(), UserContext.getId()));
		dto.setMinScore(userScoreRepository.findFirstByExamIdAndCreatedByOrderByScoreAsc(entity.getId(), UserContext.getId()).get().getScore());
		dto.setMaxScore(userScoreRepository.findFirstByExamIdAndCreatedByOrderByScoreDesc(entity.getId(), UserContext.getId()).get().getScore());
		dto.setMediumScore(userScoreRepository.getMediumScoreByExamIdAndCreatedBy(entity.getId(), UserContext.getId()));
		questionDTOs.sort(new Comparator<QuestionExerciseDTO>() {
			@Override
			public int compare(QuestionExerciseDTO o1, QuestionExerciseDTO o2) {
				return o1.getOrder().equals(o2.getOrder()) ? -1 : 1;
			}
        });
		dto.setChapterDTO(chapterMapper.mapToDTO(entity.getChapter()));
		return dto;
	}
	


}
