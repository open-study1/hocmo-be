package com.edu.openstudy.data.mapper.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.QuestionExerciseDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.ExerciseEntity;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionExerciseEntity;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.ExerciseMapper;
import com.edu.openstudy.data.mapper.QuestionMapper;
import com.edu.openstudy.data.repository.QuestionRepository;
import com.edu.openstudy.service.QuestionService;

@Component
public class ExerciseMapperImpl implements ExerciseMapper{
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private QuestionRepository questionRepository;
	@Override
	public ExerciseDTO mapToDTO(ExerciseEntity entity, long userId) {
		if(entity == null)
			return null;
		ExerciseDTO dto = new ExerciseDTO();
		List<QuestionExerciseDTO> questionDTOs = new ArrayList<>() ;
		int questionPracticed = 0;
		
		dto.setId(entity.getId());
		dto.setTitle(entity.getTitle());
		dto.setDescription(entity.getDescription());	
				
		questionDTOs = questionRepository.findQuestionByExercise(entity.getId()).stream()
				.map(item -> this.questionMapper.mapToExerciseDTO(item,entity.getId(),userId)).collect(Collectors.toList());
		
		
		for (QuestionExerciseDTO questionExerciseDTO : questionDTOs) {
			if(questionExerciseDTO.getQuestionResponseUser()!=null)
				questionPracticed ++;
		}
		dto.setQuestionPracticed(questionPracticed);
		dto.setQuestions(questionDTOs);
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
	
		return dto;
	}

	@Override
	public ExerciseEntity mapToEntity(ExamDTO questionGroupDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExerciseDTO mapViewToDTO(ExerciseEntity entity) {
		if(entity == null)
			return null;
		ExerciseDTO dto = new ExerciseDTO();
		List<QuestionExerciseDTO> questionDTOs = new ArrayList<>() ;
		dto.setId(entity.getId());
		dto.setTitle(entity.getTitle());
		dto.setDescription(entity.getDescription());			
		dto.setQuestions(questionDTOs);
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
	
		return dto;
	}


}
