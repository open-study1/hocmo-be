package com.edu.openstudy.data.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.LessonDTO;
import com.edu.openstudy.data.dto.LessonDetailViewDTO;
import com.edu.openstudy.data.dto.LessonMediaDTO;
import com.edu.openstudy.data.dto.LessonPriorityViewDTO;
import com.edu.openstudy.data.dto.LessonViewDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonMediaEntity;
import com.edu.openstudy.data.entity.LessonPriorityEntity;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.mapper.LessonMapper;
import com.edu.openstudy.data.mapper.LessonMediaMapper;
import com.edu.openstudy.data.mapper.LessonPriorityMapper;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.exception.ErrorMessageException;

@Component
public class LessonMapperImpl implements LessonMapper {
	@Autowired
	private ChapterMapper chapterMapper;
	@Autowired
	private LessonMediaMapper lessonMediaMapper;
	@Autowired
	private LessonPriorityMapper lessonPriorityMapper;
	@Autowired
	private ExamMapper questionGroupMapper;
	@Autowired
	private MessageSource messageSource;

	@Override
	public LessonEntity mapToEntity(LessonCreateDTO lessonCreateDTO) {
		if (lessonCreateDTO == null)
			return null;
		LessonEntity lesson = new LessonEntity();
		lesson.setId(lessonCreateDTO.getId());
		lesson.setContent(lessonCreateDTO.getContent());
		lesson.setTitle(lessonCreateDTO.getTitle());
		ExamEntity questionGroup = lessonCreateDTO.getQuestionGroup();
//		lesson.setQuestionGroup(questionGroup);
		lesson.setLessonSourceMediaLinkImage(lessonCreateDTO.getSourceMediaLinkImage());
		lesson.setLessonSourceMediaLinkVideo(lessonCreateDTO.getSourceMediaLinkVideo());
		setUser(lesson, lessonCreateDTO);
		return lesson;
	}

	@Override
	public LessonCreateDTO mapToDTO(LessonEntity lessonDTO) {
		if (lessonDTO == null)
			return null;
		LessonCreateDTO lesson = new LessonCreateDTO();
		lesson.setId(lessonDTO.getId());
		lesson.setContent(lessonDTO.getContent());
		lesson.setTitle(lessonDTO.getTitle());
//		lesson.setQuestionGroup(lessonDTO.getQuestionGroup());
		return lesson;
	}

	@Override
	public void mapToDTOUpdate(LessonEntity lesson, LessonCreateDTO lessonCreateDTO) {
		lesson.setContent(lessonCreateDTO.getContent());
		lesson.setTitle(lessonCreateDTO.getTitle());
		// modified if need admin
		lesson.setLessonSourceMediaLinkImage(lessonCreateDTO.getSourceMediaLinkImage());
		lesson.setLessonSourceMediaLinkVideo(lessonCreateDTO.getSourceMediaLinkVideo());
		// set question group
		ExamEntity questionGroupDTO = lessonCreateDTO.getQuestionGroup();
		mapToUpdateQuestinGroup(questionGroupDTO, lesson);

	}

	public LessonViewDTO mapToViewDTO(LessonEntity lesson) {
		if (lesson == null)
			return null;
		LessonViewDTO lessonDTO = new LessonViewDTO();
		lessonDTO.setId(lesson.getId());
		lessonDTO.setContent(lesson.getContent());
		lessonDTO.setTitle(lesson.getTitle());
		lessonDTO.setStatus(lesson.isStatus());
		lessonDTO.setChapter(chapterMapper.mapToDTO(lesson.getChapter()));
		lessonDTO.setLessonSourceMediaLinkImage(lesson.getLessonSourceMediaLinkImage());
		lessonDTO.setLessonSourceMediaLinkVideo(lesson.getLessonSourceMediaLinkVideo());
		lessonDTO.setCreatedBy(lesson.getCreatedBy());
		lessonDTO.setCreatedDate(lesson.getCreatedDate());
		lessonDTO.setModifiedBy(lesson.getModifiedBy());
		lessonDTO.setModifiedDate(lesson.getModifiedDate());
		return lessonDTO;
	}
@Override
	public LessonViewDTO mapToViewIncludePriorityDTO(LessonEntity lesson) {
		if (lesson == null)
			return null;
		List<LessonPriorityViewDTO> lessonPriorityViewDTOs = new ArrayList<>();
		LessonViewDTO lessonViewDTO = new LessonViewDTO();
		lessonViewDTO.setId(lesson.getId());
		lessonViewDTO.setContent(lesson.getContent());
		lessonViewDTO.setTitle(lesson.getTitle());
		lessonViewDTO.setChapter(chapterMapper.mapToDTO(lesson.getChapter()));

		lessonViewDTO.setStatus(lesson.isStatus());

		lessonViewDTO.setLessonSourceMediaLinkImage(lesson.getLessonSourceMediaLinkImage());
		lessonViewDTO.setLessonSourceMediaLinkVideo(lesson.getLessonSourceMediaLinkVideo());
		lessonViewDTO.setCreatedBy(lesson.getCreatedBy());
		lessonViewDTO.setCreatedDate(lesson.getCreatedDate());
		lessonViewDTO.setModifiedBy(lesson.getModifiedBy());
		lessonViewDTO.setModifiedDate(lesson.getModifiedDate());
		for (LessonPriorityEntity lessonPriority : lesson.getLessonPriorities()) {
			if(lessonPriority.getModifiedBy()==UserContext.getId()) {
				lessonPriorityViewDTOs.add(lessonPriorityMapper.mapToViewDTO(lessonPriority));
			}
		}
		lessonViewDTO.setPriorities(lessonPriorityViewDTOs);
		return lessonViewDTO;
	}
	@Override
	public LessonDetailViewDTO mapToViewDetailDTO(LessonEntity lesson) {
		if (lesson == null)
			return null;
		LessonDetailViewDTO lessonDetailViewDTO = new LessonDetailViewDTO();
		List<LessonMediaDTO> lessonMediaDTOs = new ArrayList<>();
		lessonDetailViewDTO.setId(lesson.getId());
		lessonDetailViewDTO.setContent(lesson.getContent());
		lessonDetailViewDTO.setTitle(lesson.getTitle());
		lessonDetailViewDTO.setStatus(lesson.isStatus());

		lessonDetailViewDTO.setLessonSourceMediaLinkImage(lesson.getLessonSourceMediaLinkImage());
		lessonDetailViewDTO.setLessonSourceMediaLinkVideo(lesson.getLessonSourceMediaLinkVideo());
		for (LessonMediaEntity lessonMedia : lesson.getLessonMedias()) {
			lessonMediaDTOs.add(lessonMediaMapper.mapToDTO(lessonMedia));
		}
		lessonDetailViewDTO.setLessonMediaDTOs(lessonMediaDTOs);
		lessonDetailViewDTO.setCreatedBy(lesson.getCreatedBy());
		lessonDetailViewDTO.setCreatedDate(lesson.getCreatedDate());
		lessonDetailViewDTO.setModifiedBy(lesson.getModifiedBy());
		lessonDetailViewDTO.setModifiedDate(lesson.getModifiedDate());
		lessonDetailViewDTO.setUserIdAsAppover(lesson.getUserIdAsAppover());
		return lessonDetailViewDTO;
	}
	
	@Override
	public LessonDetailViewDTO mapToViewDetailIncludePriorityDTO(LessonEntity lesson) {
		if (lesson == null)
			return null;
		LessonDetailViewDTO lessonDetailViewDTO = new LessonDetailViewDTO();
		List<LessonPriorityViewDTO> lessonPriorityViewDTOs = new ArrayList<>();
		List<LessonMediaDTO> lessonMediaDTOs = new ArrayList<>();
		lessonDetailViewDTO.setId(lesson.getId());
		lessonDetailViewDTO.setContent(lesson.getContent());
		lessonDetailViewDTO.setTitle(lesson.getTitle());
		lessonDetailViewDTO.setStatus(lesson.isStatus());
		lessonDetailViewDTO.setChapter(chapterMapper.mapToDTO(lesson.getChapter()));
		lessonDetailViewDTO.setLessonSourceMediaLinkImage(lesson.getLessonSourceMediaLinkImage());
		lessonDetailViewDTO.setLessonSourceMediaLinkVideo(lesson.getLessonSourceMediaLinkVideo());
		for (LessonMediaEntity lessonMedia : lesson.getLessonMedias()) {
			lessonMediaDTOs.add(lessonMediaMapper.mapToDTO(lessonMedia));
		}
		lessonDetailViewDTO.setLessonMediaDTOs(lessonMediaDTOs);
		lessonDetailViewDTO.setCreatedBy(lesson.getCreatedBy());
		lessonDetailViewDTO.setCreatedDate(lesson.getCreatedDate());
		lessonDetailViewDTO.setModifiedBy(lesson.getModifiedBy());
		lessonDetailViewDTO.setModifiedDate(lesson.getModifiedDate());
		lessonDetailViewDTO.setUserIdAsAppover(lesson.getUserIdAsAppover());
		for (LessonPriorityEntity lessonPriority : lesson.getLessonPriorities()) {
			if(lessonPriority.getModifiedBy()==UserContext.getId()) {
				lessonPriorityViewDTOs.add(lessonPriorityMapper.mapToViewDTO(lessonPriority));
			}
		}
		lessonDetailViewDTO.setPriorities(lessonPriorityViewDTOs);
		lessonDetailViewDTO.setUserIdAsAppover(lesson.getUserIdAsAppover());
		return lessonDetailViewDTO;
	}

	private void setUser(LessonEntity lesson, LessonCreateDTO lessonCreateDTO) {
		Long user = UserContext.getId();
		if (user != null && user.longValue() != 0) {
			lesson.setCreatedBy(user);
			lesson.setModifiedBy(user);
		} else {
			throw new ErrorMessageException(this.messageSource.getMessage("Forbidden", null, null),
					TypeError.Forbidden);
		}

	}

	private void mapToUpdateQuestinGroup(ExamEntity questionGroupDTO, LessonEntity lesson) {
//		List<QuestionGroupEntity> questionGroupEntities = lesson.getQuestionGroups();
//		if (questionGroupEntity == null) {
//			questionGroupEntity = new QuestionGroupEntity();
//			questionGroupEntity.setCreatedBy(UserContext.getId());
//			questionGroupEntity.setModifiedBy(UserContext.getId());
//			questionGroupEntity.setCreatedDate(DateTime.getInstances());
//			questionGroupEntity.setModifiedDate(DateTime.getInstances());
//		}
//		questionGroupEntity.setIsEmbedded(questionGroupDTO.getIsEmbedded());
//		questionGroupEntity.setIsShuffle(questionGroupDTO.getIsShuffle());
//		questionGroupEntity.setTitle(questionGroupDTO.getTitle());
//		questionGroupEntity.setDescription(questionGroupDTO.getDescription());
//		questionGroupEntity.setMediaSegmentFrom(questionGroupDTO.getMediaSegmentFrom());
//		lesson.setQuestionGroup(questionGroupEntity);
	}
	@Override
	public LessonEntity mapToEntity(LessonDTO lessonDTO)
	{
		LessonEntity lesson = new LessonEntity();
		lesson.setId(lessonDTO.getId());
		lesson.setTitle(lessonDTO.getTitle());
		lesson.setContent(lessonDTO.getContent());
		return lesson;
	}
	
	@Override
	public LessonDTO map(LessonEntity lesson)
	{
		LessonDTO lessonDTO = new LessonDTO();
		lessonDTO.setId(lesson.getId());
		lessonDTO.setTitle(lesson.getTitle());
		lessonDTO.setContent(lesson.getContent());
		return lessonDTO;
	}

}
