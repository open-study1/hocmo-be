package com.edu.openstudy.data.mapper.impl;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.data.dto.LessonMediaDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonMediaEntity;
import com.edu.openstudy.data.mapper.LessonMediaMapper;

import org.springframework.stereotype.Component;

@Component
public class LessonMediaMapperImpl implements LessonMediaMapper {
	@Override
	public LessonMediaEntity mapToEntity(LessonMediaDTO dto)
	{
		LessonMediaEntity lessonMedia = new LessonMediaEntity();
		lessonMedia.setLessonMediaLink(dto.getLessonMediaLink());
		lessonMedia.setType(dto.getType());
		lessonMedia.setOrder(dto.getOrder());
		return lessonMedia;
	}
	@Override
	public LessonMediaDTO mapToDTO(LessonMediaEntity entity)
	{
		LessonMediaDTO dto = new LessonMediaDTO();
		dto.setId(entity.getId());
		dto.setLessonMediaLink(entity.getLessonMediaLink());
		dto.setType(entity.getType());
		dto.setOrder(entity.getOrder());
		return dto;
	}
	@Override
	public void mapToUpdate(LessonMediaEntity lessonMedia, LessonMediaDTO dto, LessonEntity lesson)
	{
		lessonMedia.setModifiedBy(UserContext.getId());
		lessonMedia.setModifiedDate(DateTime.getInstances());
		lessonMedia.setLesson(lesson);
		lessonMedia.setType(dto.getType());
		lessonMedia.setOrder(dto.getOrder());
	}
}
