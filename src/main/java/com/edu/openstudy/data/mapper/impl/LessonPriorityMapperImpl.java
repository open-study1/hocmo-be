package com.edu.openstudy.data.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.LessonPriorityDTO;
import com.edu.openstudy.data.dto.LessonPriorityViewDTO;
import com.edu.openstudy.data.entity.LessonPriorityEntity;
import com.edu.openstudy.data.mapper.LessonMapper;
import com.edu.openstudy.data.mapper.LessonPriorityMapper;

@Component
public class LessonPriorityMapperImpl implements LessonPriorityMapper {
	@Autowired
	private LessonMapper lessonMapper;

	@Override
	public LessonPriorityViewDTO mapToViewDTO(LessonPriorityEntity lessonPriority) {
		if (lessonPriority == null)
			return null;
		LessonPriorityViewDTO lessonPriorityViewDTO = new LessonPriorityViewDTO();
		lessonPriorityViewDTO.setId(lessonPriority.getId());
		lessonPriorityViewDTO.setPriority(lessonPriority.getPriority());
		lessonPriorityViewDTO.setCreatedDate(lessonPriority.getCreatedDate());
		lessonPriorityViewDTO.setCreatedBy(lessonPriority.getCreatedBy());
		lessonPriorityViewDTO.setModifiedDate(lessonPriority.getModifiedDate());
		lessonPriorityViewDTO.setModifiedBy(lessonPriority.getModifiedBy());
		return lessonPriorityViewDTO;
	}

	@Override
	public LessonPriorityEntity mapToEntity(LessonPriorityDTO lessonPriorityDTO) {
		LessonPriorityEntity lessonPriority = new LessonPriorityEntity();
		lessonPriority.setId(lessonPriorityDTO.getId());
		lessonPriority.setPriority(lessonPriorityDTO.getPriority());
		//lessonPriority.setLesson(lessonMapper.mapToEntity(lessonPriorityDTO.getLesson()));
		return lessonPriority;
	}

	@Override
	public LessonPriorityDTO mapToDTO(LessonPriorityEntity lessonPriority) {
		LessonPriorityDTO lessonPriorityDTO = new LessonPriorityDTO();
		lessonPriorityDTO.setId(lessonPriority.getId());
		lessonPriorityDTO.setPriority(lessonPriority.getPriority());
		//lessonPriorityDTO.setLesson(lessonMapper.map(lessonPriority.getLesson()));
		return lessonPriorityDTO;
	}

}
