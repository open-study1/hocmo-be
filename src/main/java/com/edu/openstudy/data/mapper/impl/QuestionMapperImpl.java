package com.edu.openstudy.data.mapper.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.QuestionExerciseDTO;
import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.dto.QuestionOptionViewDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDTO;
import com.edu.openstudy.data.dto.QuestionSolutionDTO;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionOptionEntity;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import com.edu.openstudy.data.entity.QuestionSolutionEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.mapper.QuestionMapper;
import com.edu.openstudy.data.mapper.QuestionOptionMapper;
import com.edu.openstudy.data.mapper.QuestionResponseUserMapper;
import com.edu.openstudy.data.mapper.QuestionSolutionMapper;
import com.edu.openstudy.data.mapper.QuestionTypeMapper;
import com.edu.openstudy.data.repository.QuestionOptionRepository;
import com.edu.openstudy.data.repository.QuestionResponseUserRepository;
import com.edu.openstudy.data.repository.QuestionSolutionRepository;
import com.edu.openstudy.service.QuestionOptionService;
import com.edu.openstudy.service.QuestionSolutionService;
@Component
public class QuestionMapperImpl  implements QuestionMapper{
	@Autowired
	private QuestionOptionMapper questionOptionMapper;
	@Autowired
	private ChapterMapper chapterMapper;
	@Autowired
	private QuestionTypeMapper questionTypeMapper;
	@Autowired
	private QuestionSolutionMapper questionSolutionMapper;
	@Autowired
	private QuestionSolutionService questionSolutionService;
	@Autowired
	private QuestionOptionService questionOptionService;
	@Autowired
	private QuestionOptionRepository questionOptionRepository;
	@Autowired
	private QuestionSolutionRepository questionSolutionRepository ;
	@Autowired
	private QuestionResponseUserMapper questionResponseUserMapper ;
	@Autowired
	private QuestionResponseUserRepository questionResponseUserRepository ;
	@Override
	public QuestionDTO mapToDTO(QuestionEntity question)
	{
		if(question == null) 
			return null;
		QuestionDTO questionDTO = new QuestionDTO();
		List<QuestionOptionDTO> questionOptionDTOs = new ArrayList<>() ;
		questionDTO.setId(question.getId());
		questionDTO.setContent(question.getContent());
		questionDTO.setChapter(chapterMapper.mapToDTO(question.getChapter()));
		questionDTO.setQuestionType(questionTypeMapper.mapToDTO(question.getQuestionType()));
		questionOptionDTOs = questionOptionRepository.findQuestionOptionByQuestionId(question.getId()).stream()
				.map(item -> this.questionOptionMapper.mapToDTO(item)).collect(Collectors.toList());
		questionDTO.setQuestionOptions(questionOptionDTOs);
		return questionDTO;
	}
	@Override
	public QuestionEntity mapToEntity(QuestionDTO questionDTO)
	{
		if(questionDTO == null) 
			return null;
		QuestionEntity question = new QuestionEntity();
		question.setId(questionDTO.getId());
		question.setContent(questionDTO.getContent());
		question.setChapter(chapterMapper.map(questionDTO.getChapter()));
		if(questionDTO.getQuestionType() != null)
			question.setQuestionType(questionTypeMapper.mapToEntity(questionDTO.getQuestionType()));
		return question;
	}
	@Override
	public QuestionExerciseDTO mapToExamDTO(QuestionEntity question,long examID, long userId, LocalDateTime createDate) {
		if(question == null) 
			return null;
		QuestionExerciseDTO dto = new QuestionExerciseDTO();
		List<QuestionOptionViewDTO> questionOptionDTOs = new ArrayList<QuestionOptionViewDTO>();
		dto.setId(question.getId());
		dto.setContent(question.getContent());
		dto.setQuestionSolutions(questionSolutionMapper.mapToDTO(question.getQuestionSolution()));
		
		dto.setQuestionOptions(questionOptionDTOs);
		if(question.getQuestionSolution()!=null) {
			dto.setQuestionSolutions(questionSolutionMapper.mapToDTO(question.getQuestionSolution()));
		}
		String valueTextUser = questionResponseUserRepository.getValueTextOfExamByUser(examID, question.getId(), userId,createDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString());
		if(valueTextUser!=null ) {
			if(!valueTextUser.equals("")) {
				List<String> listQuestionResponseUser = new ArrayList<String>(Arrays.asList(valueTextUser.split(",")));
				String solutionStr = new  String();
				for (QuestionOptionDTO questionOptionDTO : dto.getQuestionSolutions().getSolution()) {
					solutionStr = solutionStr+","+String.valueOf(questionOptionDTO.getId());
				}
				solutionStr = solutionStr.substring(1);
				dto.setQuestionResponseUser(listQuestionResponseUser);
				
				//check result
				dto.setResult(true);
				if(dto.getQuestionSolutions().getSolution().size()>listQuestionResponseUser.size())
					dto.setResult(false);
				else {
					for (String valueText : listQuestionResponseUser) {
						if(solutionStr.contains(valueText)==false) {
							dto.setResult(false);break;
						}
					}
				}
			}	
		}
		for (QuestionOptionEntity questionOption : question.getQuestionOptions() ) {
			questionOptionDTOs.add(questionOptionMapper.mapToViewDTO(questionOption, dto.getQuestionResponseUser())); 
		}
		dto.setQuestionOptions(questionOptionDTOs);
		dto.setQuestionType(questionTypeMapper.mapToDTO(question.getQuestionType()));
		
	
		return dto;
	}
	
	@Override
	public QuestionExerciseDTO mapToExerciseDTO(QuestionEntity question,long exerciseID, long userId) {
		if(question == null) 
			return null;
		QuestionExerciseDTO dto = new QuestionExerciseDTO();
		List<QuestionOptionViewDTO> questionOptionDTOs = new ArrayList<QuestionOptionViewDTO>();
	
		dto.setId(question.getId());
		dto.setContent(question.getContent());
		String valueTextUser = questionResponseUserRepository.getValueTextOfExerciseByUser(exerciseID, question.getId(), userId);
	
		if(question.getQuestionSolution()!=null) {
			dto.setQuestionSolutions(questionSolutionMapper.mapToDTO(question.getQuestionSolution()));
		}
		
		dto.setQuestionType(questionTypeMapper.mapToDTO(question.getQuestionType()));
		
		//check result
		dto.setResult(true);
		if(valueTextUser!=null) {
			List<String> listQuestionResponseUser = new ArrayList<String>(Arrays.asList(valueTextUser.split(",")));
			String solutionStr = new  String();
			for (QuestionOptionDTO questionOptionDTO : dto.getQuestionSolutions().getSolution()) {
				solutionStr = solutionStr+","+String.valueOf(questionOptionDTO.getId());
			}
			solutionStr = solutionStr.substring(1);
			dto.setQuestionResponseUser(listQuestionResponseUser);
			if(dto.getQuestionSolutions().getSolution().size()>listQuestionResponseUser.size())
				dto.setResult(false);
			else {
				for (String text : listQuestionResponseUser) {
					if(solutionStr.contains(text)==false) {
						dto.setResult(false);break;
					}
				}
			}
		}else {
			dto.setResult(false);
		}
		
		for (QuestionOptionEntity questionOption : question.getQuestionOptions() ) {
			questionOptionDTOs.add(questionOptionMapper.mapToViewDTO(questionOption, dto.getQuestionResponseUser())); 
		}
		dto.setQuestionOptions(questionOptionDTOs);
		return dto;
	}
	
	@Override
	public QuestionDTO mapHaveSolution(QuestionEntity question)//Map for do Score
	{
		QuestionDTO questionDTO =	mapToDTO(question);
		questionDTO.setQuestionSolution(questionSolutionMapper.mapToDTO(question.getQuestionSolution()));
		return questionDTO;
	}
	@Override
	public void mapToUpdate(QuestionEntity entity, QuestionDTO dto)
	{
		
//		entity.setModifiedBy(UserContext.getId());
		entity.setModifiedDate(DateTime.getInstances());
	}

}
