package com.edu.openstudy.data.mapper.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.dto.QuestionOptionViewDTO;
import com.edu.openstudy.data.entity.QuestionOptionEntity;
import com.edu.openstudy.data.mapper.QuestionOptionMapper;
@Component
public class QuestionOptionMapperImpl implements QuestionOptionMapper {
	@Override
	public QuestionOptionDTO mapToDTO(QuestionOptionEntity questionOption)
	{
		if (questionOption == null)
			return null;
		QuestionOptionDTO questionOptionDTO = new QuestionOptionDTO();
		questionOptionDTO.setId(questionOption.getId());
		questionOptionDTO.setContent(questionOption.getContent());
		questionOptionDTO.setOrder(questionOption.getOrder());
		if(questionOption.getCode() != null)
			questionOptionDTO.setCode(questionOption.getCode());
		return questionOptionDTO;
	}
	@Override
	public QuestionOptionEntity mapToEntity(QuestionOptionDTO dto)
	{
		if (dto == null)
			return null;
		QuestionOptionEntity entity = new QuestionOptionEntity();
		entity.setId(dto.getId());
		entity.setContent(dto.getContent());
		entity.setOrder(dto.getOrder());
		entity.setCode(dto.getCode());
		return entity;
	}
	@Override
	public QuestionOptionViewDTO mapToViewDTO(QuestionOptionEntity questionOption, List<String> valueTexts) {
		if (questionOption == null)
			return null;
		QuestionOptionViewDTO questionOptionDTO = new QuestionOptionViewDTO();
		questionOptionDTO.setId(questionOption.getId());
		questionOptionDTO.setContent(questionOption.getContent());
		if(questionOption.getOrder() != 0)
			questionOptionDTO.setOrder(questionOption.getOrder());
		if(questionOption.getCode() != null)
			questionOptionDTO.setCode(questionOption.getCode());
		if(valueTexts!=null) {
			for (String checked : valueTexts) {
				if (questionOptionDTO.getId() == Long.parseLong(checked)) {
							questionOptionDTO.setChecked(true);
				}
			
			}
		}

		return questionOptionDTO;
	}
}
