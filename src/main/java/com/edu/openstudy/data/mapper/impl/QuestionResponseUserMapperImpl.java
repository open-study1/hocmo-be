package com.edu.openstudy.data.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserViewDTO;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.ExerciseMapper;
import com.edu.openstudy.data.mapper.QuestionMapper;
import com.edu.openstudy.data.mapper.QuestionResponseUserMapper;
import com.edu.openstudy.data.repository.ExamRepository;
import com.edu.openstudy.data.repository.ExerciseRepository;
import com.edu.openstudy.data.repository.QuestionRepository;
import com.edu.openstudy.service.QuestionService;

@Component
public class QuestionResponseUserMapperImpl implements QuestionResponseUserMapper {
	@Autowired
	private QuestionService questionService;
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private ExamMapper examMapper;
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private ExerciseRepository exerciseRepository;
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private ExerciseMapper exerciseMapper;
	@Override
	public QuestionResponseUserEntity mapToEntity(QuestionResponseUserDTO dto) {
		if (dto == null)
			return null;
		QuestionResponseUserEntity entity = new QuestionResponseUserEntity();
		if (dto.getId() != null)
			entity.setId(dto.getId());
		entity.setValueText(dto.getValueText());
		// set question
		QuestionDTO questionDTO = dto.getQuestionDTO();
		if (questionDTO != null) {
			setQuestion(questionDTO, entity);
		}
		if(dto.getExerciseDTO()!=null) {
			ExerciseDTO exerciseDTO = dto.getExerciseDTO();
			if (exerciseRepository.existsById(exerciseDTO.getId())) {
				entity.setExercise(exerciseRepository.findById(exerciseDTO.getId()).get());
			}
		}

		if(dto.getExamDTO()!=null) {
			ExamDTO examDTO = dto.getExamDTO();
			if (examRepository.existsById(examDTO.getId())) {
				entity.setExam(examRepository.findById(examDTO.getId()).get());
			}
		}

		return entity;
	}

	@Override
	public QuestionResponseUserDTO mapToDTO(QuestionResponseUserEntity entity) {
		if (entity == null)
			return null;
		QuestionResponseUserDTO dto = new QuestionResponseUserDTO();
		dto.setValueText(entity.getValueText());
		dto.setId(entity.getId());
		QuestionEntity question = questionRepository.findById(entity.getQuestion().getId()).get();
		if (question != null)
			setQuestionDTO(question, dto);
		return dto;
	}
	
	@Override
	public QuestionResponseUserViewDTO mapToViewDTO(QuestionResponseUserEntity entity) {
		if (entity == null)
			return null;
		QuestionResponseUserViewDTO dto = new QuestionResponseUserViewDTO();
		dto.setValueText(entity.getValueText());
		dto.setId(entity.getId());
		QuestionEntity question = questionRepository.findById(entity.getQuestion().getId()).get();
		if (question != null) {
			dto.setQuestionDTO(this.questionMapper.mapToExerciseDTO(question, entity.getQuestion().getId(),entity.getCreatedBy()));
		}
			
		return dto;
	}
	
	@Override
	public QuestionResponseUserViewDTO mapToViewExamDTO(QuestionResponseUserEntity entity) {
		if (entity == null)
			return null;
		QuestionResponseUserViewDTO dto = new QuestionResponseUserViewDTO();
		dto.setValueText(entity.getValueText());
		dto.setId(entity.getId());
		QuestionEntity question = questionRepository.findById(entity.getQuestion().getId()).get();
		if (question != null) {
			dto.setQuestionDTO(this.questionMapper.mapToExamDTO(question, entity.getQuestion().getId(),entity.getCreatedBy(),entity.getCreatedDate()));
		}
			
		return dto;
	}

	private void setQuestion(QuestionDTO questionDmapToDTO, QuestionResponseUserEntity questionResponseUser) {
		Long idQuestion = questionDmapToDTO.getId();
		QuestionEntity question = this.questionMapper.mapToEntity(questionService.findById(idQuestion));
		questionResponseUser.setQuestion(question);
	}

	private void setQuestionDTO(QuestionEntity question, QuestionResponseUserDTO dto) {
		QuestionDTO questionDTO = this.questionMapper.mapHaveSolution(question);
		dto.setQuestionDTO(questionDTO);
	}

}
