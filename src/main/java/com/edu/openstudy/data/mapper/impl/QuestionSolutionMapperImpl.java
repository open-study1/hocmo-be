package com.edu.openstudy.data.mapper.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.dto.QuestionSolutionDTO;
import com.edu.openstudy.data.entity.QuestionSolutionEntity;
import com.edu.openstudy.data.mapper.QuestionOptionMapper;
import com.edu.openstudy.data.mapper.QuestionSolutionMapper;
import com.edu.openstudy.data.repository.QuestionOptionRepository;
@Component
public class QuestionSolutionMapperImpl implements QuestionSolutionMapper {
	@Autowired
	private QuestionOptionRepository questionOptionRepository;
	@Autowired
	private QuestionOptionMapper questionOptionMapper;
	@Override
	public QuestionSolutionDTO mapToDTO(QuestionSolutionEntity entity)
	{
		if(entity == null)
			return null;
		QuestionSolutionDTO dto = new QuestionSolutionDTO();
		dto.setId(entity.getId());
		
		//Set option
		List<String> listSolution = new ArrayList<String>(Arrays.asList(entity.getSolution().split(",")));
		List<QuestionOptionDTO> optionDTOs = new ArrayList<>();
		for (String solution : listSolution) {
			optionDTOs.add(questionOptionMapper.mapToDTO(questionOptionRepository.findById(Long.parseLong(solution)).get()));
		}
		dto.setSolution(optionDTOs);
		dto.setExplanation(entity.getExplanation());
		return dto;
	}
	
	@Override
	public QuestionSolutionEntity mapToEntity(QuestionSolutionDTO dto)
	{
		if(dto == null)
			return null;
		QuestionSolutionEntity entity = new QuestionSolutionEntity();
		entity.setId(dto.getId());
		entity.setSolution(dto.getSolutionStr());
		entity.setExplanation(dto.getExplanation());
		return entity;
	}
}
