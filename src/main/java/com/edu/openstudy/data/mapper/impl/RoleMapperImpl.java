package com.edu.openstudy.data.mapper.impl;

import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.RoleDTO;
import com.edu.openstudy.data.entity.RoleEntity;
import com.edu.openstudy.data.mapper.RoleMapper;

@Component
public class RoleMapperImpl implements RoleMapper{

	@Override
	public RoleEntity mapToEntity(RoleDTO dto) {
		if(dto == null)
			return null;
		RoleEntity entity = new RoleEntity();
		entity.setId(dto.getId());
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setCreatedDate(dto.getCreatedDate());
		entity.setModifiedDate(dto.getModifiedDate());
		return entity;
	}

	@Override
	public RoleDTO mapToDTO(RoleEntity entity) {
		if(entity == null)
			return null;
		RoleDTO dto = new RoleDTO();
		dto.setId(entity.getId());
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedDate(entity.getModifiedDate());
		return dto;
	}

}
