package com.edu.openstudy.data.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.edu.openstudy.data.dto.RoleDTO;
import com.edu.openstudy.data.dto.UserDTO;
import com.edu.openstudy.data.entity.RoleEntity;
import com.edu.openstudy.data.entity.UserEntity;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.RoleMapper;
import com.edu.openstudy.data.mapper.UserMapper;

@Component
public class UserMapperImpl implements UserMapper{
	
	@Autowired
	private RoleMapper roleMapper ;

	@Override
	public UserEntity mapToEntity(UserDTO dto) {
		if(dto == null)
			return null;
		UserEntity entity = new UserEntity();
		entity.setName(dto.getName());
		entity.setVerifiedEmail(dto.isVerifiedEmail());
		entity.setAvatar(dto.getAvatar());
		entity.setEmail(dto.getEmail());
		entity.setPassword(dto.getPassword());
		entity.setRole(roleMapper.mapToEntity(dto.getRoleDTO()));
		entity.setPhone(dto.getPhone());
		entity.setStudent(dto.isStudent());
		entity.setCreatedDate(dto.getCreatedDate());
		entity.setModifiedDate(dto.getModifiedDate());
		return entity;
	}

	@Override
	public UserDTO mapToDTO(UserEntity entity) {
		if(entity == null)
			return null;
		UserDTO dto = new UserDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setVerifiedEmail(entity.isVerifiedEmail());
		dto.setAvatar(entity.getAvatar());
		dto.setEmail(entity.getEmail());
		dto.setRoleDTO(roleMapper.mapToDTO(entity.getRole()));
		dto.setPhone(entity.getPhone());
		dto.setStudent(entity.isStudent());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedDate(entity.getModifiedDate());
		return dto;
	}

}
