package com.edu.openstudy.data.mapper.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.edu.openstudy.common.util.HandleGeneral;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.dto.UserScoreViewDTO;
import com.edu.openstudy.data.entity.UserScoreEntity;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.ExerciseMapper;
import com.edu.openstudy.data.mapper.QuestionMapper;
import com.edu.openstudy.data.mapper.QuestionResponseUserMapper;
import com.edu.openstudy.data.mapper.UserScoreMapper;
import com.edu.openstudy.data.repository.QuestionResponseUserRepository;
import com.edu.openstudy.data.repository.QuestionSolutionRepository;
import com.edu.openstudy.service.QuestionService;
import com.edu.openstudy.service.QuestionSolutionService;

@Component
public class UserScoreMapperImpl implements UserScoreMapper {
	@Autowired
	private ExamMapper examMapper ;
	@Autowired
	private QuestionResponseUserMapper questionResponseUserMapper;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private QuestionSolutionService questionSolutionService;
	@Autowired
	private HandleGeneral handleGeneral;
	@Autowired
	private QuestionSolutionRepository questionSolutionRepository;
	@Autowired
	private QuestionResponseUserRepository questionResponseUserRepository;
	@Autowired
	private MessageSource messageSource;
	@Override
	public UserScoreEntity mapToEntity(UserScoreDTO dto) {
		if(dto == null)
			return null;
		UserScoreEntity entity = new UserScoreEntity();
		entity.setId(dto.getId());
		entity.setScore(dto.getScore());
		entity.setCreatedBy(dto.getCreatedBy());
		entity.setCreatedDate(dto.getCreatedDate());
		entity.setModifiedBy(dto.getModifiedBy());
		entity.setModifiedDate(dto.getModifiedDate());
		entity.setTimeWork(dto.getTimeWork());
		entity.setWrongAnswer(dto.getWrongAnswer());
		entity.setRightAnswer(dto.getRightAnswer());
		entity.setTotalQuestion(dto.getTotalQuestion());
//		entity.setExam(examMapper.mapToEntity(dto.getExamDTO()));
		return entity;
	}
	@Override
	public UserScoreDTO mapToDTO(UserScoreEntity entity) {
		if(entity == null)
			return null;
		UserScoreDTO dto = new UserScoreDTO();
		dto.setId(entity.getId());
		dto.setScore(entity.getScore());
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
		dto.setTimeWork(entity.getTimeWork());
		dto.setWrongAnswer(entity.getWrongAnswer());
		dto.setRightAnswer(entity.getRightAnswer());
		dto.setTotalQuestion(entity.getTotalQuestion());
		dto.setExamDTO(examMapper.mapToViewDetailDTO(entity.getExam(), UserContext.getId(),entity.getCreatedDate() ));
	
		return dto;
	}

	@Override
	public UserScoreViewDTO mapToViewDTO(UserScoreEntity entity) {
		if(entity == null)
			return null;
		UserScoreViewDTO dto = new UserScoreViewDTO();
		dto.setId(entity.getId());
		dto.setScore(entity.getScore());
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setModifiedBy(entity.getModifiedBy());
		dto.setModifiedDate(entity.getModifiedDate());
		dto.setTimeWork(entity.getTimeWork());
		dto.setWrongAnswer(entity.getWrongAnswer());
		dto.setRightAnswer(entity.getRightAnswer());
		dto.setTotalQuestion(entity.getTotalQuestion());
		return dto;
	}

}
