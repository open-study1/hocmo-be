package com.edu.openstudy.data.repository;

import com.edu.openstudy.data.entity.ChapterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChapterRepository extends JpaRepository<ChapterEntity, Long> {
    	List<ChapterEntity> findAllByParentCode(String code);
//
//    @Query("SELECT c FROM ChapterEntity c INNER JOIN LessonChapterEntity lc ON lc.chapter.id=c.id  " +
//            "INNER JOIN LessonEntity l ON l.id=lc.lesson.id " +
//            "INNER JOIN QuestionGroupEntity qg ON qg.lesson.id = l.id " +
//            "INNER JOIN QuestionEntity q ON q.questionGroup.id = qg.id " +
//            "WHERE q.id=:id")
//    List<ChapterEntity> getByQuestion(@Param("id") Long id);
//
//    @Query("SELECT c.code FROM ChapterEntity c INNER JOIN LessonChapterEntity lc ON lc.chapter.id=c.id  " +
//            "INNER JOIN LessonEntity l ON l.id=lc.lesson.id " +
//            "INNER JOIN QuestionGroupEntity qg ON qg.lesson.id = l.id " +
//            "INNER JOIN QuestionEntity q ON q.questionGroup.id = qg.id " +
//            "WHERE q.id=:id")
//    List<String> getCodeByQuestionId(@Param("id") Long id);
//
//    @Query("SELECT c FROM ChapterEntity c " +
//            "INNER JOIN LessonChapterEntity lc ON lc.chapter.id=c.id " +
//            "WHERE lc.lesson.id=:lessonId")
//    List<ChapterEntity> getByLesson(@Param("lessonId") Long id);
//
//
    @Query("SELECT c.code FROM ChapterEntity c WHERE c.parentCode =:code")
    List<String> findChapterCodeByParentCode(@Param("code") String code);

//    boolean existsByCode(String code);
//
    Optional<ChapterEntity> findByCode(String code);
//
//    List<ChapterEntity> findAllByGroupChapter(String groupName);
}
