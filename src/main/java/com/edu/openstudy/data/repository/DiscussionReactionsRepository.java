package com.edu.openstudy.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.DiscussionReactionsEntity;
@Repository
public interface DiscussionReactionsRepository extends JpaRepository<DiscussionReactionsEntity, Long> {
	List<DiscussionReactionsEntity> findAllByDiscussionId(long id);
	
	List<DiscussionReactionsEntity> findAllByCreatedBy(long id);

	Optional<DiscussionReactionsEntity>  findByDiscussionIdAndCreatedBy(long discussionId, long UserId);
	
	List<DiscussionReactionsEntity> removeByDiscussionIdAndCreatedBy(long discussionId, long UserId);

}
