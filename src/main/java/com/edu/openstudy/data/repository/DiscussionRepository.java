package com.edu.openstudy.data.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.DiscussionEntity;

@Repository
public interface DiscussionRepository extends JpaRepository<DiscussionEntity, Long> {


	Page<DiscussionEntity> findAllByLessonIdAndParentIdOrderByCreatedDateDesc(long lessson, Long parentId, Pageable pageable);

	List<DiscussionEntity> findAllByParentId(long id);

	@Query(value = "SELECT d FROM DiscussionEntity d " +
			"WHERE d.lesson.id = :lessonId")
	List<DiscussionEntity> findByLessonId(@Param("lessonId") Long lessonId);
	
	DiscussionEntity findByCreatedDateAndCreatedByAndLessonId(LocalDateTime createdDate, Long userId, Long lessonId);
}
