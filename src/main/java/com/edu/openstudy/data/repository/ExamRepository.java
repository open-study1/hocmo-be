package com.edu.openstudy.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.ExamEntity;

@Repository
public interface ExamRepository extends JpaRepository<ExamEntity, Long> {
	@Query(value = "SELECT e " +
	"FROM ExamEntity e " +
	"WHERE e IN (SELECT u.exam FROM UserScoreEntity u WHERE u.createdBy= :userId)")
	List<ExamEntity> findExamPracticedByUser(long userId);
	
}
