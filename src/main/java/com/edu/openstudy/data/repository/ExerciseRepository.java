package com.edu.openstudy.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.ExerciseEntity;

@Repository
public interface ExerciseRepository extends JpaRepository<ExerciseEntity, Long> {	
	Optional<ExerciseEntity> findByLessonId(long id);
	
	
}
