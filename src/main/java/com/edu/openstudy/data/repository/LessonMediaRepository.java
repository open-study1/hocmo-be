package com.edu.openstudy.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.LessonMediaEntity;

@Repository
public interface LessonMediaRepository extends JpaRepository<LessonMediaEntity, Long> {
//	@Query(value = "SELECT lm " +
//			"FROM LessonMediaEntity lm " +
//			"WHERE lm.lesson.id = :lessonId")
//	List<LessonMediaEntity> findByLessonId(@Param("lessonId") Long lessonId);
}
