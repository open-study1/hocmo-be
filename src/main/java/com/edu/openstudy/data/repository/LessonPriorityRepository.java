package com.edu.openstudy.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.LessonPriorityEntity;

@Repository
public interface LessonPriorityRepository extends JpaRepository<LessonPriorityEntity, Long> {
//	Optional<LessonPriorityEntity> findByLessonIdAndCreatedBy(Long lessonId, Long createBy);
}
