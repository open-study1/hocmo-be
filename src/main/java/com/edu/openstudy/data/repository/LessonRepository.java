package com.edu.openstudy.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.LessonEntity;

@Repository
public interface LessonRepository extends JpaRepository<LessonEntity, Long>, JpaSpecificationExecutor<LessonEntity> {

//	@Query("SELECT l " + "FROM  LessonEntity l  "
//			+ "WHERE l.id in (select  lc.lesson.id from LessonChapterEntity lc where lc.chapter.id =:chapterId)")
//	List<LessonEntity> getLessonByChapter(Long chapterId);
//
//	
//	@Query("SELECT l " + "FROM LessonEntity l " + "INNER JOIN LessonChapterEntity lc ON l.id = lc.lesson.id "
//			+ "WHERE l.zOrder = :order AND lc.chapter.id = :id")
//	Optional<LessonEntity> findByChapterAndZorder(@Param("order") Long order, @Param("id") Long chapterId);
//

//	// @Query("SELECT l FROM LessonEntity l, LessonChapterEntity lc,
//	// QuestionGroupEntity qg, QuestionEntity q "
////            + "WHERE lc.lesson.id = l.id AND lc.lesson.questionGroup.id =qg.id AND q.questionGroup.id=qg.id AND q.id=:questionId ")
//	@Query("SELECT l " + "FROM LessonEntity l " + "INNER JOIN QuestionGroupEntity qg ON l.questionGroup.id =qg.id "
//			+ "INNER JOIN QuestionEntity q ON q.questionQuestionGroup.questionGroup.id=qg.id " + "WHERE q.id=:questionId")
//	Optional<LessonEntity> findByQuestionId(@Param("questionId") Long questionId);
//
//	// @Query("SELECT l.content FROM LessonEntity l, LessonChapterEntity lc,
//	// QuestionGroupEntity qg, QuestionEntity q "
////            + "WHERE lc.lesson.id = l.id AND lc.lesson.questionGroup.id =qg.id AND q.questionGroup.id=qg.id AND q.id=:questionId ")
//	@Query("SELECT l.content " + "FROM LessonEntity l INNER JOIN QuestionGroupEntity qg ON l.questionGroup.id =qg.id "
//			+ "INNER JOIN QuestionEntity q ON q.questionQuestionGroup.questionGroup.id=qg.id " + "WHERE q.id=:questionId")
//	String getContentByQuestionId(@Param("questionId") Long questionId);


//	@Query("select lp.lesson.id from LessonPriorityEntity lp where lp.modifiedBy =:userId")
//	List<Long> getAllIdByUserIdPriority(@Param("userId") Long userId);
//
//	@Query("select qg.lesson.id from QuestionGroupEntity qg where qg.id in (select  q.questionGroup.id from QuestionEntity q where q.id in (select us.question.id from UserScoreEntity us where us.user =:userId))  ")
//	List<Long> getAllIdByUserPractice(@Param("userId") Long userId);
//
	@Query("SELECT l.id " + "FROM  LessonEntity l  "
	+ "WHERE l.chapter.code =:code ")
	List<Long> findLessonIdByChapterCode(String code);

	@Query("select l.id from LessonEntity l where l.chapter = null")
	List<Long> getAllIdLessonNoChapter();

}
