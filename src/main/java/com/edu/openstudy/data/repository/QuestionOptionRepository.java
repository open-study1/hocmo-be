package com.edu.openstudy.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.QuestionOptionEntity;
@Repository
public interface QuestionOptionRepository extends JpaRepository<QuestionOptionEntity, Long> {
	@Query(value = "SELECT qo " +
			"FROM QuestionOptionEntity qo " +
			"WHERE qo.question.id = :id")
	List<QuestionOptionEntity> findQuestionOptionByQuestionId(@Param("id") long id);
//	@Modifying
//	@Query(value = "DELETE FROM QuestionOptionEntity qo " +
//			"WHERE qo.question.id = :questionId")
//	void deleteQuestionOptionByQuestion(@Param("questionId") long questionId);
	
	
}
