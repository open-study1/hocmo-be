package com.edu.openstudy.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.QuestionEntity;

@Repository
public interface QuestionRepository extends JpaRepository<QuestionEntity, Long> {

	
	@Query(value = "SELECT q " +
            "FROM QuestionEntity q " +
            "WHERE q.id IN (SELECT qe.question.id  FROM QuestionExamEntity qe WHERE qe.exam.id = :id AND qe.order = :order)")
    Optional<QuestionEntity> findByExamIdAndOrder(@Param("id") long examId,@Param("order") long order);

    @Query(value = "SELECT q " +
            "FROM QuestionEntity q " +
            "WHERE q.id IN (SELECT qe.question.id  FROM QuestionExamEntity qe WHERE qe.exam.id = :id)")
    List<QuestionEntity> findQuestionByExamId(@Param("id") long id);
    
    @Query(value = "SELECT q " +
            "FROM QuestionEntity q " +
            "WHERE q.id IN (SELECT qe.question.id  FROM QuestionExerciseEntity qe WHERE qe.exercise.id = :id)")
    List<QuestionEntity> findQuestionByExercise(@Param("id") long id);
//
//    @Query(value = "SELECT q.name " +
//            "FROM QuestionEntity q " +
//            "WHERE q.id = :id")
//    String getNameByQuestionId(Long id);
//
//    @Modifying
//    @Query(value = "DELETE FROM QuestionEntity q " +
//            "WHERE q.questionQuestionGroup.questionGroup.id = :id")
//    void deleteByQuestionGroup(@Param("id") long id);

}
