package com.edu.openstudy.data.repository;

import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionResponseUserRepository extends JpaRepository<QuestionResponseUserEntity, Long> {
//    @Query(value = "SELECT qru.valueText " +
//            "FROM UserScoreEntity u " +
//            "INNER JOIN QuestionResponseUserEntity qru ON  u.questionResponseUser.id = qru.id "
//            + "WHERE u.id IN (:listId)")
//    List<String> getValueTextByUserScore(@Param("listId") List<Long> userScoreId);

	  @Query(nativeQuery = true,value = "SELECT value_text " +
	  "FROM question_response_user qru WHERE qru.exam_id =:examId AND qru.question_id =:questionId AND qru.created_by =:userId AND qru.created_date =:createDate")
	  String getValueTextOfExamByUser(long examId, long questionId, long userId, String createDate);
	  
	  
	  @Query(value = "SELECT qru.valueText " +
	  "FROM QuestionResponseUserEntity qru WHERE qru.exercise.id =:exerciseId AND qru.question.id =:questionId AND qru.createdBy =:userId ")
	  String getValueTextOfExerciseByUser(long exerciseId, long questionId, long userId);
	
//    //	@Query(value = "SELECT qru FROM QuestionResponseUser qru WHERE qru.question.id = :questionId AND qru.userId=:userId AND qru.createdDate =:createdDate")
//    @Query(nativeQuery = true, value = "SELECT * " +
//            "FROM QUESTION_RESPONSE_USER qru " +
//            "WHERE qru.[question_id] =:questionId AND qru.USER_ANSWER_ID=:userId AND convert(varchar(19), qru.CREATED_DATE, 120) =:createdDate")
//    List<QuestionResponseUserEntity> findToDoScore(@Param("questionId") Long questionId, @Param("userId") Long userId,
//                                                      @Param("createdDate") LocalDateTime createDate);
//
//    @Modifying
//    @Query(value = "DELETE QuestionResponseUserEntity q " +
//            "WHERE q.id=:id")
//    void deleteByIdResponse(@Param("id") Long id);
	  
	  Optional<QuestionResponseUserEntity> findByQuestionIdAndExerciseId(Long questionId, Long examId);

	  Boolean existsByQuestionIdAndExerciseId(Long questionId, Long examId);
}
