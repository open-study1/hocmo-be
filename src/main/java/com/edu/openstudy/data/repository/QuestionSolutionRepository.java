package com.edu.openstudy.data.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.QuestionSolutionEntity;
@Repository
public interface QuestionSolutionRepository extends JpaRepository<QuestionSolutionEntity, Long> {
//	@Query(value = "SELECT qs " +
//			"FROM QuestionSolutionEntity qs " +
//			"WHERE qs.question.id = :id")
//	List<QuestionSolutionEntity> getQuestionSolutionByQuestionId(@Param("id") long id);
//	@Query(value = "SELECT COUNT(qs) " +
//			"FROM QuestionSolutionEntity qs " +
//			"WHERE qs.question.questionGroup.lesson.id = :lessonId")
//	int countQuestionSoltutionByLessonId(@Param("lessonId") long lessonId);
//	@Modifying
//	@Query(value = "DELETE FROM QuestionSolutionEntity qs " +
//			"WHERE qs.question.id = :questionId")
//	void deleteByQuestion(@Param("questionId") long questionId);
//	Optional<QuestionSolutionEntity> findByValueText(String valueText);
}
