package com.edu.openstudy.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.QuestionTypeEntity;
@Repository
public interface QuestionTypeRepository extends JpaRepository<QuestionTypeEntity, Long> {

}
