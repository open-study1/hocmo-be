package com.edu.openstudy.data.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.entity.RoleEntity;
import com.edu.openstudy.data.entity.UserEntity;

@Repository
public interface UserRepository  extends JpaRepository<UserEntity, Long> {
    boolean existsByEmail(String email);
	Optional<UserEntity> findByEmail(String email);
	
	@Query("Select u.role from UserEntity u Where u.email =: email ")
	RoleEntity findRoleByEmail(String email);
//	@Query("select c.gender, count(c.username) from User c group by c.gender")
//	List<Object[]> statisticsBySex();
//	@Query("SELECT count(u.username) FROM User u where  u.createDate BETWEEN :from and  :to")
//	Long getCountUsernameByYear(@Param("from") LocalDate from, @Param("to") LocalDate to);
//	@Query("SELECT r.name, count(r.id) FROM User c, Role r WHERE c.role.id = r.id GROUP by r.name")
//	List<Object[]> statisticsByRole();
//	@Query("SELECT s.name, count(u.username) from User u , Status s where u.status = s.id group by s.name")
//	List<Object[]> statisticsByStatus();
//	@Query(nativeQuery = true, 	value="SELECT count(c.username) FROM user c where  c.create_Date <= CURDATE() and c.create_Date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)")
//	Long statisticsNewUser();

}
