package com.edu.openstudy.data.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.UserScoreEntity;

@Repository
public interface UserScoreRepository extends JpaRepository<UserScoreEntity, Long> {
//	@Query(value = "SELECT u " +
//			"FROM UserScoreEntity u JOIN FETCH u.question  " +
//			"WHERE u.question.questionGroup.lesson.id = :lessonId AND u.user= :userId")
//	List<UserScoreEntity> getByLessonAndUser(@Param("lessonId") Long lessonId, @Param("userId") Long userId);
//
//	@Query(value = "SELECT u " +
//			"FROM UserScoreEntity u JOIN FETCH u.question  " +
//			"WHERE u.question.questionGroup.lesson.id = :lessonId ")
//	List<UserScoreEntity> getByLesson(@Param("lessonId") Long lessonId);
//
//	@Query(value = "SELECT u " +
//			"FROM UserScoreEntity u " +
//			"WHERE u.question.id IN (:questionIds) AND u.user=:userId")
//	List<UserScoreEntity> getByQuestionAndUser(@Param("questionIds") List<Long> questionIds, @Param("userId") Long userId);
//
//	@Query(value = "SELECT u " +
//			"FROM UserScoreEntity u " +
//			"WHERE u.question.id IN (:questionIds)")
//	List<UserScoreEntity> getByQuestion(@Param("questionIds") List<Long> questionIds);
//
//	@Query(value = "SELECT u " +
//			"FROM UserScoreEntity u " +
//			"WHERE u.question.id = :questionId")
//	List<UserScoreEntity> getByQuestionId(@Param("questionId") Long questionId);
//
//	@Query(value = "SELECT u.id " +
//			"FROM UserScoreEntity u " +
//			"WHERE u.createdDate = :dateTime")
////	@Query(nativeQuery = true, value = "SELECT * FROM VDT.USER_SCORE u WHERE convert(varchar(19), u.CREATED_DATE, 120)= :dateTime")//using sql server
//	List<Long> getIdByCreateDateEquals(LocalDateTime dateTime);
//	
//	@Query(nativeQuery = true, value = "SELECT * " +
//			"FROM VDT.USER_SCORE u " +
//			"WHERE convert(varchar(19), u.CREATED_DATE, 120)= :dateTime")//using sql server
//	List<Long> findIdByCreateDateEquals(LocalDateTime dateTime);
//
//	@Query(value = "SELECT u " +
//			"FROM UserScoreEntity u " +
//			"WHERE u.questionSolution.id=:solutionId")
//	List<UserScoreEntity> getBySolution(@Param("solutionId") long solutionId);
//	
//	@Modifying
//	@Query(value = "DELETE UserScoreEntity u " +
//			"WHERE u.id=:id")
//	void deleteByIdUserScore(@Param("id") Long id);
//	@Query(value = "DELETE UserScoreEntity u " +
//			"WHERE u.id=:id")
	
	@Query(value ="SELECT  AVG(u.score) FROM UserScoreEntity u WHERE u.exam.id= :examId AND u.createdBy= :userId")
	long getMediumScoreByExamIdAndCreatedBy(long examId, long userId);
	
	@Query(value ="SELECT  SUM(u.totalQuestion) FROM UserScoreEntity u WHERE u.exam.chapter.id= :chapterId AND u.createdBy= :userId")
	Long getTotalAnswerByExamIdAndCreatedBy(long chapterId, long userId);
	
	@Query(value ="SELECT  SUM(u.rightAnswer) FROM UserScoreEntity u WHERE u.exam.chapter.id= :chapterId AND u.createdBy= :userId")
	Long getRightAnswerByExamIdAndCreatedBy(long chapterId, long userId);
	
	@Query(value ="SELECT  SUM(u.wrongAnswer) FROM UserScoreEntity u WHERE u.exam.chapter.id= :chapterId AND u.createdBy= :userId")
	Long getWrongAnswerByExamIdAndCreatedBy(long chapterId, long userId);
	
	boolean existsByExamId(long examId);
		
	long countByExamIdAndCreatedBy(long examId, long userId);
	
	Optional<UserScoreEntity> findFirstByExamIdAndCreatedByOrderByScoreDesc(long examId, long userId);
	
	Optional<UserScoreEntity> findFirstByExamIdAndCreatedByOrderByScoreAsc(long examId, long userId);
	
	List<UserScoreEntity> findAllByCreatedByOrderByCreatedDateDesc(long userId);
}
