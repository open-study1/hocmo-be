package com.edu.openstudy.data.repository.specification;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import com.edu.openstudy.constant.Constant;
import com.edu.openstudy.data.dto.BaseLessonPriorityDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.ChapterEntity;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonMediaEntity;
import com.edu.openstudy.data.entity.LessonPriorityEntity;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.request.RequestLessonDTO;

public final class LessonSpecification implements Specification<LessonEntity> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.DATE_TIME_FORMAT);

	@Override
	public Predicate toPredicate(Root<LessonEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		return null;
	}

	public static Specification<LessonEntity> filter(RequestLessonDTO requestDTO, List<Long> lessonChapterIds,
			List<Long> questionGroupIds, List<Long> lessonPriorityIds, List<Long> lessonIdNotExplanations,
			List<Long> lessonIdNoChapters) {
		return ((root, query, criteriaBuilder) -> {

			List<Predicate> predicates = new ArrayList<>();
			List<Predicate> predicatesChapter = null;
			Boolean status = requestDTO.getStatus();
			String chapterCode = requestDTO.getChapterCode();
			Long zOrder = requestDTO.getZOrder();
			String searchKeyWord = requestDTO.getSearchKeyWord();
			Boolean praticeStatus = requestDTO.getPracticeStatus();
			Integer priority = requestDTO.getPriority();
			if (status != null) {
				predicates.add(hasBooleanPredicate(status, "status", criteriaBuilder, root));
			}
			if (zOrder != null) {
				predicates.add(hasLongEqualPredicate(zOrder, "zOrder", criteriaBuilder, root));
			}
			if (searchKeyWord != null & isInteger(searchKeyWord) == false) {
				predicates.add(hasStringLikePredicate(searchKeyWord, "content", criteriaBuilder, root));
			}
			if (searchKeyWord != null & isInteger(searchKeyWord) == true) {
				predicates.add(hasLongEqualPredicate(Long.parseLong(searchKeyWord), "zOrder", criteriaBuilder, root));
			}
			if (chapterCode != null) {
					predicatesChapter = new ArrayList<>();
					for (Long lessonChapterId : lessonChapterIds) {
						predicatesChapter
								.add(hasChapter(lessonChapterId, "id", criteriaBuilder, root));
					}
					predicates.add(criteriaBuilder.or(predicatesChapter.toArray(new Predicate[predicates.size()])));
			}
			if (praticeStatus != null) {
				predicates.add(hasPractice(praticeStatus, questionGroupIds, criteriaBuilder, root));
			}
			if (priority != null) {
				if (priority == Constant.ALL_MAKR || priority == Constant.ALL_NOT_MARK) {
					predicates.add(hasPriorityOfUser(priority, lessonPriorityIds, criteriaBuilder, root));
				} else {
					predicates.add(hasPriority(priority, criteriaBuilder, root));
					predicates.add(hasPriorityOfUser(priority, lessonPriorityIds, criteriaBuilder, root));
				}
			}

			return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		});
	}

	// handle filter By Shadowing
	private static Predicate hasShadowing(Boolean condition, String value, String field,
			CriteriaBuilder criteriaBuilder, Root<LessonEntity> root) {
		Join<LessonEntity, LessonMediaEntity> lessonMedia = root.join("lessonMedias");
		if (condition == true) {
			return criteriaBuilder.equal(lessonMedia.get(field).as(String.class), value);

		} else {
			return criteriaBuilder.notEqual(lessonMedia.get(field).as(String.class), value);
		}
	}

	// handle filter By Chapter
	private static Predicate hasChapter(Long value, String field, CriteriaBuilder criteriaBuilder,
		Root<LessonEntity> root) {
			Join<LessonEntity, ChapterEntity> chapter = root.join("chapter");
			return criteriaBuilder.equal(chapter.get(field).as(Long.class), value);
	}

	// handle filter By Priority
	private static Predicate hasPriority(Integer value, CriteriaBuilder criteriaBuilder, Root<LessonEntity> root) {
		Join<LessonEntity, LessonPriorityEntity> lessonPriority = root.join("lessonPriorities");
		return criteriaBuilder.equal(lessonPriority.get("priority").as(Integer.class), value);
	}

	// handle filter By Priority of User
	private static Predicate hasPriorityOfUser(Integer condition, List<Long> lessonPriorityIds,
			CriteriaBuilder criteriaBuilder, Root<LessonEntity> root) {
		if (condition == Constant.ALL_NOT_MARK) {
			return criteriaBuilder.in(root.get("id")).value(lessonPriorityIds).not();
		} else {
			return criteriaBuilder.in(root.get("id")).value(lessonPriorityIds);
		}
	}

	// handle filter By PracticeStatus
	private static Predicate hasPractice(Boolean value, List<Long> questionGroupIds, CriteriaBuilder criteriaBuilder,
			Root<LessonEntity> root) {
		Join<LessonEntity, ExamEntity> lessonQuestionGroup = root.join("questionGroup");
		if (value == true) {
			return criteriaBuilder.in(lessonQuestionGroup.get("id")).value(questionGroupIds);
		} else {
			return criteriaBuilder.in(lessonQuestionGroup.get("id")).value(questionGroupIds).not();

		}
	}

	// handle filter By Explanation
	private static Predicate hasExplanation(Boolean value, List<Long> lessonIdNotExplanation,
			CriteriaBuilder criteriaBuilder, Root<LessonEntity> root) {
		if (value == true) {
			return criteriaBuilder.in(root.get("id")).value(lessonIdNotExplanation).not();
		} else {
			return criteriaBuilder.in(root.get("id")).value(lessonIdNotExplanation);

		}
	}

	// handle filter By Long Equal Predicate
	private static Predicate hasLongEqualPredicate(Long value, String field, CriteriaBuilder criteriaBuilder,
			Root<LessonEntity> root) {
		return criteriaBuilder.equal(root.get(field).as(Long.class), value);
	}

	// handle filter By Boolean Equal Predicate
	private static Predicate hasBooleanPredicate(Boolean value, String field, CriteriaBuilder criteriaBuilder,
			Root<LessonEntity> root) {
		return criteriaBuilder.equal(root.get(field).as(Boolean.class), value);
	}

	// handle filter By String Like Predicate
	private static Predicate hasStringLikePredicate(String value, String field, CriteriaBuilder criteriaBuilder,
			Root<LessonEntity> root) {
		return criteriaBuilder.like(root.get(field).as(String.class), "%" + value + "%");
	}

	private static Predicate handleFilterStartAndEndTime(String startTimeStr, String endTimeStr,
			CriteriaBuilder criteriaBuilder, Root<LessonEntity> root) {
		if (startTimeStr != null || endTimeStr != null) {
			LocalDateTime startTime = null;
			LocalDateTime endTime = null;
			if (startTimeStr != null) {
				startTime = parseLocalDateTime(startTimeStr);
			}
			if (endTimeStr != null) {
				endTime = parseLocalDateTime(endTimeStr);
			}

			// between start and end time
			if (startTime != null && endTime != null) {
				return criteriaBuilder.between(root.get("modifiedDate").as(LocalDate.class), startTime.toLocalDate(),
						endTime.toLocalDate());
			}
			// select date to search
			if (startTime != null && endTime == null)
				return criteriaBuilder.equal(root.get("modifiedDate").as(LocalDate.class), startTime.toLocalDate());

		}
		return null;
	}

	private static LocalDateTime parseLocalDateTime(String dateTimeStr) throws DateTimeParseException {

		LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, Constant.DATE_TIME_FORMATTER);
		return dateTime;

	}

	private static boolean isInteger(String s) {
		if (s == null || s.equals("")) {
			return false;
		}
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {

		}
		return false;
	}

}
