package com.edu.openstudy.data.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RequestLessonDTO {
    private String chapterCode;
    private Long zOrder;
    private Integer sort;
    private Integer priority;
    private Boolean practiceStatus;
    private Boolean status;
    private String searchKeyWord;
    private Integer pageNumber;
    private Integer limit;
}
