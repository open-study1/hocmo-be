package com.edu.openstudy.data.result;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.edu.openstudy.constant.Constant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResult implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer code;
    private String message;

    public BaseResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResult() {
        this(HttpStatus.OK.value(), Constant.SUCCESS);
    }

    public BaseResult(String message) {
        this(HttpStatus.OK.value(), message);
    }
}
