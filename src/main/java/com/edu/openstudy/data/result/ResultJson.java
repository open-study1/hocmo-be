package com.edu.openstudy.data.result;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ResultJson extends BaseResult {
    private static final long serialVersionUID = 1L;
    private Object data;

    public ResultJson(Object data) {
        super();
        this.data = data;
    }

    public ResultJson(Integer code, String messge, Object data) {
        super(code, messge);
        this.data = data;
    }

}
