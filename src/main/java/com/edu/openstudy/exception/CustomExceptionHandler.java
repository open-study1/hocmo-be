package com.edu.openstudy.exception;

import com.edu.openstudy.common.Logger;
import com.edu.openstudy.data.result.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.format.DateTimeParseException;

@RestControllerAdvice
public class CustomExceptionHandler {
    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(value = ErrorMessageException.class)
    public ResponseEntity<?> handlerNotFound(ErrorMessageException ex, HttpServletRequest request,
                                             HttpServletResponse response, final WebRequest webRequest) {

        switch (ex.getTypeError()) {
            case NotFound:
                Logger.logInfo.info(ex.getMessage());
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponeErrorMessage(HttpStatus.NOT_FOUND.value(), ex.getMessage(), request.getServletPath()));
            case InternalServerError:
                String message = ex.getMessage();
                if (message == null || message.equals(""))
                    message = messageSource.getMessage("InternalServerError", null, null);
                Logger.logWarn.warn(message);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponeErrorMessage(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(), message, request.getServletPath()));
            case BadRequest:
                Logger.logWarn.warn(ex.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponeErrorMessage(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), request.getServletPath()));
            case NoContent:
                Logger.logWarn.warn(ex.getMessage());
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body(
                        new ResponeErrorMessage(HttpStatus.NO_CONTENT.value(), ex.getMessage(), request.getServletPath()));
            case AlreadyExists:
                Logger.logWarn.warn(ex.getMessage());
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                        .body(new ResponeErrorMessage(409, ex.getMessage(), request.getServletPath()));
            case Forbidden:
                Logger.logWarn.warn(ex.getMessage());
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .body(new ResponeErrorMessage(HttpStatus.FORBIDDEN.value(),
                                this.messageSource.getMessage("Forbidden", null, null), request.getServletPath()));
            default:
                return null;
        }
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> handleException(Exception exception, HttpServletRequest request,
                                             HttpServletResponse response) {
        ResponeErrorMessage responeErrorMessage = new ResponeErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                exception.getMessage(), request.getServletPath());
        Logger.logWarn.warn(exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responeErrorMessage);
    }

    @ExceptionHandler(value = DateTimeParseException.class)
    public ResponseEntity<?> handleException(DateTimeParseException exception, HttpServletRequest request,
                                             HttpServletResponse response) {
        ResponeErrorMessage responeErrorMessage = new ResponeErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                messageSource.getMessage("DateTimeFormatv1", null, null), request.getServletPath());
        Logger.logWarn.warn(exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responeErrorMessage);
    }

    @ExceptionHandler(value = DataNotFoundException.class)
    public ResponseEntity<?> handleDataNotFoudException(DataNotFoundException dataNotFoundException, HttpServletRequest request,
                                                        HttpServletResponse response) {
        ResultJson resultJson = new ResultJson(HttpStatus.NOT_FOUND.value(), "Data not found", new ResponeErrorMessage(
                dataNotFoundException.getMessage()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resultJson);
    }


    @ExceptionHandler(value = InternalServerException.class)
    public ResponseEntity<?> handleInternalServerException(InternalServerException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).
                body(new ResultJson(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal Server Code", new ResponeErrorMessage(ex.getMessage())));
    }
}
