package com.edu.openstudy.exception;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String object, String field, String value) {
        super(String.format("%s not found with %s=%s.", object, field, value));
    }

    public DataNotFoundException() {
        super("Data not found");
    }

    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
