package com.edu.openstudy.exception;

//import com.edu.openstudy.common.AuthorizationCommon;

public class ForbbidenException extends RuntimeException {
    public ForbbidenException(String message) {
        super(message);
    }

//    public ForbbidenException() {
//        super(AuthorizationCommon.MESSAGE_ACCESS_DENIED);
//    }

    public ForbbidenException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
