package com.edu.openstudy.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponeErrorMessage implements Serializable {

	private Integer httpCode;
	private String message;
	private String timestamp;
	private String path;
	private String description;
	public ResponeErrorMessage()
	{
		
	}
	public ResponeErrorMessage(int code, String message)
	{
		this.httpCode =  code;
		this.message =  message;
		this.timestamp = LocalDateTime.now().toString();
	}
	public ResponeErrorMessage(Integer code, String message, String path)
	{
		this.httpCode =  code;
		this.message =  message;
		this.path = path;
		this.timestamp = LocalDateTime.now().toString();
	}
	public ResponeErrorMessage(int code, String message, String path, String description)
	{
		this.httpCode =  code;
		this.message =  message;
		this.path = path;
		this.description = description;
		this.timestamp = LocalDateTime.now().toString();
	}

	public ResponeErrorMessage(String message)
	{
		this.message =  message;
	}

}
