package com.edu.openstudy.service;

import java.util.List;

import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.entity.ChapterEntity;

public interface ChapterService {
	ChapterDTO findById(long id);

	List<ChapterDTO> getAllByGroup(String groupChapter);

	List<ChapterDTO> getByParentCode(String code);
	
	List<String> getCodeByParentCode(String code);

	void checkChapterIsParent(ChapterEntity chapter);

	ChapterDTO save(ChapterDTO chapter);

	void checkCodeChapter(String code);

	ChapterEntity getById(Long id);

	List<String> getCodeChapterByLessonId(Long lessonId);

	String getCodeById(List<ChapterDTO> chapterDTOs, ChapterEntity chapter);

	List<ChapterDTO> getByLessonId(Long id);

	List<ChapterDTO> getAll();

	ChapterDTO update(Long id, ChapterDTO chapterDTO);

	boolean checkPriorityNumber(int numberPriority);
}
