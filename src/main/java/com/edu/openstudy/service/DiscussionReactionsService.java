package com.edu.openstudy.service;

import java.util.List;

import com.edu.openstudy.data.dto.DiscussionReactionsCreateDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsViewDTO;

public interface DiscussionReactionsService {

	DiscussionReactionsDTO findById(Long id);

	List<DiscussionReactionsViewDTO> findAllByDisscusionId(long id);
	
	boolean existsById(Long id);

	void save(DiscussionReactionsCreateDTO discussionLikeDTO);
		
	void update(DiscussionReactionsCreateDTO discussionLikeDTO, long id);
	
	void delete(long discussionId,Long userId);

	void deleteByDiscussionId(Long discussionId);

}
