package com.edu.openstudy.service;

import com.edu.openstudy.data.dto.DiscussionCreateDTO;
import com.edu.openstudy.data.dto.DiscussionDTO;
import com.edu.openstudy.data.dto.PaginationDTO;

public interface DiscussionService {

	PaginationDTO findAllDiscussionByLessonId(long lessonId,int page,int limit);
	
	PaginationDTO findAllDiscussionIncludeAnswerByLessonId(long lessonId,Long userId,int page,int limit);
	
	DiscussionDTO findById(long id);
	
	boolean existsById(Long id);

	void save(DiscussionCreateDTO discussionDTO);
		
	void update(DiscussionCreateDTO discussionDTO, long id);
	
	void delete(long id);

	void deleteByLesson(Long lessonId);
}
