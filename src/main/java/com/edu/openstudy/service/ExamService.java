package com.edu.openstudy.service;

import java.time.LocalDateTime;
import java.util.List;

import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExamHistoryDTO;
import com.edu.openstudy.data.dto.ExamViewDTO;
import com.edu.openstudy.data.dto.ExamViewDetailDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.LessonEntity;

public interface ExamService {

	ExamEntity updateByLesson(long idLesson, ExamEntity dto);

	ExamEntity findByLessonId(long id);

	ExamEntity findById(long id);
	
	ExamDTO getById(long id);

	void deleteByLessonId(Long lessonId);
	
	ExamDTO getExamById (long id);
	
	ExamViewDetailDTO getExamByIdAndUserIdAndCreateDate (long id,LocalDateTime createdDate);
	
	List<ExamViewDTO> getAllExam();
	
	List<ExamHistoryDTO> getAllExamPracticed(long userId);
		
	ExamEntity save(LessonEntity lesson, ExamEntity questionGroup);

	PaginationDTO findAllPaging(int no, int limit);
}
