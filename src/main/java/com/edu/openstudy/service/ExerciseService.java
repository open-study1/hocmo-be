package com.edu.openstudy.service;

import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.ExerciseEntity;
import com.edu.openstudy.data.entity.LessonEntity;

public interface ExerciseService {
	ExerciseEntity updateByLesson(long idLesson, ExamEntity dto);

	ExerciseEntity findByLessonId(long id);

	ExerciseEntity findById(long id);
	
	ExamDTO getById(long id);

	void deleteByLessonId(Long lessonId);

	ExerciseDTO getExerciseById (long id);
	
	ExerciseDTO getExerciseByLessonId (long lessonId);
		
	ExerciseEntity save(LessonEntity lesson, ExerciseEntity exercise);
	
	PaginationDTO findAllPaging(int no, int limit);

}
