package com.edu.openstudy.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.edu.openstudy.data.dto.LessonAudioCreateDTO;
import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.LessonMediaDTO;
import com.edu.openstudy.data.entity.LessonEntity;

public interface LessonMediaService {
	LessonMediaDTO save(LessonMediaDTO lessonMediaDTO, LessonEntity lessonSaved);

	void save(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lessonSaved);

	void uploadFile(LessonCreateDTO lessonAudioDTO, MultipartFile image);

	void deleteByLesson(Long lessonId);

	void update(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lesson);

}
