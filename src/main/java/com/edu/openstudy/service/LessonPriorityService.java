package com.edu.openstudy.service;

import com.edu.openstudy.data.dto.LessonPriorityDTO;

public interface LessonPriorityService {

	LessonPriorityDTO creation(LessonPriorityDTO lessonPriorityDTO);

	void delete(Long lessonId);

}
