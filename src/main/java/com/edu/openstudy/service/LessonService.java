package com.edu.openstudy.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.edu.openstudy.data.dto.LessonAudioCreateDTO;
import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.LessonDetailViewDTO;
import com.edu.openstudy.data.dto.LessonViewDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.request.RequestLessonDTO;

public interface LessonService {

	long count();


	LessonEntity findById(Long id);

	LessonDetailViewDTO getDetailById(Long id);
	
	LessonDetailViewDTO getDetailByChapterAndZOder(Long zOder,Long chapterId);

	boolean isExists(Long id);

	PaginationDTO findByRequestLessonDTO(RequestLessonDTO requestDTO);
	

	void update(long id, LessonCreateDTO dto);

	LessonEntity findByZorderAndChaspter(Long zOrder, Long chapterId);

	void active(Long id);

	void deleteById(Long id);

	Long getByQuestion(Long questionId);

	String getContentByQuestionId(Long questionId);

	void disableById(Long id);

	LessonCreateDTO readJson(String lesson, MultipartFile image);

	LessonCreateDTO save(LessonCreateDTO lesson);

	void deleteByIdAndCode(Long id, String code);

	void deleteByListId(List<Long> ids);

	LessonViewDTO update(LessonCreateDTO lesson, Long id);

	void setStatusListIdLesson(List<Long> listIdLesson, boolean status);

	void resetAllZorder();
}
