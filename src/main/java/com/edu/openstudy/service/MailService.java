package com.edu.openstudy.service;


import java.io.IOException;

import jakarta.mail.MessagingException;
public interface MailService {
	void sendEmail(String from, String to, String subject, String text, String category) throws IOException;
	void sendEmail(String to, String subject, String body);
	void sendHtmlEmail() throws MessagingException ;
}
