package com.edu.openstudy.service;

import java.time.LocalDateTime;
import java.util.List;

import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.entity.QuestionEntity;

public interface QuestionOptionService {

	void save(QuestionOptionDTO optionDto, QuestionEntity questionSaved);

	void update(QuestionOptionDTO optionDto, QuestionEntity questionSaved, LocalDateTime createDate, Long createDateBy);

	void update(List<QuestionOptionDTO> optionDtos, QuestionEntity questionSaved, LocalDateTime createDate, Long createDateBy);

	void deleteByQuestionId(Long questionId);

}
