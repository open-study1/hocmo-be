package com.edu.openstudy.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.QuestionExerciseDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDoExamDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserViewDTO;
import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;

public interface QuestionResponseUserService {

	QuestionExerciseDTO save(QuestionResponseUserDTO dto);

	List<QuestionResponseUserEntity> saveList(List<QuestionResponseUserDTO> questionResponseUserDTOs, LocalDateTime createDate);
	
	List<QuestionResponseUserEntity> saveListV2(long examId, List<QuestionResponseUserDoExamDTO> questionResponseUserDoExamDTOs, LocalDateTime createDate);


	QuestionResponseUserDTO findByQuestionIdAndExamId(Long id, LocalDateTime createDate);
	
	QuestionResponseUserDTO findByQuestionIdAndExerciseId(long questionId, long examId);
	
	Boolean existsByQuestionIdAndExerciseId(long questionId, long exerciseId);

	void deleteByQuestionId(Long id);

	QuestionResponseUserDTO update(QuestionResponseUserDTO dto);

}
