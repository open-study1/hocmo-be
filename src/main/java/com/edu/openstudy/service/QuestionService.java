package com.edu.openstudy.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.QuestionTypeEntity;

public interface QuestionService {

	void deleteById(Long id);

	long count();

	<S extends QuestionDTO> Page<S> findAll(Example<S> example, Pageable pageable);

	boolean existsById(Long id);

	QuestionDTO findById(Long id);

	QuestionDTO getById(Long id);
	
	List<QuestionDTO> findAll();
	
    PaginationDTO findAllPaging(int no, int limit);


	List<QuestionDTO> findQuestionByExamId(long id);


	void update(QuestionDTO dto, long id);

	void save(QuestionDTO dto);
	
	QuestionDTO create(QuestionDTO dto);

	void setQuestionSolutionInQuestionDTO(QuestionDTO questionDTO);

	QuestionDTO update(QuestionDTO dto, ExamEntity exam, LocalDateTime createDate, Long createDateBy);

	QuestionDTO getByExamAndOrder(long examId, int order);
}
