package com.edu.openstudy.service;

import java.time.LocalDateTime;
import java.util.List;

import com.edu.openstudy.data.dto.QuestionSolutionDTO;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionSolutionEntity;

public interface QuestionSolutionService {

	void save(QuestionSolutionDTO questionSolutionDTO, QuestionEntity questionSaved);

	void update(QuestionSolutionDTO questionSolutionDTO, QuestionEntity questionSaved, LocalDateTime creatDateTime, Long createDateBy);

	QuestionSolutionEntity findById(Long id);

	List<QuestionSolutionDTO> findByQuestion(Long id);

	List<QuestionSolutionDTO> findByQuestionDTO(Long id);

	void update(List<QuestionSolutionDTO> questionSolutionDTOs, QuestionEntity questionSaved, LocalDateTime creatDateTime, Long createDateBy);

	int countByLesson(Long id);

	void deleteByQuestion(Long questionId);
	
	
}
