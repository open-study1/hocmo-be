package com.edu.openstudy.service;

import java.util.List;

import com.edu.openstudy.data.dto.QuestionTypeDTO;
import com.edu.openstudy.data.entity.QuestionTypeEntity;

public interface QuestionTypeService {

	QuestionTypeEntity findById(Long id);

	List<QuestionTypeDTO> findAll();

}
