package com.edu.openstudy.service;

import com.edu.openstudy.data.dto.RoleDTO;

public interface RoleService {
	RoleDTO getAll();
	RoleDTO getRoleByUserId(long userId);
}
