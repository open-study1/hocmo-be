package com.edu.openstudy.service;

import java.time.LocalDateTime;
import java.util.List;

import com.edu.openstudy.data.dto.QuestionResponseUserViewDTO;
import com.edu.openstudy.data.dto.StatisticsDTO;
import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.dto.UserScoreViewDTO;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import com.edu.openstudy.data.entity.UserScoreEntity;

public interface UserScoreService {


    UserScoreDTO save(UserScoreDTO userScoreDTO);

    UserScoreDTO doScore(long ExamId, LocalDateTime createTime,List<QuestionResponseUserEntity> responseUserEntities,long duration);

	void deleteByCreate(String dateTime);


	void deleteByQuestionId(long questionId);


	List<UserScoreEntity> findBySolutionId(long id);
	
	List<UserScoreViewDTO> findAllByCreatedBy(long userId);
	
	UserScoreDTO findById(long id);
	
	List<StatisticsDTO> statisticsChapter(long userId);

}
