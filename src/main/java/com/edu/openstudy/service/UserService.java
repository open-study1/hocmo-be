package com.edu.openstudy.service;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.UserDTO;
import com.edu.openstudy.data.entity.UserEntity;

public interface UserService extends UserDetailsService {
    boolean existsById(long id);

    List<UserDTO> findAll();

    UserDTO save(UserEntity user);
    
    UserDTO create(UserDTO user);

    UserDTO findById(long userId);
    
    List<UserDTO> findAll(Sort sort);

    PaginationDTO findAllPaging(int no, int limit);

//	UserDTO save(UserCreationDTO dto);
//  void changePassword(ChangePassDTO dto);

    UserDTO update(UserDTO user);

    boolean checkIfValidOldPassword(String oldPass, String newPass);

    List<Object[]> statisticsByRole();

    boolean existsByEmail(String email);

    String resetPassword(String email);

    String encodePass(String pass);

    UserDTO findByEmail(String email);
    
    UserDTO handlerValid(UserDTO userDTO);
    
    boolean verifiedEmail(long userId, String optCode);
    
    void sendOTPEmail(long userId);
    
    void delete(long userId);
}
