package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.edu.openstudy.service.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.edu.openstudy.common.Logger;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.ChapterEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.data.repository.ChapterRepository;
import com.edu.openstudy.service.ChapterService;

@Service
public class ChapterServiceImpl implements ChapterService {
	@Autowired
	private ChapterRepository chapterRepository;
	@Autowired
	private ChapterMapper chapterMapper;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LessonService lessonService;
	@Override
	public ChapterDTO findById(long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<ChapterDTO> getAllByGroup(String groupChapter) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void checkChapterIsParent(ChapterEntity chapter) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void checkCodeChapter(String code) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public ChapterEntity getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<String> getCodeChapterByLessonId(Long lessonId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getCodeById(List<ChapterDTO> chapterDTOs, ChapterEntity chapter) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<ChapterDTO> getByLessonId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ChapterDTO update(Long id, ChapterDTO chapterDTO) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean checkPriorityNumber(int numberPriority) {
		// TODO Auto-generated method stub
		return false;
	}

//	@Override
//	public ChapterDTO findById(long id) {
//		ChapterEntity chapter = this.chapterRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
//				String.format(messageSource.getMessage("NotFound", null, null), "Chapter", "Id", String.valueOf(id)),
//				TypeError.NotFound));
//		return chapterMapper.mapToDTO(chapter);
//	}
//
//	@Override
//	public List<ChapterDTO> getAllByGroup(String groupChapter) {
//		if (groupChapter == null || groupChapter.equals(""))
//			return getAll();
//		List<ChapterDTO> chapterDTOs = chapterRepository.findAllByGroupChapter(groupChapter).stream()
//				.map(item -> this.chapterMapper.mapToDTO(item)).collect(Collectors.toList());
//		Logger.logInfo.info("Get all {}", "Chapter");
//		return chapterDTOs;
//	}
//
	@Override
	public List<ChapterDTO> getAll() {
		List<ChapterDTO> chapterDTOs = chapterRepository.findAll().stream()
				.map(item -> this.chapterMapper.mapToDTO(item)).collect(Collectors.toList());
		return chapterDTOs;
	}
//
//	@Override
//	public void checkChapterIsParent(ChapterEntity chapter) {
//		if (chapter.getParentCode() == null)
//			throw new ErrorMessageException(
//					String.format(messageSource.getMessage("ChooseAnyChapter", null, null), chapter.getName()),
//					TypeError.BadRequest);
//	}
//
	@Override
	public List<ChapterDTO> getByParentCode(String parentCode) {
		List<ChapterDTO> chapterDTOs = chapterRepository.findAllByParentCode(parentCode).stream()
				.map(item -> this.chapterMapper.mapToDTO(item)).collect(Collectors.toList());
		Logger.logInfo.info("Get chapter parentId {}", "Chapter");
		return chapterDTOs;
	}

	@Override
	public List<String> getCodeByParentCode(String code) {
		List<String> codes = chapterRepository.findChapterCodeByParentCode(code);
		Logger.logInfo.info("Get chapterCode by parentCode {}", "Chapter");
		return codes;
	}
//
//	@Override
//	public List<ChapterDTO> getByLessonId(Long id) {
//		boolean existsLesson = this.lessonService.existsById(id);
//		if (!existsLesson)
//			throw new ErrorMessageException(
//					String.format(messageSource.getMessage("NotFound", null, null), "Lesson", "id", String.valueOf(id)),
//					TypeError.NotFound);
//		List<ChapterEntity> categories = this.chapterRepository.getByLesson(id);
//		if (categories != null && categories.size() == 0 || categories == null) {
//			throw new ErrorMessageException(
//					String.format(messageSource.getMessage("NotFound", null, null), "Lesson", "id", String.valueOf(id)),
//					TypeError.NotFound);
//		}
//
//		return categories.stream().map(chapter -> chapterMapper.mapToDTO(chapter)).collect(Collectors.toList());
//	}
//
//	@Override
//	public void checkCodeChapter(String code) {
//		boolean isExists = this.chapterRepository.existsByCode(code);
//		if (!isExists)
//			throw new ErrorMessageException(
//					String.format(messageSource.getMessage("NotFound", null, null), "Chapter", " code", code),
//					TypeError.NotFound);
//	}
//
//	@Override
//	public ChapterEntity findByCode(String code) {
//		return this.chapterRepository.findByCode(code)
//				.orElseThrow(() -> new ErrorMessageException(
//						String.format(messageSource.getMessage("NotFound", null, null), "Chapter", " code", code),
//						TypeError.NotFound));
//
//	}
//
//	@Override
//	public ChapterEntity getById(Long id) {
//		return this.chapterRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
//				String.format(messageSource.getMessage("NotFound", null, null), "Chapter", "Id", String.valueOf(id)),
//				TypeError.NotFound));
//	}
//
//	@Override
//	public List<String> getCodeChapterByLessonId(Long lessonId) {
//		List<ChapterDTO> chapterDTO = getByLessonId(lessonId);
//		List<String> codes = new ArrayList<String>();
//		chapterDTO.forEach(item -> {
//			codes.add(item.getCode());
//		});
//		return codes;
//	}
//
//	@Override
//	public String getCodeById(List<ChapterDTO> chapterDTOs, ChapterEntity chapter) {
//		int firstIndex = 0;
//		ChapterDTO chapterDTO = chapterDTOs.get(firstIndex);
//		chapter = getById(chapterDTO.getId());
//		return chapter.getCode();
//	}
//
	@Override
	public ChapterDTO save(ChapterDTO chapterDTO) {
		// valid
		ChapterEntity chapter = this.chapterMapper.map(chapterDTO);
		chapter.setCreatedBy(UserContext.getId());
		chapter.setCreatedDate(LocalDateTime.now());
		chapter.setModifiedBy(UserContext.getId());
		chapter.setModifiedDate(LocalDateTime.now());
		ChapterEntity chapterSaved = this.chapterRepository.save(chapter);
		return this.chapterMapper.mapToDTO(chapterSaved);
	}
//
//	@Override
//	public ChapterDTO update(Long id, ChapterDTO chapterDTO) {
//		ChapterEntity chapter = this.chapterRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
//				String.format(messageSource.getMessage("NotFound", null, null), "Chapter", "Id", String.valueOf(id)),
//				TypeError.NotFound));
//		this.chapterMapper.mapToUpdate(chapterDTO, chapter);
//		ChapterEntity chapterUpdated = this.chapterRepository.save(chapter);
//		return this.chapterMapper.mapToDTO(chapterUpdated);
//	}
//
//	@Override
//	public boolean checkPriorityNumber(int numberPriority) {
//		List<ChapterEntity> categories = this.chapterRepository.findAll();
//		boolean isExistsPriorityNumber = false;
//		try {
//			String numberPriorityAsString = String.valueOf(numberPriority);
//			for (ChapterEntity chapter : categories) {
//				String code = chapter.getCode();
//				if (code.equalsIgnoreCase(numberPriorityAsString))
//					isExistsPriorityNumber = true;
//			}
//		} catch (Exception e) {
//			throw new ErrorMessageException(e.getMessage(), TypeError.InternalServerError);
//		}
//		return isExistsPriorityNumber;
//	}
//
	public void deleteById(Long id)
	{
		ChapterEntity chapter = this.getById(id);
		this.chapterRepository.deleteById(chapter.getId());
	}

}
