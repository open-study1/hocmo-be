package com.edu.openstudy.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.Logger;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.DiscussionDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsCreateDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsDTO;
import com.edu.openstudy.data.dto.DiscussionReactionsViewDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.DiscussionReactionsEntity;
import com.edu.openstudy.data.mapper.DiscussionReactionsMapper;
import com.edu.openstudy.data.repository.DiscussionReactionsRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.DiscussionReactionsService;
import com.edu.openstudy.service.DiscussionService;

@Transactional
@Service
public class DiscussionReactionsServiceImpl implements DiscussionReactionsService {

	@Autowired
	private DiscussionReactionsRepository discussionRactionsRepository;
	@Autowired
	private DiscussionReactionsMapper discussionReactionsMapper;
	@Autowired
	private DiscussionService discussionService;
	@Autowired
	private MessageSource messageSource;

	@Override
	public DiscussionReactionsDTO findById(Long id) {
		
		Optional<DiscussionReactionsEntity> entity = this.discussionRactionsRepository.findById(id);
		if (entity.isPresent()) {
			Logger.logInfo.info("Get discussion_reaction by id {}", "DiscussionReactions");
			return discussionReactionsMapper.map(entity.get());
		} else
			return discussionReactionsMapper.map(entity.get());
	}

	@Override
	public List<DiscussionReactionsViewDTO> findAllByDisscusionId(long id) {
		List<DiscussionReactionsViewDTO> discussionReactionsViewDTOs = discussionRactionsRepository
				.findAllByDiscussionId(id).stream().map(item -> this.discussionReactionsMapper.mapToView(item))
				.collect(Collectors.toList());
		if (discussionReactionsViewDTOs.size() < 1) {
			throw new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
					"DiscussionReactions", " Request", String.valueOf("")), TypeError.NotFound);
		}
		return discussionReactionsViewDTOs;
	}

	@Override
	public boolean existsById(Long id) {
		return discussionRactionsRepository.existsById(id);

	}

	@Override
	public void save(DiscussionReactionsCreateDTO discussionReactionsCreateDTO) {
		// Check exist
		if (discussionRactionsRepository.findByDiscussionIdAndCreatedBy(
				discussionReactionsCreateDTO.getDiscussion().getId(), UserContext.getId()).isEmpty()) {
			DiscussionReactionsEntity discussionReactions = new DiscussionReactionsEntity();
			DiscussionDTO discussionDTO = discussionService
					.findById(discussionReactionsCreateDTO.getDiscussion().getId());
			discussionReactionsCreateDTO.setDiscussion(discussionDTO);
			discussionReactions = discussionReactionsMapper.map(discussionReactionsCreateDTO);
			//set time and user
			discussionReactions = setDateTimeAndUser(discussionReactions);
			//save
			this.discussionRactionsRepository.save(discussionReactions);
			Logger.logInfo.info("Add discussion_reaction successful");
		} else {
			throw new ErrorMessageException(
					String.format(messageSource.getMessage("AlreadyExists", null, null), String.valueOf(
							"Reaction of discussion : " + discussionReactionsCreateDTO.getDiscussion().getId())),
					TypeError.AlreadyExists);
		}

	}

	@Override
	public void update(DiscussionReactionsCreateDTO discussionReactionsUpdateDTO, long id) {
		DiscussionReactionsDTO discussionReactionsDTO = findById(id);
		// check Permission
		if (UserContext.getId() != discussionReactionsDTO.getCreatedBy()) {
			throw new ErrorMessageException(String.format(messageSource.getMessage("Forbidden", null, null)),
					TypeError.Forbidden);
		}
		if (discussionReactionsUpdateDTO.getReactions() != null) {
			discussionReactionsDTO.setReactions(discussionReactionsUpdateDTO.getReactions());
		}
		// set UserId
		discussionReactionsDTO.setModifiedBy(UserContext.getId());
		// set Time
		discussionReactionsDTO.setModifiedDate(DateTime.getInstances());
		// Create entity
		DiscussionReactionsEntity discussionReactions = discussionReactionsMapper.map(discussionReactionsDTO);
		// save DiscussionReactions
		this.discussionRactionsRepository.save(discussionReactions);
		// logging
		Logger.logInfo.info("Update discussion_reaction by id =" + id + "", "DiscussionReactions");
	}

	@Override
	public void delete(long discussionId, Long userId) {
		DiscussionReactionsEntity discussionReactions = discussionRactionsRepository.findByDiscussionIdAndCreatedBy(discussionId, userId).get();
		// Check null
		if (discussionReactions != null) {
			discussionRactionsRepository.delete(discussionReactions);
			Logger.logInfo.info(
					"Delete discussion_reaction of discussion :" + discussionId + "by user id =" + userId + "",
					"DiscussionReactions");
		}
		// DiscussionReactions is null
		else {
			throw new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
					"DiscussionReactions", " Request", String.valueOf("")), TypeError.NotFound);
		}

	}

	@Override
	public void deleteByDiscussionId(Long discussionId) {
		// Get sub discussion of discussion
		List<DiscussionReactionsEntity> discussionReactions = discussionRactionsRepository
				.findAllByDiscussionId(discussionId);
		// Delete all sub discussion of discussion
		discussionReactions.forEach(item -> {
			this.discussionRactionsRepository.deleteById(item.getId());
		});
		Logger.logInfo.info("Delete DiscussionReactions by id: " + discussionId, "DiscussionReaction");
	}

	public DiscussionReactionsEntity setDateTimeAndUser(DiscussionReactionsEntity discussionReactions) {
		discussionReactions.setCreatedDate(DateTime.getInstances());
		discussionReactions.setCreatedBy(UserContext.getId());
		discussionReactions.setModifiedBy(UserContext.getId());
		discussionReactions.setModifiedDate(DateTime.getInstances());
		return discussionReactions;
	}

}
