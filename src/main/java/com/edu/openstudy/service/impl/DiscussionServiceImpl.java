package com.edu.openstudy.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.Logger;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.DiscussionCreateDTO;
import com.edu.openstudy.data.dto.DiscussionDTO;
import com.edu.openstudy.data.dto.DiscussionSubDTO;
import com.edu.openstudy.data.dto.DiscussionViewDTO;
import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.DiscussionEntity;
import com.edu.openstudy.data.mapper.DiscussionMapper;
import com.edu.openstudy.data.repository.DiscussionRepository;
import com.edu.openstudy.data.repository.LessonRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.DiscussionReactionsService;
import com.edu.openstudy.service.DiscussionService;
import com.edu.openstudy.service.UserScoreService;

@Service
public class DiscussionServiceImpl implements DiscussionService {

	@Autowired
	private DiscussionRepository discussionRepository;
	@Autowired
	private DiscussionMapper discussionMapper;
	@Autowired
	private LessonRepository lessonRepository;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private DiscussionReactionsService discussionReactionsService;
	@Autowired
	private UserScoreService userScoreService;

	@Override
	public PaginationDTO findAllDiscussionByLessonId(long lessonId, int page, int limit) {
		Pageable pageable = PageRequest.of(page, limit);
		List<DiscussionViewDTO> discussionsViewDTOs = this.discussionRepository
				.findAllByLessonIdAndParentIdOrderByCreatedDateDesc(lessonId, null, pageable).stream()
				.map(item -> this.discussionMapper.mapToViewDTO(item)).map(item -> this.setDiscussionSubs(item))// Set
																												// DiscussionSub
																												// for
																												// discussionsViewDTOs
																												// from
																												// discussionRepository
				.collect(Collectors.toList());
		Page<DiscussionEntity> pageDiscussion = this.discussionRepository
				.findAllByLessonIdAndParentIdOrderByCreatedDateDesc(lessonId, null, pageable);
		PaginationDTO pageDTO = new PaginationDTO(discussionsViewDTOs, pageDiscussion.isFirst(),
				pageDiscussion.isLast(), pageDiscussion.getTotalPages(), pageDiscussion.getTotalElements(),
				pageDiscussion.getSize(), pageDiscussion.getNumber());
		Logger.logInfo.info("Get all discussion by lesson id =" + lessonId + "", "Discussion");
		return pageDTO;
	}

	public DiscussionViewDTO setDiscussionSubs(DiscussionViewDTO discussionViewDTO) {
		Set<DiscussionSubDTO> discussionSubs = new HashSet<>();
		for (DiscussionEntity discussionSub : discussionRepository.findAllByParentId(discussionViewDTO.getId())) {
			discussionSubs.add(discussionMapper.mapToSubDTO(discussionSub));
		}
		discussionViewDTO.setDiscussionSubs(discussionSubs);
		return discussionViewDTO;
	}

//	@Override
//	public PaginationDTO findAllDiscussionIncludeAnswerByLessonId(long lessonId, Long userId, int page, int limit) {
//		List<ScoreResponseDTO> discussionIncludeAnswerDTOs = userScoreService.getByLessoAndUser(lessonId, userId);
//		List<ScoreResponseDTO> discussionIncludeAnswerViewDTOs = new ArrayList<>();
//		// Get for Board
//		if (userId == null) {
//			for (int i = 0; i < discussionIncludeAnswerDTOs.size(); i++) {
//				if (discussionIncludeAnswerDTOs.get(i).getScore() != null) {
//					discussionIncludeAnswerViewDTOs.add(discussionIncludeAnswerDTOs.get(i));
//				}
//			}
//		}
//		// Get for Me
//		else {
//			for (int i = 0; i < discussionIncludeAnswerDTOs.size(); i++) {
//				discussionIncludeAnswerViewDTOs.add(discussionIncludeAnswerDTOs.get(i));
//			}
//		}
//		for (ScoreResponseDTO responseDTO : discussionIncludeAnswerDTOs) {
//			DiscussionEntity discussion = discussionRepository
//					.findByCreatedDateAndCreatedByAndLessonIdAndIsIncludeAnswer(responseDTO.getCreateDate(),
//							responseDTO.getUserId(), lessonId, true);
//			if (discussion != null) {
//				responseDTO.setId(discussion.getId());
//				Set<DiscussionSubDTO> discussionSubs = new HashSet<>();
//				for (DiscussionEntity discussionSub : discussionRepository.findAllByParentId(discussion.getId())) {
//					discussionSubs.add(discussionMapper.mapToSubDTO(discussionSub));
//				}
//				responseDTO.setDiscussionSubs(discussionSubs);
//			}
//		}
//		PaginationDTO pageDTO = new PaginationDTO().customPagination(discussionIncludeAnswerViewDTOs,
//				discussionIncludeAnswerViewDTOs.size(), page, limit);
//		Logger.logInfo.info("Get all discussion include answer by lesson id =" + lessonId + "", "Discussion");
//		return pageDTO;
//	}

	@Override
	public boolean existsById(Long id) {
		return discussionRepository.existsById(id);
	}

	@Override
	public void save(DiscussionCreateDTO discussionDTO) {
		DiscussionEntity discussion = new DiscussionEntity();
		try {
			if (discussionDTO.getIsIncludeAnswer() == null) {
				discussionDTO.setIsIncludeAnswer(false);
			}
			if (discussionDTO.getCreatedDate() == null) {
				discussionDTO.setCreatedDate(DateTime.getInstances());
			}
			if (discussionDTO.getModifiedDate() == null) {
				discussionDTO.setModifiedDate(DateTime.getInstances());
			}
			LessonCreateDTO lessonCreateDTO = discussionDTO.getLesson();
			lessonRepository.findById(lessonCreateDTO.getId()).orElseThrow(
					() -> new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
							"Lesson", "Id", String.valueOf(lessonCreateDTO.getId())), TypeError.NotFound));
			discussionDTO.setLesson(lessonCreateDTO);
			discussion = discussionMapper.map(discussionDTO);
			discussion.setCreatedBy(UserContext.getId());
			discussion.setModifiedBy(UserContext.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.discussionRepository.save(discussion);
		Logger.logInfo.info("Add discussion with id =" + discussion.getId() + "", "Discussion");

	}

	@Override
	public void update(DiscussionCreateDTO discussionUpdateDTO, long id) {
		try {
			DiscussionDTO discussionDTO = findById(id);
			// check permission
			if (UserContext.getId() != discussionDTO.getCreatedBy()) {
				throw new ErrorMessageException(String.format(messageSource.getMessage("Forbidden", null, null)),
						TypeError.Forbidden);
			}
			if (discussionUpdateDTO.getDiscussionContent() != null) {
				discussionDTO.setDiscussionContent(discussionUpdateDTO.getDiscussionContent());
			}
			discussionDTO.setModifiedDate(DateTime.getInstances());
			DiscussionEntity discussion = discussionMapper.map(discussionDTO);
			discussion.setModifiedBy(UserContext.getId());
			this.discussionRepository.save(discussion);
			Logger.logInfo.info("Update discussion by id =" + id + "", "Discussion");

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void delete(long id) {
		// check exist
		if (existsById(id) == false) {
			throw new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
					"Discussion", "Id", String.valueOf(id)), TypeError.NotFound);
		} else {
			DiscussionDTO discussionDTO = findById(id);
			// check permission
			if (UserContext.getId() != discussionDTO.getCreatedBy()) {
				throw new ErrorMessageException(String.format(messageSource.getMessage("Forbidden", null, null)),
						TypeError.Forbidden);
			}
			List<DiscussionEntity> discussions = discussionRepository.findAllByParentId(id);
			for (DiscussionEntity discussion : discussions) {
				discussionRepository.deleteById(discussion.getId());
			}
			discussionRepository.deleteById(id);
		}
	}

	@Override
	public DiscussionDTO findById(long id) {
		DiscussionEntity discussion = discussionRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
				String.format(messageSource.getMessage("NotFound", null, null), "Discussion", "Id", String.valueOf(id)),
				TypeError.NotFound));
		Logger.logInfo.info("Get lesson by id {}", "Lesson");
		return discussionMapper.map(discussion);
	}

	@Override
	public void deleteByLesson(Long lessonId) {
		// delete discussion reaction
		List<DiscussionEntity> discussions = this.discussionRepository.findByLessonId(lessonId);
		discussions.forEach(discussion -> {
			this.discussionReactionsService.deleteByDiscussionId(discussion.getId());
			this.discussionRepository.deleteById(discussion.getId());
		});
		Logger.logInfo.info("Delete discussion by lesson id:" + lessonId, "Discussion");
	}

	@Override
	public PaginationDTO findAllDiscussionIncludeAnswerByLessonId(long lessonId, Long userId, int page, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

}
