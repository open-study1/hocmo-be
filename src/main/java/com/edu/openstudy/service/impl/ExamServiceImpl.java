package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExamHistoryDTO;
import com.edu.openstudy.data.dto.ExamViewDTO;
import com.edu.openstudy.data.dto.ExamViewDetailDTO;
import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.dto.UserScoreViewDTO;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.UserScoreEntity;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.UserScoreMapper;
import com.edu.openstudy.data.repository.ExamRepository;
import com.edu.openstudy.data.repository.UserScoreRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.ExamService;
import com.edu.openstudy.service.QuestionService;

@Service
public class ExamServiceImpl implements ExamService {
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExamMapper examMapper;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private UserScoreRepository userScoreRepository;

	@Autowired
	private UserScoreMapper userScoreMapper;

	@Override
	public ExamEntity updateByLesson(long idLesson, ExamEntity dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteByLessonId(Long lessonId) {
		// TODO Auto-generated method stub

	}

	@Override
	public ExamEntity save(LessonEntity lesson, ExamEntity questionGroup) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public QuestionGroupEntity updateByLesson(long idLesson, QuestionGroupEntity dto) {
//		QuestionGroupEntity questionGroup = findByLessonId(idLesson);
//		//set properties
//		questionGroup.setModifiedDate(DateTime.getInstances());
//		questionGroup.setModifiedBy(UserContext.getId());
//		questionGroup.setDescription(dto.getDescription());
//		questionGroup.setTitle(dto.getTitle());
//		questionGroup.setIsEmbedded(dto.getIsEmbedded());
//		questionGroup.setIsShuffle(dto.getIsShuffle());
//		questionGroup.setMediaSegmentFrom(dto.getMediaSegmentFrom());
//		return this.questionGroupRepository.save(questionGroup);
//	}
//	@Override
//	public QuestionGroupEntity save(LessonEntity lesson, QuestionGroupEntity questionGroup)
//	{
//		//set datetime
//		questionGroup.setCreatedBy(UserContext.getId());
//		questionGroup.setModifiedBy(UserContext.getId());
//		questionGroup.setCreatedDate(DateTime.getInstances());
//		questionGroup.setModifiedDate(DateTime.getInstances());
//		questionGroup.setLesson(lesson);
//		return this.questionGroupRepository.save(questionGroup);
//		
//	}
//
//

	@Override
	public ExamDTO getById(long id) {
		ExamDTO questionGroupDTO = examMapper.mapToDTO(findById(id));
		return questionGroupDTO;
	}

	@Override
	public ExamViewDetailDTO getExamByIdAndUserIdAndCreateDate(long id, LocalDateTime createdDate) {
		ExamViewDetailDTO examViewDTO = examMapper.mapToViewDetailDTO(findById(id), UserContext.getId(), createdDate);
		return examViewDTO;
	}

	@Override
	public ExamDTO getExamById(long id) {
		ExamDTO examDTO = examMapper.mapToDTO(findById(id));
		return examDTO;
	}

	@Override
	public ExamEntity findById(long id) {
		ExamEntity entity = this.examRepository.findById(id).orElseThrow(
				() -> new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
						"QuestionGroup", "Id", String.valueOf(id)), TypeError.NotFound));
		return entity;
	}

//	@Override
//	public void deleteByLessonId(Long lessonId)
//	{
//		//delete Question
//		questionService.deleteByQuestionGroupId(lessonId);
//	}
	@Override
	public ExamEntity findByLessonId(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ExamViewDTO> getAllExam() {

		return examRepository.findAll().stream().map(item -> this.examMapper.mapToViewDTO(item))
				.collect(Collectors.toList());
	}

	@Override
	public List<ExamHistoryDTO> getAllExamPracticed(long userId) {
		List<ExamHistoryDTO> historyDTOs = examRepository.findExamPracticedByUser(userId).stream()
				.map(item -> this.examMapper.mapToHistoryDTO(item)).collect(Collectors.toList());
		List<UserScoreEntity> userScores = userScoreRepository.findAllByCreatedByOrderByCreatedDateDesc(userId);

		for (ExamHistoryDTO historyDTO : historyDTOs) {
			List<UserScoreViewDTO> scoreViewDTOs = new ArrayList<>();
			for (UserScoreEntity userScore : userScores) {
				if (historyDTO.getId() == userScore.getExam().getId())
					scoreViewDTOs.add(userScoreMapper.mapToViewDTO(userScore));
			}
			historyDTO.setUserScoreViewDTOs(scoreViewDTOs);
		}
		return historyDTOs;
	}

	@Override
	public PaginationDTO findAllPaging(int no, int limit) {
		Pageable page = PageRequest.of(no, limit);// Page: 0 and Member: 10
		List<Object> listExam = this.examRepository.findAll(page).toList().stream()
				.map(item -> this.examMapper.mapToViewDTO(item)).collect(Collectors.toList());
		Page<ExamEntity> pageExam = this.examRepository.findAll(page);
		return new PaginationDTO(listExam, pageExam.isFirst(), pageExam.isLast(), pageExam.getTotalPages(),
				pageExam.getTotalElements(), pageExam.getSize(), pageExam.getNumber());
	}

}
