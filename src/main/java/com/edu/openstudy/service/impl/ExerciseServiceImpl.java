package com.edu.openstudy.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.edu.openstudy.data.dto.ExerciseDTO;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.ExerciseEntity;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.mapper.ExamMapper;
import com.edu.openstudy.data.mapper.ExerciseMapper;
import com.edu.openstudy.data.repository.ExamRepository;
import com.edu.openstudy.data.repository.ExerciseRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.ExamService;
import com.edu.openstudy.service.ExerciseService;
import com.edu.openstudy.service.QuestionService;

@Service
public class ExerciseServiceImpl implements ExerciseService {

	@Autowired
	private ExerciseRepository exerciseRepository;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExerciseMapper exerciseMapper ;
	@Autowired
	private QuestionService questionService;
	@Override
	public ExerciseEntity updateByLesson(long idLesson, ExamEntity dto) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ExerciseEntity findByLessonId(long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ExerciseEntity findById(long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ExamDTO getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void deleteByLessonId(Long lessonId) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public ExerciseDTO getExerciseById(long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ExerciseDTO getExerciseByLessonId(long lessonId) {
		ExerciseEntity entity = this.exerciseRepository.findByLessonId(lessonId).orElseThrow(() -> new ErrorMessageException(
				String.format(messageSource.getMessage("NotFound", null, null), "Exercise", "Id", String.valueOf(lessonId)),
				TypeError.NotFound));
		return  exerciseMapper.mapToDTO(entity, UserContext.getId());
	}
	@Override
	public ExerciseEntity save(LessonEntity lesson, ExerciseEntity exercise) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public PaginationDTO findAllPaging(int no, int limit) {
		Pageable page = PageRequest.of(no, limit);// Page: 0 and Member: 10
		List<Object> listExercise = this.exerciseRepository.findAll(page).toList().stream()
				.map(item -> this.exerciseMapper.mapViewToDTO(item)).collect(Collectors.toList());
		Page<ExerciseEntity> pageExercise = this.exerciseRepository.findAll(page);
		return new PaginationDTO(listExercise, pageExercise.isFirst(), pageExercise.isLast(), pageExercise.getTotalPages(),
				pageExercise.getTotalElements(), pageExercise.getSize(), pageExercise.getNumber());
	}
	
}
