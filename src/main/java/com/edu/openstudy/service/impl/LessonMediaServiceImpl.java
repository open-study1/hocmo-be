package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.File;
import com.edu.openstudy.data.dto.LessonAudioCreateDTO;
import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.dto.LessonMediaDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonMediaEntity;
import com.edu.openstudy.data.mapper.LessonMediaMapper;
import com.edu.openstudy.data.repository.LessonMediaRepository;
import com.edu.openstudy.service.ChapterService;
import com.edu.openstudy.service.LessonMediaService;
import com.edu.openstudy.common.util.FileHandle;

@Service
@Transactional
public class LessonMediaServiceImpl implements LessonMediaService {
	@Autowired
	private LessonMediaRepository lessonMediaRepository;
	@Autowired
	private LessonMediaMapper lessonMediaMapper;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private FileHandle fileHandle;
	private final String[] ATTRIBUTES_IMAGE = { "jpeg", "jpg", "png", "gif", "pdf", "psd", "tiff" };
	private final String[] ATTRIBUTES_AUDIO = { "mp3", "wma", "wav", "flac" };
	private final String[] ATTRIBUTES_VIDEO = { "avi", "mp4", "mkv", "wmv" };
	@Override
	public LessonMediaDTO save(LessonMediaDTO lessonMediaDTO, LessonEntity lessonSaved) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void save(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lessonSaved) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void uploadFile(LessonCreateDTO lessonAudioDTO, MultipartFile image) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void deleteByLesson(Long lessonId) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lesson) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public LessonMediaDTO save(LessonMediaDTO lessonMediaDTO, LessonEntity lessonSaved) {
//		if (lessonMediaDTO != null && !lessonMediaDTO.getLessonMediaLink().equals("")) {
//			LessonMediaEntity lessonMedia = lessonMediaMapper.mapToEntity(lessonMediaDTO);
//			setTime(lessonMedia);
//			lessonMedia.setLesson(lessonSaved);
//			setLinkMediaAndType(lessonMedia);
//			LessonMediaEntity saved = this.lessonMediaRepository.save(lessonMedia);
//			return lessonMediaMapper.mapToDTO(saved);
//		}
//		return null;
//
//	}
//
//	private LessonMediaEntity findById(Long id) {
//		return this.lessonMediaRepository.findById(id).get();
//	}
//
//	public void update(LessonMediaDTO dto, LessonEntity lessonSaved, LocalDateTime createDate, Long createBy) {
//		LessonMediaEntity lessonMedia = findById(dto.getId());
//		lessonMediaMapper.mapToUpdate(lessonMedia, dto, lessonSaved);
//		boolean isEqualURL = compareLinkURLMedia(lessonMedia, dto);
//		if (!isEqualURL) {
//			lessonMedia.setLessonMediaLink(dto.getLessonMediaLink());
//			setLinkMediaAndType(lessonMedia);
//		}
//		this.lessonMediaRepository.save(lessonMedia);
//	}
//
//	@Override
//	public void update(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lesson) {
//		Long lessonId = lesson.getId();
//		List<LessonMediaEntity> lessonMedias = this.lessonMediaRepository.findByLessonId(lessonId);
//		lessonMediaDTOs = getListDTO(lessonId, lessonMediaDTOs);// handle RA or DI
//		if (lessonMedias != null && lessonMedias.size() > 0) {
//			int firstIndex = 0;
//			LocalDateTime createTime = lessonMedias.get(firstIndex).getCreatedDate();
//			Long createBy = lessonMedias.get(firstIndex).getCreatedBy();
//			// update
//			handleUpdate(lessonMediaDTOs, lesson, createTime, createBy);
//			// delete not use
//			handleDeleteEntityNotUse(lessonMedias, lessonMediaDTOs);
//		}
//		// save new
//		handleSaveNew(lessonMediaDTOs, lesson);
//	}
//
//	private List<LessonMediaDTO> handleListDTO(List<LessonMediaDTO> lessonMediaDTOs, String type) {
//
//		if (lessonMediaDTOs != null && lessonMediaDTOs.size() > 0) {
//			int size = lessonMediaDTOs.size();
//			for (int i = 0; i < size; i++) {
//				LessonMediaDTO dto = lessonMediaDTOs.get(i);
//				if (dto.getId() != null && dto.getId() > 0 && dto.getType().equals(type)) {
//					lessonMediaDTOs.remove(i);
//					size--;
//				}
//			}
//		}
//		return lessonMediaDTOs;
//
//	}
//
//	private List<LessonMediaDTO> getListDTO(Long lessonId, List<LessonMediaDTO> lessonMediaDTOs) {
//		List<String> codes = chapterService.getCodeChapterByLessonId(lessonId);
//		if (codes != null && codes.size() > 0) {
//			String codeChapter = codes.get(0);
//			if (CodeChapter.GROUP_DI.contains(codeChapter))
//				lessonMediaDTOs = handleListDTO(lessonMediaDTOs, File.TYPE_IMAGE);
//			if (codeChapter.equals(CodeChapter.CODE_RA))
//				lessonMediaDTOs = handleListDTO(lessonMediaDTOs, File.TYPE_SHADOWING);
//		}
//		return lessonMediaDTOs;
//	}
//
//	private void handleSaveNew(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lesson) {
//		List<LessonMediaDTO> lessonMediaDTOS = getDTOList(lessonMediaDTOs);
//
//		save(lessonMediaDTOS, lesson);
//	}
//
//	private void handleDeleteEntityNotUse(List<LessonMediaEntity> lessonMedias, List<LessonMediaDTO> lessonMediaDTOs) {
//		List<LessonMediaEntity> listNotUses = getListNotUse(lessonMedias, lessonMediaDTOs);
//		if (listNotUses != null && listNotUses.size() > 0) {
//			listNotUses.forEach(item -> {
//				delete(item);
//			});
//		}
//	}
//
//	private void handleUpdate(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lesson, LocalDateTime createTime,
//		Long createBy) {
//		List<LessonMediaDTO> listDTOs = getEntityList(lessonMediaDTOs);
//		if (listDTOs != null && listDTOs.size() > 0) {
//			listDTOs.forEach(dto -> {
//				update(dto, lesson, createTime, createBy);
//			});
//		}
//	}
//
//	private List<LessonMediaDTO> getDTOList(List<LessonMediaDTO> dtos) {
//		List<LessonMediaDTO> lessonMediaDTOS = new ArrayList<LessonMediaDTO>();
//		dtos.forEach(dto -> {
//			if (dto.getId() == null || dto.getId() == 0)
//				lessonMediaDTOS.add(dto);
//		});
//		return lessonMediaDTOS;
//	}
//
//	private List<LessonMediaDTO> getEntityList(List<LessonMediaDTO> dtos) {
//		List<LessonMediaDTO> lessonMediaDTOS = new ArrayList<LessonMediaDTO>();
//		dtos.forEach(dto -> {
//			if (dto.getId() != null && dto.getId() != 0)
//				lessonMediaDTOS.add(dto);
//		});
//		return lessonMediaDTOS;
//	}
//
//	private List<LessonMediaEntity> getListNotUse(List<LessonMediaEntity> lessonMedias, List<LessonMediaDTO> dtos) {
//		List<Long> ids = new ArrayList<Long>();
//		dtos.forEach(dto -> {
//			ids.add(dto.getId());
//		});
//		List<LessonMediaEntity> results = new ArrayList<LessonMediaEntity>();
//		for (int i = 0; i < lessonMedias.size(); i++) {
//			LessonMediaEntity current = lessonMedias.get(i);
//			Long id = current.getId();
//			if (!ids.contains(id)) {
//				results.add(current);
//			}
//		}
//		return results;
//	}
//
//	private boolean compareLinkURLMedia(LessonMediaEntity lessonMedia, LessonMediaDTO lessonMediaDTO) {
//		String urlEntity = lessonMedia.getLessonMediaLink();
//		String urlDTO = lessonMediaDTO.getLessonMediaLink();
//		boolean isEquals = false;
//		if (urlEntity != null && urlDTO != null) {
//			if (urlEntity.equalsIgnoreCase(urlDTO))
//				isEquals = true;
//		}
//		return isEquals;
//	}
//
//	private void setLinkMediaAndType(LessonMediaEntity lessonMedia) {
//		String link = null;
//		String lessonMediaLink = lessonMedia.getLessonMediaLink();
//		if (lessonMediaLink != null && !lessonMediaLink.equals("")) {
//			String attribute = getType(lessonMediaLink);
//			if (attribute.equals(File.TYPE_AUDIO))
//				link = setLinkMedia(lessonMediaLink);
//			else
//				link = lessonMediaLink;
//
//			lessonMedia.setLessonMediaLink(link);
//			if (lessonMedia.getType() == null || lessonMedia.getType().equals(""))
//				lessonMedia.setType(attribute);
//		}
//	}
//
//	private String getType(String link) {
//		if (link != null && !link.equals("")) {
//			String attributeFile = fileHandle.getAttributeFile(link).substring(1);//get attribute file
//			if (checkAudio(attributeFile) != null)
//				return checkAudio(attributeFile);
//			else if (checkVideo(attributeFile) != null)
//				return checkVideo(attributeFile);
//			else if (checkImage(attributeFile) != null)
//				return checkImage(attributeFile);
//			else
//				return File.TYPE_AUDIO;
//		}
//		return null;
//	}
//
//	private String checkImage(String attribute) {
//		if (Arrays.asList(ATTRIBUTES_IMAGE).contains(attribute))
//			return File.TYPE_IMAGE;
//		return null;
//	}
//
//	private String checkAudio(String attribute) {
//		if (Arrays.asList(ATTRIBUTES_AUDIO).contains(attribute))
//			return File.TYPE_AUDIO;
//		return null;
//	}
//
//	private String checkVideo(String attribute) {
//		if (Arrays.asList(ATTRIBUTES_VIDEO).contains(attribute))
//			return File.TYPE_VIDEO;
//		return null;
//	}
//
//	@Override
//	public void save(List<LessonMediaDTO> lessonMediaDTOs, LessonEntity lessonSaved) {
//		if (lessonMediaDTOs != null && lessonMediaDTOs.size() > 0) {
//			lessonMediaDTOs.forEach(lessonMedia -> {
//				save(lessonMedia, lessonSaved);
//			});
//		}
//	}
//
//	private void setTime(LessonMediaEntity lessonMedia) {
//		lessonMedia.setCreatedDate(DateTime.getInstances());
//		lessonMedia.setCreatedBy(UserContext.getId());
//		lessonMedia.setModifiedDate(DateTime.getInstances());
//		lessonMedia.setModifiedBy(UserContext.getId());
//	}
//
//	private String setLinkMedia(String url) {
//		String link = this.fileHandle.setURLServer(url);
//		return link;
//	}
//
//	public void uploadFile(LessonAudioCreateDTO lessonAudioDTO, MultipartFile image) {
//		if (image != null ) {
//			List<LessonMediaDTO> lessonMediaDTOs = lessonAudioDTO.getLessonMedias();
//			if (lessonMediaDTOs == null)
//				lessonMediaDTOs = new ArrayList<LessonMediaDTO>();
//			File fileImage = uploadFile(image);
//			addEntity(fileImage, lessonMediaDTOs, File.TYPE_IMAGE);
//			lessonAudioDTO.setLessonMedias(lessonMediaDTOs);
//		}
//	}
//
//	private File uploadFile(MultipartFile image) {
//		File file = null;
//		if (image != null) {
//			file = new File();
//			file.setFile(image);
//			this.fileHandle.update(file);
//		}
//		return file;
//	}
//
//	private void addEntity(File file, List<LessonMediaDTO> lessonMediaDTOs, String type) {
//		if (file != null) {
//			LessonMediaDTO lessonMediaDTO = new LessonMediaDTO();
//			lessonMediaDTO.setLessonMediaLink(file.getFileName());
//			lessonMediaDTO.setType(type);
//			lessonMediaDTOs.add(lessonMediaDTO);
//		}
//	}
//
//	@Override
//	public void deleteByLesson(Long lessonId) {
//		List<LessonMediaEntity> lessonMedias = this.lessonMediaRepository.findByLessonId(lessonId);
//		if (lessonMedias != null && lessonMedias.size() > 0) {
//			for (LessonMediaEntity lessonMedia : lessonMedias) {
//				delete(lessonMedia);
//			}
//		}
//	}
//
//	private void delete(LessonMediaEntity lessonMedia) {
//		this.lessonMediaRepository.deleteById(lessonMedia.getId());
//	}
//
//	@Override
//	public void uploadFile(LessonCreationDTO lessonAudioDTO, MultipartFile image) {
//		// TODO Auto-generated method stub
//		
//	}

}
