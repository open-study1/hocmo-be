package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.LessonPriorityDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonPriorityEntity;
import com.edu.openstudy.data.mapper.LessonPriorityMapper;
import com.edu.openstudy.data.repository.LessonPriorityRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.ChapterService;
import com.edu.openstudy.service.LessonPriorityService;
import com.edu.openstudy.service.LessonService;

@Service
public class LessonPriorityServiceImpl implements LessonPriorityService {
	@Autowired
	private LessonPriorityRepository lessonPriorityRepository;
	@Autowired
	private LessonPriorityMapper lessonPriorityMapper;
	@Autowired
	private LessonService lessonService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private MessageSource messageSource;
	@Override
	public LessonPriorityDTO creation(LessonPriorityDTO lessonPriorityDTO) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void delete(Long lessonId) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public LessonPriorityDTO creation(LessonPriorityDTO lessonPriorityDTO) {
//		LessonPriorityEntity lessonPriority = this.lessonPriorityMapper.mapToEntity(lessonPriorityDTO);
//		Long lessonId = lessonPriority.getLesson().getId();
//		int priorityNumber = lessonPriority.getPriority();
//		Optional<LessonPriorityEntity> lessonPriorityOptional = findByLessonIdAndUserId(lessonId, UserContext.getId());
//		boolean isExistsNumberPriority = this.chapterService.checkPriorityNumber(priorityNumber);
//		if (!isExistsNumberPriority)
//			throw new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null), "Priority",
//					"number", lessonPriority.getPriority()), TypeError.NotFound);
//		if (!lessonPriorityOptional.isPresent()) {
//			// save new
//			return save(lessonPriority, lessonId);
//		} else {
//			return update(lessonPriorityOptional, lessonPriorityDTO);
//		}
//
//	}
//
//	@Override
//	public void delete(Long lessonId) {
//		Optional<LessonPriorityEntity> lessonPriorityOptional = findByLessonIdAndUserId(lessonId, UserContext.getId());
//		if (lessonPriorityOptional.isPresent())
//			this.lessonPriorityRepository.deleteById(lessonPriorityOptional.get().getId());
//	}
//
//	private Optional<LessonPriorityEntity> findByLessonIdAndUserId(Long lessonId, Long userId) {
//		return this.lessonPriorityRepository.findByLessonIdAndCreatedBy(lessonId, userId);
//	}
//
//	private LessonPriorityDTO save(LessonPriorityEntity lessonPriority, Long lessonId) {
//		// set time now
//		lessonPriority.setCreatedBy(UserContext.getId());
//		lessonPriority.setCreatedDate(LocalDateTime.now());
//		lessonPriority.setModifiedBy(UserContext.getId());
//		lessonPriority.setModifiedDate(LocalDateTime.now());
//		// set Lesson
//		LessonEntity lesson = this.lessonService.findById(lessonId);
//		lessonPriority.setLesson(lesson);
//		LessonPriorityEntity lessonPrioritySaved = this.lessonPriorityRepository.save(lessonPriority);
//		return this.lessonPriorityMapper.mapToDTO(lessonPrioritySaved);
//	}
//
//	private LessonPriorityDTO update(Optional<LessonPriorityEntity> lessonPriorityOptional,
//			LessonPriorityDTO lessonPriorityDTO) {
//		LessonPriorityEntity lessonPriorityEntity = lessonPriorityOptional.get();
//		lessonPriorityEntity.setPriority(lessonPriorityDTO.getPriority());
//		lessonPriorityEntity.setModifiedDate(LocalDateTime.now());
//		lessonPriorityEntity.setModifiedBy(UserContext.getId());
//		LessonPriorityEntity lessonPriorityUpdated = this.lessonPriorityRepository.save(lessonPriorityEntity);
//		return this.lessonPriorityMapper.mapToDTO(lessonPriorityUpdated);
//	}
}
