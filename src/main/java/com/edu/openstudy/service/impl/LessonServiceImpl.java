package com.edu.openstudy.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.edu.openstudy.common.*;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.common.util.FileHandle;
import com.edu.openstudy.common.util.LessonDAO;
import com.edu.openstudy.common.util.LessonUtil;
import com.edu.openstudy.constant.Constant;
import com.edu.openstudy.data.dto.*;
import com.edu.openstudy.data.entity.LessonEntity;
import com.edu.openstudy.data.entity.LessonPriorityEntity;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.mapper.LessonMapper;
import com.edu.openstudy.data.mapper.LessonMediaMapper;
import com.edu.openstudy.data.mapper.LessonPriorityMapper;
import com.edu.openstudy.data.repository.ChapterRepository;
import com.edu.openstudy.data.repository.LessonPriorityRepository;
import com.edu.openstudy.data.repository.LessonRepository;
import com.edu.openstudy.data.repository.ExamRepository;
import com.edu.openstudy.data.repository.specification.LessonSpecification;
import com.edu.openstudy.data.request.RequestLessonDTO;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.*;
import com.edu.openstudy.service.strategy.LessonStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class LessonServiceImpl implements LessonService {
	@Autowired
	private LessonRepository lessonRepository;
	@Autowired
	private LessonDAO lessonDAO;
	@Autowired
	private LessonMapper lessonMapper;
	@Autowired
	private QuestionService questionService;
	private PaginationDTO paginationDTO;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExamService questionGroupService;
	@Autowired
	private FileHandle fileHandle;
	@Autowired
	private LessonMediaService lessonMediaService;
	private LessonStrategy readAloudStrategy;
	@Autowired
	private DiscussionService discussionService;
	@Resource(name = "LessonUtil")
	private LessonUtil lessonUtil;
	@Autowired
	private ChapterMapper chapterMapper;
	@Autowired
	private LessonPriorityMapper lessonPriorityMapper;
	@Autowired
	private LessonMediaMapper lessonMediaMapper;
	@Autowired
	private ChapterService chapterService;
	final int LEANER = 1;
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public LessonDetailViewDTO getDetailByChapterAndZOder(Long zOder, Long chapterId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void update(long id, LessonCreateDTO dto) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void active(Long id) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Long getByQuestion(Long questionId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getContentByQuestionId(Long questionId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void disableById(Long id) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void deleteByIdAndCode(Long id, String code) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void deleteByListId(List<Long> ids) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setStatusListIdLesson(List<Long> listIdLesson, boolean status) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void resetAllZorder() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public LessonCreateDTO save(LessonCreateDTO lessonCreateDTO) {
		// set properties and save
		LessonEntity lessonEntity = lessonMapper.mapToEntity(lessonCreateDTO);
		lessonEntity.setCreatedBy((long) 1);
		lessonEntity.setModifiedBy((long) 1);
		lessonEntity.setCreatedDate(DateTime.getInstances());
		lessonEntity.setModifiedDate(DateTime.getInstances());
		this.lessonRepository.save(lessonEntity);
		Logger.logInfo.info(this.messageSource.getMessage("AddedEntitySuccess", null, null), UserContext.getId(),
				lessonEntity.getTitle(), "lesson", lessonEntity.getChapter());
		return this.lessonMapper.mapToDTO(lessonEntity);
	}
//
//	@Override
//	public LessonCreateDTO save(LessonCreationDTO lesson) {
//		MultipartFile image = lesson.getImage();
//		this.lessonMediaService.uploadFile(lesson, image);
//		return save(lesson);
//	}
//
//	@Override
//	public LessonViewDTO update(LessonCreationDTO lesson, Long id) {
////		LessonAudioCreateDTO dto = lesson.getLesson();
////		MultipartFile image = lesson.getImage();
////		lessonMediaService.uploadFile(dto, image);
////		// update
////		update(dto, id);
//		return null;
//	}
//
//
//	@Override
//	public void save(LessonCreateDIDTO lessonCreateDIDTO) {
//		LessonCreateDTO lessonCreateDTO = lessonCreateDIDTO.getLessonCreateDTO();
//		MultipartFile fileUp = lessonCreateDIDTO.getImage();
//		// upload file
//		if (fileUp != null) {
//			File file = new File();
//			file.setFile(fileUp);
//			this.fileHandle.update(file);
//			lessonCreateDTO.setSourceMediaLinkImage(file.getFileName());
//		}
//		save(lessonCreateDTO);
//	}
//
//
//	@Override
//	public Long getByQuestion(Long questionId) {
//		LessonEntity lesson = this.lessonRepository.findByQuestionId(questionId).orElseThrow(
//				() -> new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
//						"Question", "Id", String.valueOf(questionId)), TypeError.NotFound));
//		return lesson.getId();
//
//	}
//
	@Override
	public boolean isExists(Long id) {
		return lessonRepository.existsById(id);
	}

	@Override
	public LessonEntity findById(Long id) {
		LessonEntity lesson = lessonRepository.findById(id).get();
		return lesson;

	}

//	@Override
//	public boolean existsById(Long id) {
//		return lessonRepository.existsById(id);
//	}
//
//	@Override
//	public long count() {
//		return lessonRepository.count();
//	}
//
//	@Override
//	public String getContentByQuestionId(Long questionId) {
//		return this.lessonRepository.getContentByQuestionId(questionId);
//	}
//
//	@Override
//	public void update(long id, LessonCreateDTO dto) {
//		// Get Chapter from Database
//		List<ChapterDTO> chapterDTOs = dto.getChapterDTO();
//		LessonEntity lesson = lessonUtil.setModifiedAndProperty(id, dto);
//		// Update lesson
//		LessonEntity lessonUpdated = this.lessonRepository.save(lesson);
//		// update Source
//		lessonChapterService.updateListLessonChapter(chapterDTOs, lessonUpdated);
//		Boolean isEmbedded = lesson.getQuestionGroup().getIsEmbedded();
//		this.questionService.updateList(lesson.getQuestionGroup().getId(), dto.getQuestionDTO(),
//				lesson.getQuestionGroup(), isEmbedded);
//		Logger.logInfo.info(this.messageSource.getMessage("UpdateEntitySuccess", null, null), lessonUpdated.getTitle(),
//				"lesson");
//	}
//
//	@Override
//	public PaginationDTO findByRequestLessonDTO(RequestLessonDTO requestDTO) {
//
//		paginationDTO = new PaginationDTO();
//		// Create query by requestDTO
//		String query = lessonUtil.makeFindLessonQueryWithRequestDAO(requestDTO);
//		// Execute a query
//		List<LessonViewDTO> lessons = new ArrayList<>();
//		lessons = lessonDAO.findByQuery(query, requestDTO.getPageNumber(), requestDTO.getLimit()).stream()
//				.map(lesson -> {
//					LessonViewDTO lessonViewDTO = lessonMapper.mapToViewIncludePriorityDTO(lesson);
//					// set chapter
//					lessonViewDTO.setCategories(lesson.getListLesson_Chapter().stream()
//							.map(lessonChapter -> chapterMapper.mapToDTO(lessonChapter.getChapter()))
//							.collect(Collectors.toList()));
//					// set priority
//					List<LessonPriorityViewDTO> lessonPriorityViewDTOs = new ArrayList<>();
//					for (LessonPriorityEntity lessonPriority : lesson.getLessonPriorities()) {
//						if (lessonPriority.getModifiedBy() == UserContext.getId()) {
//							lessonPriorityViewDTOs.add(lessonPriorityMapper.mapToViewDTO(lessonPriority));
//						}
//					}
//					lessonViewDTO.setPriorities(lessonPriorityViewDTOs);
//					return lessonViewDTO;
//				}).collect(Collectors.toList());
//		// check lesson is empty
//		Logger.logInfo.info("Get lesson by requestDAO {}", "Lesson");
//		paginationDTO = paginationDTO.customPaginationLesson(lessons, lessonDAO.countTotalRecords(query),
//				requestDTO.getPageNumber(), requestDTO.getLimit());
//
//		Logger.logInfo.info("Get lesson by query" + query, "Lesson");
//		return paginationDTO;
//
//	}

	@Override
	public PaginationDTO findByRequestLessonDTO(RequestLessonDTO requestDTO) {
		List<String> chapterCodes = new ArrayList<>();
		List<Long> lessonIdWithChapterFilters = new ArrayList<>();
		List<Long> lessonIdWithPriorityFilters = new ArrayList<>();
		List<Long> lessonIdWithPracticeFilters = new ArrayList<>();
		List<Long> lessonIdNotExplanations = new ArrayList<>();
		List<Long> lessonIdNoChapters = new ArrayList<>();
		Pageable pageable = null;
		if (requestDTO.getSort() != null) {
			if (requestDTO.getSort() == Constant.SORT_BY_TITLE) {
				pageable = PageRequest.of(requestDTO.getPageNumber(), requestDTO.getLimit(), Sort.Direction.ASC,
						"title");
			}
			if (requestDTO.getSort() == Constant.SORT_BY_DATE) {
				pageable = PageRequest.of(requestDTO.getPageNumber(), requestDTO.getLimit(), Sort.Direction.DESC,
						"modifiedDate");
			}
			if (requestDTO.getSort() == Constant.SORT_BY_ZODER) {
				pageable = PageRequest.of(requestDTO.getPageNumber(), requestDTO.getLimit(), Sort.Direction.DESC,
						"zOrder");
			}
		} else {
			pageable = PageRequest.of(requestDTO.getPageNumber(), requestDTO.getLimit());
		}
		if (requestDTO.getChapterCode() != null) {
//			if (requestDTO.getChapterId() == Constant.NO_Chapter) {
//				lessonIdNoChapters = lessonRepository.getAllIdLessonNoChapter();	
//			} else {
				List<String> codes = findCodeChild(requestDTO.getChapterCode(), chapterCodes);
				for (String code : codes) {
					lessonIdWithChapterFilters.addAll(lessonRepository.findLessonIdByChapterCode(code));
				}
//			}
		}
//		if (requestDTO.getPriority() != null) {
//			lessonIdWithPriorityFilters = lessonRepository.getAllIdByUserIdPriority(UserContext.getId());
//		}
//		if (requestDTO.getPracticeStatus() != null) {
//			lessonIdWithPracticeFilters = lessonRepository.getAllIdByUserPractice(UserContext.getId());
//		}
//		if (requestDTO.getExplanation() != null) {
//			lessonIdNotExplanations = lessonRepository.getAllIdLessonNotExplanation();
//		}

		Page<LessonViewDTO> pageDto = lessonRepository
				.findAll(
						LessonSpecification.filter(requestDTO, lessonIdWithChapterFilters, lessonIdWithPracticeFilters,
								lessonIdWithPriorityFilters, lessonIdNotExplanations, lessonIdNoChapters),
						pageable)
				.map(lessonEntity -> {
					LessonViewDTO lessonViewDTO = lessonMapper.mapToViewIncludePriorityDTO(lessonEntity);
					// set chapter
					lessonViewDTO.setChapter(chapterMapper.mapToDTO(lessonEntity.getChapter()));
					// set priority
					List<LessonPriorityViewDTO> lessonPriorityViewDTOs = new ArrayList<>();
					for (LessonPriorityEntity lessonPriority : lessonEntity.getLessonPriorities()) {
						if (lessonPriority.getModifiedBy() == UserContext.getId()) {
							lessonPriorityViewDTOs.add(lessonPriorityMapper.mapToViewDTO(lessonPriority));
						}
					}
					lessonViewDTO.setPriorities(lessonPriorityViewDTOs);
					return lessonViewDTO;
				});
		return new PaginationDTO(pageDto);
	}

	@Override
	public LessonDetailViewDTO getDetailById(Long id) {
		LessonDetailViewDTO detailViewDTO = new LessonDetailViewDTO();
		LessonEntity lessonEntity = findById(id);
//		if (UserContext.getTypeUser() == LEANER) {
			detailViewDTO = lessonMapper.mapToViewDetailIncludePriorityDTO(lessonEntity);
			// set chapter
			detailViewDTO.setChapter(chapterMapper.mapToDTO(lessonEntity.getChapter()));
			// set priority
			List<LessonPriorityViewDTO> lessonPriorityViewDTOs = new ArrayList<>();
			for (LessonPriorityEntity lessonPriority : lessonEntity.getLessonPriorities()) {
				if (lessonPriority.getModifiedBy() == UserContext.getId()) {
					lessonPriorityViewDTOs.add(lessonPriorityMapper.mapToViewDTO(lessonPriority));
				}
			}
			detailViewDTO.setPriorities(lessonPriorityViewDTOs);
//		} else {
//			detailViewDTO = lessonMapper.mapToViewDetailDTO(lessonEntity);
//			detailViewDTO.setCategories(lessonEntity.getListLesson_Chapter().stream()
//					.map(lessonChapter -> this.chapterMapper.mapToDTO(lessonChapter.getChapter()))
//					.collect(Collectors.toList()));
//		}
		Logger.logInfo.info("Get lessondetail by id {}", "Lesson");
		return detailViewDTO;
	}
//
//	
////	Fixing 
//	@Override
//	public LessonDetailViewDTO getDetailByChapterAndZOder(Long zOder ,Long chapterId) {
//		LessonDetailViewDTO detailViewDTO = new LessonDetailViewDTO();
//		LessonEntity lessonEntity = findByZorderAndChapter(zOder, chapterId);
//		if (UserContext.getTypeUser() == LEANER) {
//			detailViewDTO = lessonMapper.mapToViewDetailIncludePriorityDTO(lessonEntity);
//			// set chapter
//			detailViewDTO.setCategories(lessonEntity.getListLesson_Chapter().stream()
//					.map(lessonChapter -> chapterMapper.mapToDTO(lessonChapter.getChapter()))
//					.collect(Collectors.toList()));
//			// set priority
//			List<LessonPriorityViewDTO> lessonPriorityViewDTOs = new ArrayList<>();
//			for (LessonPriorityEntity lessonPriority : lessonEntity.getLessonPriorities()) {
//				if (lessonPriority.getModifiedBy() == UserContext.getId()) {
//					lessonPriorityViewDTOs.add(lessonPriorityMapper.mapToViewDTO(lessonPriority));
//				}
//			}
//			detailViewDTO.setPriorities(lessonPriorityViewDTOs);
//		} else {
//			detailViewDTO = lessonMapper.mapToViewDetailDTO(lessonEntity);
//			detailViewDTO.setCategories(lessonEntity.getListLesson_Chapter().stream()
//					.map(lessonChapter -> this.chapterMapper.mapToDTO(lessonChapter.getChapter()))
//					.collect(Collectors.toList()));
//		}
//		Logger.logInfo.info("Get lessondetail by id {}", "Lesson");
//		return detailViewDTO;
//	}
//
//	
//
	
	@Override
	public LessonCreateDTO readJson(String lesson, MultipartFile image) {
		LessonCreateDTO lessonCreateDTO = new LessonCreateDTO();
		try {
			lessonCreateDTO = ObjectMapper.getMapper().readValue(lesson, LessonCreateDTO.class);
		} catch (JsonProcessingException ex) {
			ex.printStackTrace();
		}
		if (image != null)
			lessonCreateDTO.setImage(image);
		return lessonCreateDTO;
	}
	@Override
	public LessonEntity findByZorderAndChaspter(Long zOrder, Long chapterId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public LessonViewDTO update(LessonCreateDTO lesson, Long id) {
		// TODO Auto-generated method stub
		return null;
	}
//
//	@Override
//	public LessonEntity findByZorderAndChapter(Long zOrder, Long idChapter) {
//		return this.lessonRepository.findByChapterAndZorder(zOrder, idChapter).orElseThrow(() -> new ErrorMessageException(
//				String.format(messageSource.getMessage("NotFound", null, null), "Lesson", "ZOder", String.valueOf(zOrder)),
//				TypeError.NotFound));
//	}
//
//	@Override
//	public void active(Long id) {
//		LessonEntity lesson = findById(id);
//		lesson.setStatus(true);
//		lesson.setUserIdAsAppover(UserContext.getId());
//		lesson.setModifiedDate(DateTime.getInstances());
//		Logger.logInfo.info(this.messageSource.getMessage("activeLesson", null, null), UserContext.getId(),
//				lesson.getTitle(), DateTim le.getInstances().toString());
//		this.lessonRepository.save(lesson);
//	}
//
//	@Override
//	public void deleteById(Long id) {
//		boolean isExistsId = existsById(id);
//		if (!isExistsId)
//			throw new ErrorMessageException(
//					String.format(messageSource.getMessage("NotFound", null, null), "lesson", "Id", String.valueOf(id)),
//					TypeError.NotFound);
//		this.lessonChapterService.deleteByLessonId(id);
//		// delete question group
//		this.questionGroupService.deleteByLessonId(id);
//		// delete lesson media
//		this.lessonMediaService.deleteByLesson(id);
//		// delete discussion
//		discussionService.deleteByLesson(id);
//		// delete lesson tested
//		this.lessonRepository.deleteById(id);
//	}
//
//	@Override
//	public void deleteByListId(List<Long> ids) {
//		if (ids != null && ids.size() > 0) {
//			for (Long id : ids) {
//				deleteById(id);
//			}
//		}
//	}
//
//	@Override
//	public void deleteByIdAndCode(Long id, String code) {
//		lessonUtil.checkExistId(id);
//		boolean isSingleChapter = this.lessonChapterService.lessonIsSingleChapter(id);
//		if (isSingleChapter == true) {
//			deleteById(id);
//		} else {
//			LessonEntity lesson = this.findById(id);
//			lessonChapterService.deleteByLessonIdAndCodeChapter(lesson.getId(), code);
//		}
//	}
//
//	@Override
//	public void disableById(Long id) {
//		LessonEntity lesson = findById(id);
//		lesson.setStatus(false);
//		lesson.setModifiedDate(DateTime.getInstances());
//		Logger.logInfo.info(this.messageSource.getMessage("disableLesson", null, null), UserContext.getId(),
//				lesson.getTitle(), DateTime.getInstances().toString());
//		this.lessonRepository.save(lesson);
//	}
//

//
//	@Override
//	public void setStatusListIdLesson(List<Long> listIdLesson, boolean status) {
//		if (listIdLesson != null && listIdLesson.size() > 0) {
//			listIdLesson.forEach(id -> {
//				LessonEntity lesson = this.findById(id);
//				lesson.setStatus(status);
//				lesson.setUserIdAsAppover(UserContext.getId());
//				this.lessonRepository.save(lesson);
//			});
//		}
//	}
//
//	@Override
//	public void resetAllZorder() {
//		List<ChapterDTO> chapterDTOS = chapterService.getAllByGroup(Constant.PTE);
//		chapterDTOS.forEach(chapterDTO -> {
//			Long chapterId = chapterDTO.getId();
//			List<LessonEntity> lessonEntities = lessonRepository.getByChapterAndOrderByZorder(chapterId);
//			// reset order
//			if (lessonEntities != null && lessonEntities.size() > 0) {
//				Long indexZorder = Long.valueOf(1);
//				for (LessonEntity lesson : lessonEntities) {
//					lesson.setZOrder(indexZorder);
//					LessonEntity lessonEntityUpdated = this.lessonRepository.save(lesson);
//					indexZorder++;
//				}
//			}
//		});
//	}
//
//
	
	private List<String> findCodeChild(String code, List<String> chapterCodes) {
		chapterCodes.add(code);
		List<String> codes = chapterService.getCodeByParentCode(code);
		if (codes.size() > 0) {
			for (int i = 0; i < codes.size(); i++) {
				findCodeChild(codes.get(i), chapterCodes);
			}
			return chapterCodes;
		} else {
			return chapterCodes;
		}
	}

}
