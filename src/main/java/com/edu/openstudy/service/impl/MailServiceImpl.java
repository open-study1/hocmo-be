package com.edu.openstudy.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.edu.openstudy.service.MailService;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
@Service
public class MailServiceImpl  implements MailService{
	
    @Autowired
    private JavaMailSender mailSender;

    private final OkHttpClient client;
    
	public MailServiceImpl() {
	    this.client = new OkHttpClient().newBuilder().build();
	}

	@Override
	  public void sendEmail(String from, String to, String subject, String text, String category) throws IOException {
	    MediaType mediaType = MediaType.parse("application/json");
	    RequestBody body = RequestBody.create(mediaType,
	        "{\"from\":{\"email\":\"" + from + "\",\"name\":\"Mailtrap Test\"},\"to\":[{\"email\":\"" + to + "\"}],\"subject\":\"" + subject + "\",\"text\":\"" + text + "\",\"category\":\"" + category + "\"}");
	    Request request = new Request.Builder()
	        .url("https://send.api.mailtrap.io/api/send")
	        .method("POST", body)
	        .addHeader("Authorization", "Bearer ********67a4")
	        .addHeader("Content-Type", "application/json")
	        .build();
	    Response response = client.newCall(request).execute();
	  } 
	@Override
	 public void sendEmail(String to, String subject, String body) {
	        SimpleMailMessage message = new SimpleMailMessage();
	        message.setTo(to);
	        message.setSubject(subject);
	        message.setText(body);

//	        mailSender.send(message);
	    }
	@Override
	public void sendHtmlEmail() throws MessagingException {
	    MimeMessage message = mailSender.createMimeMessage();

	    message.setFrom(new InternetAddress("sender@example.com"));
	    message.setRecipients(MimeMessage.RecipientType.TO, "recipient@example.com");
	    message.setSubject("Test email from Spring");

	    String htmlContent = "<h1>This is a test Spring Boot email</h1>" +
	                         "<p>It can contain <strong>HTML</strong> content.</p>";
	    message.setContent(htmlContent, "text/html; charset=utf-8");

	    mailSender.send(message);
	}

}
