package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionOptionEntity;
import com.edu.openstudy.data.mapper.QuestionOptionMapper;
import com.edu.openstudy.data.repository.QuestionOptionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.openstudy.service.QuestionOptionService;

@Service
public class QuestionOptionServiceImpl implements QuestionOptionService {
	@Autowired
	private QuestionOptionRepository questionOptionRepository;
	@Autowired
	private QuestionOptionMapper questionOptionMapper;
	@Override
	public void save(QuestionOptionDTO optionDto, QuestionEntity questionSaved) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(QuestionOptionDTO optionDto, QuestionEntity questionSaved, LocalDateTime createDate,
			Long createDateBy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(List<QuestionOptionDTO> optionDtos, QuestionEntity questionSaved, LocalDateTime createDate,
			Long createDateBy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void deleteByQuestionId(Long questionId) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public void save(QuestionOptionDTO optionDto, QuestionEntity questionSaved) {
//		QuestionOptionEntity questionOption = this.questionOptionMapper.mapToEntity(optionDto);
//		//set properties
//		questionOption.setQuestion(questionSaved);
//		this.questionOptionRepository.save(questionOption);
//	}
//
//	@Override
//	public void update(QuestionOptionDTO optionDto, QuestionEntity questionSaved, LocalDateTime createDate,
//		Long createDateBy) {
//		QuestionOptionEntity questionOption = this.questionOptionMapper.mapToEntity(optionDto);
//		//set
//		questionOption.setQuestion(questionSaved);
//		questionOption.setCreatedDate(createDate);
//		questionOption.setCreatedBy(createDateBy);
//		questionOption.setModifiedDate(DateTime.getInstances());
//		questionOption.setModifiedBy(UserContext.getId());
//		this.questionOptionRepository.save(questionOption);
//	}
//
//	@Override
//	public void update(List<QuestionOptionDTO> optionDtos, QuestionEntity questionSaved, LocalDateTime createDate,
//		Long createDateBy) {
//		List<QuestionOptionEntity> questionOptions = this.questionOptionRepository
//				.findQuestionOptionByQuestionId(questionSaved.getId());
//		//create new option
//		handleSaveNewOption(optionDtos, questionSaved);
//		//delete with entity no use
//		handleDeleteEntityNotUse(optionDtos, questionOptions);
//		handleUpdateList(optionDtos, questionSaved, createDate, createDateBy);
//	}
//
//	private void handleSaveNewOption(List<QuestionOptionDTO> optionDtos, QuestionEntity questionSaved) {
//		List<QuestionOptionDTO> dtos = getDTOListOption(optionDtos);
//		if (dtos != null && dtos.size() > 0) {
//			dtos.forEach(dto -> {
//				save(dto, questionSaved);
//			});
//		}
//	}
//
//	private void handleDeleteEntityNotUse(List<QuestionOptionDTO> optionDtos, List<QuestionOptionEntity> questionOptions) {
//		List<QuestionOptionEntity> optionNotUse = getListNotUse(questionOptions, optionDtos);
//		if (optionNotUse != null && optionNotUse.size() > 0) {
//			optionNotUse.forEach(option -> {
//				deleteByQuestionOption(option);
//			});
//		}
//	}
//
//	private void handleUpdateList(List<QuestionOptionDTO> optionDtos, QuestionEntity questionSaved, LocalDateTime createDate,
//		Long createDateBy) {
//		List<QuestionOptionDTO> entities = getEntityListOption(optionDtos);
//		if (entities != null && entities.size() > 0) {
//			entities.forEach(dto -> {
//				update(dto, questionSaved, createDate, createDateBy);
//			});
//		}
//	}
//
//	@Override
//	public void deleteByQuestionId(Long questionId) {
//		List<QuestionOptionEntity> questionOptions = this.questionOptionRepository.findQuestionOptionByQuestionId(questionId);
//		questionOptions.forEach(option -> {
//			this.questionOptionRepository.deleteById(option.getId());
//		});
//	}
//
//	private List<QuestionOptionDTO> getEntityListOption(List<QuestionOptionDTO> dtos) {
//		List<QuestionOptionDTO> options = new ArrayList<QuestionOptionDTO>();
//		dtos.forEach(dto -> {
//			if (dto.getId() != 0)
//				options.add(dto);
//		});
//		return options;
//	}
//
//
//	public void deleteByQuestionOption(QuestionOptionEntity option) {
//		this.questionOptionRepository.delete(option);
//	}f
//
//	private List<QuestionOptionEntity> getListNotUse(List<QuestionOptionEntity> options, List<QuestionOptionDTO> dtos) {
//
//		List<Long> ids = new ArrayList<Long>();
//		//get id from dtos
//		dtos.forEach(dto -> {
//			ids.add(dto.getId());
//		});
//		List<QuestionOptionEntity> results = new ArrayList<QuestionOptionEntity>();
//		if (options != null && options.size() > 0) {
//			//compare id entity and id dto. if id entity not exists in idDTO, add array to Remove
//			for (int i = 0; i < options.size(); i++) {
//				QuestionOptionEntity optionCurrent = options.get(i);
//				Long id = optionCurrent.getId();
//				if (!ids.contains(id)) {
//					results.add(optionCurrent);
//				}
//			}
//		}
//		return results;
//	}

}
