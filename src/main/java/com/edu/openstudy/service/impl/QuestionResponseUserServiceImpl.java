package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.util.FileHandle;
import com.edu.openstudy.data.dto.ExamDTO;
import com.edu.openstudy.data.dto.QuestionDTO;
import com.edu.openstudy.data.dto.QuestionExerciseDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserDoExamDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserViewDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import com.edu.openstudy.data.mapper.QuestionResponseUserMapper;
import com.edu.openstudy.data.repository.QuestionRepository;
import com.edu.openstudy.data.repository.QuestionResponseUserRepository;
import com.edu.openstudy.service.QuestionResponseUserService;
import com.edu.openstudy.service.UserScoreService;

import jakarta.transaction.Transactional;


@Service
@Transactional
public class QuestionResponseUserServiceImpl implements QuestionResponseUserService {
	@Autowired
	private QuestionResponseUserRepository questionResponseUserRepository;
	@Autowired
	private QuestionResponseUserMapper questionResponseUserMapper;
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private FileHandle fileHandle;
	@Autowired
	private MessageSource messageSource;

	@Override
	public QuestionExerciseDTO save(QuestionResponseUserDTO dto) {
		//set question
		QuestionResponseUserEntity questionResponseUser = questionResponseUserMapper.mapToEntity(dto);
		if(questionResponseUser.getQuestion() != null)
		{
			QuestionEntity question = this.questionRepository.findById(questionResponseUser.getQuestion().getId()).get();
			questionResponseUser.setQuestion(question);
		}
		LocalDateTime createDate = DateTime.getInstances();
		setTime(questionResponseUser,createDate);
		//check exist => update
		if(existsByQuestionIdAndExerciseId(dto.getQuestionDTO().getId(), dto.getExerciseDTO().getId())) {
			Long id  = findByQuestionIdAndExerciseId(dto.getQuestionDTO().getId(), dto.getExerciseDTO().getId()).getId();
			questionResponseUser.setId(id);
			
		}
		return questionResponseUserMapper.mapToViewDTO(this.questionResponseUserRepository.save(questionResponseUser)).getQuestionDTO();
	}
	
	@Override
	public List<QuestionResponseUserEntity> saveList(List<QuestionResponseUserDTO> questionResponseUserDTOs, LocalDateTime createDate) {
		List<QuestionResponseUserEntity> responseUserEntities = new ArrayList<>();
		questionResponseUserDTOs.forEach(item -> {
			QuestionResponseUserEntity questionResponseUser = questionResponseUserMapper.mapToEntity(item);
			setTime(questionResponseUser, createDate);
			QuestionResponseUserEntity entity = this.questionResponseUserRepository.save(questionResponseUser);
			responseUserEntities.add(entity);
		});
		return responseUserEntities;
	}
	

//	@Override
//	public QuestionResponseUserEntity findByQuestionId(Long id, LocalDateTime createDate)
//	{
//		int firstIndex = 0;
//		Long userId = UserContext.getId();
//		List<QuestionResponseUserEntity> questionResponseUsers = this.questionResponseUserRepository.findByQuestionId(id,userId,createDate);
//		if(questionResponseUsers != null && questionResponseUsers.size() >0)
//			return questionResponseUsers.get(firstIndex);
//		throw new ErrorMessageException(
//				String.format(messageSource.getMessage("NotFound", null,null), "QuestionResponseUser","Id",String.valueOf(id))
//				,TypeError.NotFound);
//	}
//	@Override
//	public void deleteByQuestionId(Long id)
//	{
//		List<QuestionResponseUserEntity> questionResponseUsers = this.questionResponseUserRepository.findByQuestion(id);
//		if(questionResponseUsers != null && questionResponseUsers.size()>0)
//		{
//			questionResponseUsers.forEach(questionResponseUser->{
//				questionResponseUser.setQuestion(null);
//				this.questionResponseUserRepository.save(questionResponseUser);
//				this.questionResponseUserRepository.deleteById(questionResponseUser.getId());
//			});
//		}
//	}
	private void setTime(QuestionResponseUserEntity questionResponseUser, LocalDateTime dateTime) {
		//set datetime
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		questionResponseUser.setCreatedDate(LocalDateTime.parse(dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString(),formatter));
		questionResponseUser.setModifiedDate(LocalDateTime.parse(dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString(),formatter));
		questionResponseUser.setCreatedBy(UserContext.getId());
		questionResponseUser.setModifiedBy(UserContext.getId());
	}



@Override
public void deleteByQuestionId(Long id) {
	// TODO Auto-generated method stub
	
}

@Override
public QuestionResponseUserDTO update(QuestionResponseUserDTO dto) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public QuestionResponseUserDTO findByQuestionIdAndExamId(Long id, LocalDateTime createDate) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public QuestionResponseUserDTO findByQuestionIdAndExerciseId(long questionId, long questionGroupId) {
	return questionResponseUserMapper.mapToDTO(questionResponseUserRepository.findByQuestionIdAndExerciseId(questionId, questionGroupId).get());
}

@Override
public Boolean existsByQuestionIdAndExerciseId(long questionId, long exerciseId) {
	return questionResponseUserRepository.existsByQuestionIdAndExerciseId(questionId, exerciseId);
}

@Override
public List<QuestionResponseUserEntity> saveListV2(long examId, List<QuestionResponseUserDoExamDTO> questionResponseUserDoExamDTOs,
		LocalDateTime createDate) {
	List<QuestionResponseUserEntity> responseUserEntities = new ArrayList<>();
	List<QuestionResponseUserDTO> questionResponseUserDTOs = new ArrayList<>();
	
	ExamDTO examDTO = new ExamDTO();
	examDTO.setId(examId);
	
	for (QuestionResponseUserDoExamDTO questionResponseUserDoExamDTO : questionResponseUserDoExamDTOs) {
		QuestionResponseUserDTO questionResponseUserDTO = new QuestionResponseUserDTO();
		questionResponseUserDTO.setId(questionResponseUserDoExamDTO.getId());
		questionResponseUserDTO.setExamDTO(examDTO);
		questionResponseUserDTO.setQuestionDTO(new QuestionDTO(questionResponseUserDoExamDTO.getQuestionId()));
		questionResponseUserDTO.setValueText(questionResponseUserDoExamDTO.getValueText());
		questionResponseUserDTOs.add(questionResponseUserDTO);
	}
	
	questionResponseUserDTOs.forEach(item -> {
		QuestionResponseUserEntity questionResponseUser = questionResponseUserMapper.mapToEntity(item);
		setTime(questionResponseUser, createDate);
		QuestionResponseUserEntity entity = this.questionResponseUserRepository.save(questionResponseUser);
		responseUserEntities.add(entity);
	});
	return responseUserEntities;
}

}
