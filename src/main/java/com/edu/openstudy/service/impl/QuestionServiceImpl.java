package com.edu.openstudy.service.impl;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.common.util.HandleGeneral;
import com.edu.openstudy.data.dto.*;
import com.edu.openstudy.data.entity.ChapterEntity;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionOptionEntity;
import com.edu.openstudy.data.entity.QuestionSolutionEntity;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.QuestionTypeEntity;
import com.edu.openstudy.data.entity.UserEntity;
import com.edu.openstudy.data.mapper.QuestionMapper;
import com.edu.openstudy.data.mapper.QuestionOptionMapper;
import com.edu.openstudy.data.mapper.QuestionSolutionMapper;
import com.edu.openstudy.data.repository.ChapterRepository;
import com.edu.openstudy.data.repository.QuestionOptionRepository;
import com.edu.openstudy.data.repository.QuestionRepository;
import com.edu.openstudy.data.repository.QuestionSolutionRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private QuestionOptionRepository questionOptionRepository;
	@Autowired
	private QuestionSolutionRepository questionSolutionRepository;
	@Autowired
	private QuestionMapper questionMapper;
	@Autowired
	private QuestionSolutionMapper questionSolutionMapper;
	@Autowired
	private QuestionOptionMapper questionOptionMapper;
	@Autowired
	private QuestionSolutionService questionSolutionService;
	@Autowired
	private QuestionOptionService questionOptionService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserScoreService userScoreService;
	@Autowired
	private QuestionTypeService questionTypeService;
	@Autowired
	private ChapterRepository chapterRepository;
	@Autowired
	private HandleGeneral handleGeneral;
	@Autowired
	private QuestionResponseUserService questionResponseUserService;
	@Autowired
	private ChapterService chapterService;

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<QuestionDTO> findQuestionByExamId(long id) {
		return questionRepository.findQuestionByExamId(id).stream().map(item -> this.questionMapper.mapToDTO(item))
				.collect(Collectors.toList());
	}
	
	
	@Override
	public QuestionDTO create(QuestionDTO dto) {
		QuestionEntity questionEntity = new QuestionEntity();
		LocalDateTime createDate = LocalDateTime.now();
		questionEntity = questionMapper.mapToEntity(dto);
		questionEntity.setCreatedBy(UserContext.getId());
		questionEntity.setModifiedBy(UserContext.getId());
		questionEntity.setCreatedDate(createDate);
		questionEntity.setModifiedDate(createDate);
		questionEntity = questionRepository.save(questionEntity);
		
		
		
		List<QuestionOptionEntity> options = new ArrayList<>();
		options =  dto.getQuestionOptions().stream().map(item -> this.questionOptionMapper.mapToEntity(item))
				.collect(Collectors.toList());
		for (QuestionOptionEntity questionOptionEntity : options) {
			questionOptionEntity.setCreatedBy(UserContext.getId());
			questionOptionEntity.setModifiedBy(UserContext.getId());
			questionOptionEntity.setCreatedDate(createDate);
			questionOptionEntity.setModifiedDate(createDate);
			questionOptionEntity.setQuestion(questionEntity);
			questionOptionRepository.save(questionOptionEntity);
		}
		
		
		QuestionSolutionEntity questionSolutionEntity = new QuestionSolutionEntity();
		for (QuestionOptionEntity questionOptionEntity : options) {
			if(questionOptionEntity.getOrder()==Integer.parseInt(dto.getQuestionSolution().getSolutionStr())){
				questionSolutionEntity.setSolution(String.valueOf(questionOptionEntity.getId()));
			}
		}
		questionSolutionMapper.mapToEntity(dto.getQuestionSolution());
		questionSolutionEntity.setQuestion(questionEntity);
		questionSolutionEntity.setCreatedBy(UserContext.getId());
		questionSolutionEntity.setModifiedBy(UserContext.getId());
		questionSolutionEntity.setCreatedDate(createDate);
		questionSolutionEntity.setModifiedDate(createDate);
		questionSolutionRepository.save(questionSolutionEntity);

		return dto;
	}
	@Override
	public void save(QuestionDTO dto) {
		QuestionEntity question = questionMapper.mapToEntity(dto);
		this.questionRepository.save(question);
	}

	@Override
	public List<QuestionDTO> findAll() {
		return questionRepository.findAll().stream().map(item -> this.questionMapper.mapToDTO(item))
				.collect(Collectors.toList());
	}

	@Override
	public QuestionDTO findById(Long id) {
		QuestionEntity question = questionRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
				String.format(messageSource.getMessage("NotFound", null, null), "Question", "Id", String.valueOf(id)),
				TypeError.NotFound));
		return questionMapper.mapToDTO(question);
	}
	@Override
	public void update(QuestionDTO dto, long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setQuestionSolutionInQuestionDTO(QuestionDTO questionDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public QuestionDTO update(QuestionDTO dto, ExamEntity questionGroup, LocalDateTime createDate, Long createDateBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends QuestionDTO> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QuestionDTO getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteById(Long id) {
		questionRepository.deleteById(id);

	}

	@Override
	public PaginationDTO findAllPaging(int no, int limit) {
		Pageable page = PageRequest.of(no, limit);// Page: 0 and Member: 10
		List<Object> listQuestion = this.questionRepository.findAll(page).toList().stream()
				.map(item -> this.questionMapper.mapToDTO(item)).collect(Collectors.toList());
		Page<QuestionEntity> pageQuestion = this.questionRepository.findAll(page);
		return new PaginationDTO(listQuestion, pageQuestion.isFirst(), pageQuestion.isLast(),
				pageQuestion.getTotalPages(), pageQuestion.getTotalElements(), pageQuestion.getSize(),
				pageQuestion.getNumber());
	}

	@Override
	public QuestionDTO getByExamAndOrder(long examId, int order) {	
		return questionMapper.mapToDTO(questionRepository.findByExamIdAndOrder(examId, order).get());
	}



}
