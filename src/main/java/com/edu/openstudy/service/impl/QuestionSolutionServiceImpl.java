package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.edu.openstudy.common.DateTime;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.data.dto.QuestionSolutionDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.entity.QuestionEntity;
import com.edu.openstudy.data.entity.QuestionSolutionEntity;
import com.edu.openstudy.data.mapper.QuestionSolutionMapper;
import com.edu.openstudy.data.repository.QuestionSolutionRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.QuestionSolutionService;
import com.edu.openstudy.service.UserScoreService;

@Service
public class QuestionSolutionServiceImpl implements QuestionSolutionService {
	@Autowired
	private QuestionSolutionRepository questionSolutionRepository;
	@Autowired
	private QuestionSolutionMapper questionSolutionMapper;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserScoreService userScoreService;
	@Override
	public void save(QuestionSolutionDTO questionSolutionDTO, QuestionEntity questionSaved) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(QuestionSolutionDTO questionSolutionDTO, QuestionEntity questionSaved,
			LocalDateTime creatDateTime, Long createDateBy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public QuestionSolutionEntity findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<QuestionSolutionDTO> findByQuestion(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<QuestionSolutionDTO> findByQuestionDTO(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void update(List<QuestionSolutionDTO> questionSolutionDTOs, QuestionEntity questionSaved,
			LocalDateTime creatDateTime, Long createDateBy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int countByLesson(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void deleteByQuestion(Long questionId) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public void save(QuestionSolutionDTO questionSolutionDTO, QuestionEntity questionSaved) {
//		QuestionSolutionEntity questionSolution = this.questionSolutionMapper.mapToEntity(questionSolutionDTO);
//		questionSolution.setQuestion(questionSaved);
//		questionSolution.setCreatedDate(DateTime.getInstances());
//		questionSolution.setCreatedBy(questionSaved.getCreatedBy());
//		questionSolution.setModifiedDate(DateTime.getInstances());
//		questionSolution.setModifiedBy(questionSaved.getModifiedBy());
//		this.questionSolutionRepository.save(questionSolution);
//	}
//
//	@Override
//	public void update(QuestionSolutionDTO questionSolutionDTO, QuestionEntity questionSaved, LocalDateTime creatDateTime,
//		Long createDateBy) {
//		QuestionSolutionEntity questionSolution = this.questionSolutionMapper.mapToEntity(questionSolutionDTO);
//		questionSolution.setQuestion(questionSaved);
//		questionSolution.setCreatedDate(creatDateTime);
//		questionSolution.setCreatedBy(createDateBy);
//		questionSolution.setModifiedDate(LocalDateTime.now());
//		questionSolution.setModifiedBy(UserContext.getId());
//		this.questionSolutionRepository.save(questionSolution);
//	}
//
//	@Override
//	public void update(List<QuestionSolutionDTO> solutions, QuestionEntity questionSaved, LocalDateTime creatDate,
//		Long createDateBy) {
//		List<QuestionSolutionEntity> questionSolutions = this.questionSolutionRepository
//				.getQuestionSolutionByQuestionId(questionSaved.getId());
//		List<QuestionSolutionDTO> entities = getEntityListSolution(solutions);
//		List<QuestionSolutionDTO> dtos = getDTOListSolution(solutions);
//		if (dtos != null && dtos.size() > 0) {
//			for (QuestionSolutionDTO questionSolutionDTO : dtos) {
//				save(questionSolutionDTO, questionSaved);
//			}
//		}
//		List<QuestionSolutionEntity> solutionNotUses = getListNotUse(questionSolutions, solutions);
//		if (solutionNotUses != null && solutionNotUses.size() > 0) {
//			for (QuestionSolutionEntity questionSolution : solutionNotUses) {
//				deleteBySolution(questionSolution);
//			}
//		}
//		if (entities != null && entities.size() > 0) {
//			entities.forEach(dto -> {
//				update(dto, questionSaved, creatDate, createDateBy);
//			});
//		}
//	}
//
//	@Override
//	public QuestionSolutionEntity findById(Long id) {
//		QuestionSolutionEntity questionSolution = this.questionSolutionRepository.findById(id).orElseThrow(
//				() -> new ErrorMessageException(String.format(this.messageSource.getMessage("NotFound", null, null),
//						"Solution", "id", String.valueOf(id)), TypeError.NotFound));
//		return questionSolution;
//	}
//
//	@Override
//	public List<QuestionSolutionDTO> findByQuestion(Long id) {
//		List<QuestionSolutionEntity> questionSolutions = this.questionSolutionRepository.getQuestionSolutionByQuestionId(id);
//		List<QuestionSolutionDTO> dtos = new ArrayList<QuestionSolutionDTO>();
//		for (QuestionSolutionEntity questionSolution : questionSolutions) {
//			QuestionSolutionDTO dto = this.questionSolutionMapper.mapmapToDTO(questionSolution);
//			dtos.add(dto);
//		}
//		return dtos;
//	}
//
//	@Override
//	public List<QuestionSolutionDTO> findByQuestionDTO(Long id) {
//		List<QuestionSolutionEntity> questionSolutions = this.questionSolutionRepository.getQuestionSolutionByQuestionId(id);
//		List<QuestionSolutionDTO> dtos = new ArrayList<QuestionSolutionDTO>();
//		for (QuestionSolutionEntity questionSolution : questionSolutions) {
//			QuestionSolutionDTO dto = this.questionSolutionMapper.mapmapToDTO(questionSolution);
//			dtos.add(dto);
//		}
//		return dtos;
//	}
//
//	public void deleteBySolution(QuestionSolutionEntity solution) {
////		solution.setQuestion(null);
////		this.questionSolutionRepository.save(solution);
//
//		// set user score (cham lai)
//		this.userScoreService.doScoreAgain(solution.getId());
//
//		this.questionSolutionRepository.deleteById(solution.getId());
//	}
//
//	@Override
//	public int countByLesson(Long id) {
//		return this.questionSolutionRepository.countQuestionSoltutionByLessonId(id);
//	}
//
//	@Override
//	public void deleteByQuestion(Long questionId) {
//		List<QuestionSolutionEntity> questionSolutions = questionSolutionRepository
//				.getQuestionSolutionByQuestionId(questionId);
//		questionSolutions.forEach(solution -> {
//			deleteBySolution(solution);
//		});
//	}
//
//	private List<QuestionSolutionDTO> getEntityListSolution(List<QuestionSolutionDTO> dtos) {
//		List<QuestionSolutionDTO> options = new ArrayList<QuestionSolutionDTO>();
//		dtos.forEach(dto -> {
//			if (dto.getId() != 0)
//				options.add(dto);
//		});
//		return options;
//	}
//
//	private List<QuestionSolutionDTO> getDTOListSolution(List<QuestionSolutionDTO> dtos) {
//		List<QuestionSolutionDTO> options = new ArrayList<QuestionSolutionDTO>();
//		dtos.forEach(dto -> {
//			if (dto.getId() == 0)
//				options.add(dto);
//		});
//		return options;
//	}
//
//	private List<QuestionSolutionEntity> getListNotUse(List<QuestionSolutionEntity> solutions, List<QuestionSolutionDTO> dtos) {
//
//		List<Long> ids = new ArrayList<Long>();
//		//get id from dtos
//		dtos.forEach(dto -> {
//			ids.add(dto.getId());
//		});
//		List<QuestionSolutionEntity> results = new ArrayList<QuestionSolutionEntity>();
//		if (solutions != null && solutions.size() > 0)
//			//compare id entity and id dto. if id entity not exists in idDTO, add array to Remove
//			for (int i = 0; i < solutions.size(); i++) {
//				QuestionSolutionEntity optionCurrent = solutions.get(i);
//				Long id = optionCurrent.getId();
//				if (!ids.contains(id)) {
//					results.add(optionCurrent);
//				}
//			}
//		return results;
//
//	}

}
