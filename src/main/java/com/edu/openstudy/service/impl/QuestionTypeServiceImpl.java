package com.edu.openstudy.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import com.edu.openstudy.data.dto.QuestionTypeDTO;
import com.edu.openstudy.data.entity.QuestionTypeEntity;
import com.edu.openstudy.data.mapper.QuestionTypeMapper;
import com.edu.openstudy.data.repository.QuestionTypeRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.service.QuestionTypeService;

@Service
public class QuestionTypeServiceImpl implements QuestionTypeService {
	//@Autowired
	private QuestionTypeRepository questionTypeRepository;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private QuestionTypeMapper questionTypeMapper;
	private final String LINEAR_SCALE = "LINEAR_SCALE";
	private final String MULTIPLE_CHOICE_GRID = "MULTIPLE_CHOICE_GRID";
	private final String DATE = "DATE";
	private final String TIME = "TIME";

	@Override
	public QuestionTypeEntity findById(Long id) {
		if(id == null)
			id = Long.valueOf(0);
		long questinTypeId = id;
		return this.questionTypeRepository.findById(id).orElseThrow(
				() -> new ErrorMessageException(String.format(messageSource.getMessage("NotFound", null, null),
						"Questin Type", "Id", String.valueOf(questinTypeId)), TypeError.NotFound));
	}

	@Override
	public List<QuestionTypeDTO> findAll() {
		List<QuestionTypeEntity> questionTypes = this.questionTypeRepository.findAll();
		List<QuestionTypeDTO> questionTypeDTOs = new ArrayList<QuestionTypeDTO>();
		questionTypes.forEach(type -> {
			String code = type.getCode();
			if (!code.equals(LINEAR_SCALE) && !code.equals(MULTIPLE_CHOICE_GRID) && !code.equals(DATE)
					&& !code.equals(TIME)) {
				QuestionTypeDTO questionTypeDTO = this.questionTypeMapper.mapToDTO(type);
				questionTypeDTOs.add(questionTypeDTO);
			}
		});
		return questionTypeDTOs;
	}
}
