package com.edu.openstudy.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.edu.openstudy.data.dto.ChapterDTO;
import com.edu.openstudy.data.dto.QuestionOptionDTO;
import com.edu.openstudy.data.dto.QuestionResponseUserViewDTO;
import com.edu.openstudy.data.dto.StatisticsDTO;
import com.edu.openstudy.data.dto.UserContext;
import com.edu.openstudy.data.dto.UserScoreDTO;
import com.edu.openstudy.data.dto.UserScoreViewDTO;
import com.edu.openstudy.data.entity.ExamEntity;
import com.edu.openstudy.data.entity.QuestionResponseUserEntity;
import com.edu.openstudy.data.entity.UserScoreEntity;
import com.edu.openstudy.data.mapper.ChapterMapper;
import com.edu.openstudy.data.mapper.QuestionResponseUserMapper;
import com.edu.openstudy.data.mapper.UserScoreMapper;
import com.edu.openstudy.data.repository.ChapterRepository;
import com.edu.openstudy.data.repository.ExamRepository;
import com.edu.openstudy.data.repository.UserScoreRepository;
import com.edu.openstudy.service.UserScoreService;


@Service
@Transactional
public class UserScoreServiceImpl implements UserScoreService {
	@Autowired
	private QuestionResponseUserMapper questionResponseUserMapper;
	@Autowired
	private UserScoreRepository userScoreRepository;
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private UserScoreMapper userScoreMapper;
	@Autowired
	private ChapterRepository chapterRepository;
	@Autowired
	private ChapterMapper chapterMapper;
	@Override
	public UserScoreDTO save(UserScoreDTO userScoreDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteByCreate(String dateTime) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteByQuestionId(long questionId) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<UserScoreEntity> findBySolutionId(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserScoreDTO doScore(long examId, LocalDateTime createTime,List<QuestionResponseUserEntity> responseUserEntities, long timeWork) {
		
		List<QuestionResponseUserViewDTO> responseUserViewDTOs = responseUserEntities.stream().map(item -> this.questionResponseUserMapper.mapToViewExamDTO(item)).collect(Collectors.toList());

		UserScoreEntity userScoreEntity = new UserScoreEntity();
		ExamEntity examEntity = examRepository.findById(examId).get();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		userScoreEntity.setCreatedDate(LocalDateTime.parse(createTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString(),formatter));
		userScoreEntity.setModifiedDate(LocalDateTime.parse(createTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString(),formatter));
		userScoreEntity.setCreatedBy(UserContext.getId());
		userScoreEntity.setModifiedBy(UserContext.getId());

		userScoreEntity.setTimeWork(timeWork);
		userScoreEntity.setRightAnswer(getRightAnswer(responseUserViewDTOs));

		userScoreEntity.setWrongAnswer(getWrongAnswer(responseUserViewDTOs, userScoreEntity.getRightAnswer()));

		userScoreEntity.setExam(examEntity);
		userScoreEntity.setTotalQuestion(examEntity.getQuantity());
		
		userScoreEntity.setScore(getScore(userScoreEntity.getRightAnswer(),userScoreEntity.getTotalQuestion()));

		UserScoreDTO userScoreDTO = userScoreMapper.mapToDTO(userScoreRepository.save(userScoreEntity));
		userScoreDTO.setDuration(examEntity.getDuration());
		return userScoreDTO;
	}

	private int getWrongAnswer(List<QuestionResponseUserViewDTO> responseUserViewDTOs, int numberRightAnswer) {
		int numberMissingAnswer = 0;
		for (QuestionResponseUserViewDTO questionResponseUserViewDTO : responseUserViewDTOs) {
			if(questionResponseUserViewDTO.getValueText().equals("")|| questionResponseUserViewDTO.getValueText()==null) {
				numberMissingAnswer++;
			}
		}	 
		return (responseUserViewDTOs.size() - (numberMissingAnswer + numberRightAnswer));
	}

	private int getRightAnswer(List<QuestionResponseUserViewDTO> responseUserViewDTOs) {
		int numberRightAnswer = 0;
		for (QuestionResponseUserViewDTO questionResponseUserViewDTO : responseUserViewDTOs) {
			questionResponseUserViewDTO.getQuestionDTO().getQuestionResponseUser();
//			List<String> listValueText = questionResponseUserViewDTO.getQuestionDTO().getQuestionResponseUser();
			List<String> listValueText = new ArrayList<>();
			if(questionResponseUserViewDTO.getValueText()!=null) {
				listValueText = new ArrayList<String>(Arrays.asList(questionResponseUserViewDTO.getValueText().split(",")));
			}	
			List<QuestionOptionDTO> listOptionSolution = questionResponseUserViewDTO.getQuestionDTO().getQuestionSolutions().getSolution();
			List<String> listSolution = new ArrayList<>();
			for (QuestionOptionDTO dto : listOptionSolution) {
				listSolution.add(Long.toString(dto.getId()));
			}
			boolean flgResult = true;
			if(listValueText!=null) {
				if(listSolution.size()==listValueText.size())
					for (String valueText : listValueText) {
						if(listSolution.contains(valueText)==false) {
							flgResult = false;break;
						}
				}
				else {
					flgResult = false;
				}
			}else {
				flgResult = false;
			}
			
			if(flgResult) {
				numberRightAnswer++;
			}
		}
		return numberRightAnswer;
	}

	private double getScore(int numberRightAnswer,  int totalQuestion) {
		double score = ((double)10/totalQuestion) * numberRightAnswer;
		return score;
	}

	@Override
	public List<UserScoreViewDTO> findAllByCreatedBy(long userId) {
		return userScoreRepository.findAllByCreatedByOrderByCreatedDateDesc(userId).stream()
				.map(item -> this.userScoreMapper.mapToViewDTO(item)).collect(Collectors.toList());
	}

	@Override
	public UserScoreDTO findById(long id) {
		return userScoreMapper.mapToDTO(userScoreRepository.findById(id).get());
	}

	@Override
	public List<StatisticsDTO> statisticsChapter(long userId) {
		List<StatisticsDTO> statisticsDTOs = chapterRepository.findAll().stream()
				.map(item -> this.chapterMapper.mapToStatisticsDTO(item)).collect(Collectors.toList());
		for (StatisticsDTO statisticsDTO : statisticsDTOs) {
			Long totalAnswer = userScoreRepository.getTotalAnswerByExamIdAndCreatedBy(statisticsDTO.getId(),userId);
			Long rightAnswer = userScoreRepository.getRightAnswerByExamIdAndCreatedBy(statisticsDTO.getId(), userId);
			Long wrongAnswer = userScoreRepository.getWrongAnswerByExamIdAndCreatedBy(statisticsDTO.getId(),userId);
			if (totalAnswer==null)
				totalAnswer = (long) 0;	
			if (rightAnswer==null)
				rightAnswer = (long) 0;
			if (wrongAnswer ==null)
				wrongAnswer = (long) 0;
			statisticsDTO.setWrongAnswers(wrongAnswer);
			statisticsDTO.setRightAnswers(rightAnswer);
			statisticsDTO.setTotalAnswers(totalAnswer);
			statisticsDTO.setMissingAnswers(totalAnswer-(rightAnswer+wrongAnswer));
		}
		return statisticsDTOs;
	}

}