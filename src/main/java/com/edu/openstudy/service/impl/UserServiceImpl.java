package com.edu.openstudy.service.impl;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties.Validation;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.edu.openstudy.common.enumeration.TypeError;
import com.edu.openstudy.config.auth.UserDetailsImpl;
import com.edu.openstudy.data.dto.PaginationDTO;
import com.edu.openstudy.data.dto.UserDTO;
import com.edu.openstudy.data.entity.RoleEntity;
import com.edu.openstudy.data.entity.UserEntity;
import com.edu.openstudy.data.mapper.UserMapper;
import com.edu.openstudy.data.repository.UserRepository;
import com.edu.openstudy.exception.ErrorMessageException;
import com.edu.openstudy.service.MailService;
import com.edu.openstudy.service.RoleService;
import com.edu.openstudy.service.UserService;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired 
    JavaMailSender mailSender;
     
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UserMapper userMapper;

	@Autowired
	private MailService mailService;
	
	@Autowired
	private RoleService roleService;
	
//	@Autowired
//	private Validation validation;

	@Autowired
	private MessageSource messageSource;
//	@Autowired
//	private UpdateFile updateFile;
	public final Logger logger = LoggerFactory.getLogger("info");
	
	@Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(email).get();
        return UserDetailsImpl.build(userEntity);
    }


	@Override
	public boolean existsById(long id) {
		return userRepository.existsById(id);
	}

	@Override
	public List<UserDTO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDTO findByEmail(String email) {
		return userMapper.mapToDTO(userRepository.findByEmail(email).get());
	}

	@Override
	public UserDTO save(UserEntity user) {
		return userMapper.mapToDTO(userRepository.save(user));
	}

	@Override
	public UserDTO findById(long userId) {
		return userMapper.mapToDTO(userRepository.findById(userId).get());
	}

	public UserEntity getById(long userId) {
		return userRepository.findById(userId).get();
	}
	@Override
	public List<UserDTO> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaginationDTO findAllPaging(int no, int limit) {
		Pageable page = PageRequest.of(no, limit);// Page: 0 and Member: 10
		List<Object> listUser = this.userRepository.findAll(page).toList().stream()
				.map(item -> this.userMapper.mapToDTO(item)).collect(Collectors.toList());
		Page<UserEntity> pageUser = this.userRepository.findAll(page);
		return new PaginationDTO(listUser, pageUser.isFirst(), pageUser.isLast(), pageUser.getTotalPages(),
				pageUser.getTotalElements(), pageUser.getSize(), pageUser.getNumber());
	}

	@Override
	public UserDTO update(UserDTO user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkIfValidOldPassword(String oldPass, String newPass) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object[]> statisticsByRole() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public String resetPassword(String email) {
		userRepository.findByEmail(email);
		return null;
	}

	@Override
	public String encodePass(String pass) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public UserDTO handlerValid(UserDTO userDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDTO create(UserDTO dto) {
		if(existsByEmail(dto.getEmail())){
			throw new ErrorMessageException(String.format(messageSource.getMessage("AlreadyExists", null, null),
					"Email ", " Request", String.valueOf("")), TypeError.AlreadyExists);
		}
		dto.setCreatedDate(LocalDateTime.now());
		dto.setModifiedDate(LocalDateTime.now());
		dto.setPassword(passwordEncoder.encode(dto.getPassword()));
		UserEntity entity = userMapper.mapToEntity(dto);
		entity.setRole(new RoleEntity(1, null, null, null, null, null));
		return save(entity);
	}

    public void generateOneTimePassword(UserEntity user) throws UnsupportedEncodingException, MessagingException {
        String OTP = RandomString.make(6);
        String encodedOTP = passwordEncoder.encode(OTP);
         
        user.setOneTimePassword(encodedOTP);
        user.setOtpRequestedTime(new Date());
         
        userRepository.save(user);
         
        sendOTPEmail(user, OTP);
    }
     
    public void sendOTPEmail(UserEntity user, String OTP)        throws UnsupportedEncodingException, MessagingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);
         
        helper.setFrom("hocmo.edu@gmail.com", "Học Mở");
        helper.setTo(user.getEmail());
         
        String subject = "Đây là mã (OTP) của bạn - Có hiệu lực trong 5 phút!";
         
        String content = "<p>Hello " + user.getName() + "</p>"
                + "<p>Vui lòng không chia sẻ mã này cho bất kì người nào khác "
                + "One Time Password to login:</p>"
                + "<p><b>" + OTP + "</b></p>"
                + "<br>"
                + "<p>Chú ý: Mã này chỉ có hiệu lực trọng 5 phút.</p>";
         
        helper.setSubject(subject);
         
        helper.setText(content, true);
         
        mailSender.send(message);
    }
 
    public void clearOTP(UserEntity user) {
    	  user.setOneTimePassword(null);
    	  user.setOtpRequestedTime(null);
    }


	@Override
	public boolean verifiedEmail(long userId, String optCode) {
		UserEntity entity = getById(userId);
		if( passwordEncoder.matches(optCode, entity.getOneTimePassword())){
			entity.setVerifiedEmail(true);
			clearOTP(entity);
			userRepository.save(entity);
			return true;
		}
		return false;
	}


	@Override
	public void sendOTPEmail(long userId) {
		UserEntity entity = getById(userId);
		try {
			generateOneTimePassword(entity);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
	}


	@Override
	public void delete(long userId) {
		userRepository.deleteById(userId);
	}  

}
