package com.edu.openstudy.service.strategy;

import com.edu.openstudy.data.dto.LessonCreateDTO;
import com.edu.openstudy.data.entity.ChapterEntity;

public interface LessonStrategy {

	void update(long id, LessonCreateDTO dto, ChapterEntity chapter);

}
